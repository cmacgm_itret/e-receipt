﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DocTeam.Master" AutoEventWireup="true" CodeBehind="DocTeam_EventReview.aspx.cs" Inherits="E_Receipt_v1_1.DocTeam_EventReview" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeaderTitle" runat="server">
    <h2><strong>Event</strong> Review</h2>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentValues" runat="server">
    <div class="row">
        <div class="col-md-12">
            <dx:ASPxCallbackPanel ID="cbpEventReview" runat="server" ClientInstanceName="cbpEventReview" Width="100%">
                <PanelCollection>
                    <dx:PanelContent>
                        <dx:ASPxGridView ID="gvEventReview" ClientInstanceName="gvEventReview" runat="server" Width="100%" Theme="Aqua"
                            DataSourceID="dsEventReview" KeyFieldName="LogDate" >
                            <Columns>
                                <dx:GridViewDataDateColumn Caption="Log Release Date" Name="releaseDate"
                                    FieldName="releaseDate" VisibleIndex="0" Width="175px" FixedStyle="Left">
                                    <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                    <CellStyle Font-Size="12px" Wrap="False" HorizontalAlign="Center">
                                    </CellStyle>
                                    <PropertiesDateEdit DisplayFormatString="dd-MMM-yyyy hh:mm:ss">
                                    </PropertiesDateEdit>
                                    <SettingsHeaderFilter Mode="DateRangeCalendar" />
                                </dx:GridViewDataDateColumn>
                                <dx:GridViewDataTextColumn Caption="Release By" FieldName="releaseBy" Name="releaseBy" 
                                    VisibleIndex="1" Width="175px" ReadOnly="True" Visible="true" FixedStyle="Left" 
                                    Settings-AllowHeaderFilter="True">
                                    <SettingsHeaderFilter Mode="CheckedList" />
                                    <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                    <CellStyle Font-Size="12px">
                                    </CellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Log ReceiptID" FieldName="receiptID" Name="receiptID" 
                                    VisibleIndex="2" Width="175px" ReadOnly="True" Visible="true" FixedStyle="Left">
                                    <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                    <CellStyle Font-Size="12px">
                                    </CellStyle>
                                </dx:GridViewDataTextColumn>
                            </Columns>
                            <SettingsPager PageSize="15" ShowSeparators="True" AlwaysShowPager="True" >
                                <PageSizeItemSettings Visible="true" Position="Right" Items="15, 20, 25, 30, 35, 40, 45, 50" />
                            </SettingsPager>
                            <SettingsBehavior ColumnResizeMode="Control" ConfirmDelete="True" FilterRowMode="Auto" />
                            <Settings ShowFooter="True" VerticalScrollableHeight="450" 
                                VerticalScrollBarMode="Visible" HorizontalScrollBarMode="Auto"
                                ShowFilterBar="Auto" ShowFilterRow="true" ShowFilterRowMenu="true" ShowGroupPanel="True" ShowHeaderFilterButton="false" />
                            <SettingsLoadingPanel Mode="ShowAsPopup" />
                        </dx:ASPxGridView>
                    </dx:PanelContent>
                </PanelCollection>
            </dx:ASPxCallbackPanel>
            <asp:SqlDataSource ID="dsEventReview" runat="server" ConnectionString="<%$ ConnectionStrings:ereceiptConnectionString %>"
                ProviderName="<%$ ConnectionStrings:ereceiptConnectionString.ProviderName %>"
                SelectCommand="SELECT receiptID, releaseFlag, releaseDate, releaseBy FROM tblReleaseDoc;"></asp:SqlDataSource>
        </div>
    </div>
</asp:Content>
