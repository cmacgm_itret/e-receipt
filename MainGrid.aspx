﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ER.Master" AutoEventWireup="true" CodeBehind="MainGrid.aspx.cs" Inherits="E_Receipt_v1_1.MainGrid" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeaderTitle" runat="server">
    <h2><strong>Main</strong> Grid</h2>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentValues" runat="server">
    <div class="row">
        <div class="col-lg-12">
            <dx:ASPxPageControl ID="pcMainContent" runat="server" ActiveTabIndex="0" Width="100%" Theme="iOS" EnableCallBacks="true">
                <TabPages>
                    <dx:TabPage Text="OR Daily">
                        <ContentCollection>
                            <dx:ContentControl>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel-group panel-accordion" id="accordion">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4>
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                            Filter
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                Receipt# / BL# / Inv#
                                                            </div>
                                                            <div class="col-md-4">
                                                                <dx:ASPxTextBox ID="txtBLFilterORDaily" runat="server" ClientInstanceName="txtBLFilterORDaily" Theme="iOS" Width="100%" />
                                                            </div>
                                                            <div class="col-md-2">
                                                                <dx:ASPxButton ID="btnSubmitFilterORDaily" runat="server" Text="Search" Theme="iOS" OnClick="btnSubmitFilterORDaily_Click" AutoPostBack="false" />
                                                                &nbsp;&nbsp;
                                                                <dx:ASPxButton ID="btnClearFilterORDaily" runat="server" Text="Clear" Theme="iOS" OnClick="btnClearFilterORDaily_Click" AutoPostBack="false" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>        
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row m-b-5">
                                            <div class="col-lg-2">
                                                <dx:ASPxButton ID="btnSendReceiptORDaily" runat="server" Text="Send Receipt" Theme="iOS" OnClick="btnSendReceiptORDaily_Click" AutoPostBack="false" />
                                            </div>
                                            <div class="col-lg-10">
                                                <dx:ASPxLabel ID="lblSubmitWarningORDaily" runat="server" CssClass="label label-danger" Visible="false" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <dx:ASPxCallbackPanel ID="cbpORDaily" ClientInstanceName="cbpORDaily" runat="server" Width="100%" Theme="iOS">
                                                    <PanelCollection>
                                                        <dx:PanelContent>
                                                            <dx:ASPxGridView ID="gvORDaily" ClientInstanceName="gvORDaily" runat="server" KeyFieldName="IDReceipt" Width="100%" 
                                                                Theme="Aqua" DataSourceID="dsORDaily" OnCustomColumnDisplayText="gvORDaily_CustomColumnDisplayText">
                                                                <Columns>
                                                                    <dx:GridViewCommandColumn ShowSelectCheckbox="true" SelectAllCheckboxMode="AllPages" VisibleIndex="0" FixedStyle="Left" />
                                                                    <dx:GridViewDataTextColumn Caption="Receipt Number" FieldName="IDReceipt" Name="IDReceipt" 
                                                                        VisibleIndex="1" Width="175px" ReadOnly="True" Visible="true" FixedStyle="Left">
                                                                        <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                                        <CellStyle Font-Size="12px">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="BL#" FieldName="BLNUMBER" Name="BLNUMBER" 
                                                                        VisibleIndex="2" Width="175px" ReadOnly="True" Visible="true" FixedStyle="Left">
                                                                        <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                                        <CellStyle Font-Size="12px">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="Invoice #" FieldName="InvoiceNumber" Name="InvoiceNumber" 
                                                                        VisibleIndex="3" Width="175px" ReadOnly="True" Visible="true">
                                                                        <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                                        <CellStyle Font-Size="10px">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="Consignee Name" FieldName="CNEE" Name="CNEE" 
                                                                        VisibleIndex="4" Width="200px" ReadOnly="True" Visible="true">
                                                                        <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                                        <CellStyle Font-Size="12px">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataDateColumn Caption="Receipt Date" Name="ReceiptDate"
                                                                        FieldName="ReceiptDate" VisibleIndex="5" Width="150px" ReadOnly="True">
                                                                        <PropertiesDateEdit DisplayFormatString="dd-MMM-yyyy hh:mm:ss">
                                                                        </PropertiesDateEdit>
                                                                        <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                                        <CellStyle Font-Size="12px" Wrap="False" HorizontalAlign="Center">
                                                                        </CellStyle>
                                                                        <SettingsHeaderFilter Mode="DateRangeCalendar" />
                                                                    </dx:GridViewDataDateColumn>
                                                                    <dx:GridViewDataTextColumn Caption="Curr" FieldName="Currency" Name="Currency" 
                                                                        VisibleIndex="6" Width="80px" ReadOnly="True" Visible="true">
                                                                        <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                                        <CellStyle Font-Size="12px">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataSpinEditColumn Caption="Amount" FieldName="Amount" 
                                                                        Name="Amount"  VisibleIndex="7" Width="118px" ReadOnly="True">
                                                                        <PropertiesSpinEdit DisplayFormatString="N1"></PropertiesSpinEdit>
                                                                        <HeaderStyle Font-Size="12px" HorizontalAlign="Center" Wrap="True" />
                                                                        <CellStyle Font-Size="12px" Wrap="False"/>
                                                                    </dx:GridViewDataSpinEditColumn>
                                                                    <dx:GridViewDataTextColumn Caption="Type" FieldName="TYPE" Name="TYPE" ShowInCustomizationForm="True"  
                                                                        VisibleIndex="8" Width="80px" ReadOnly="true" Visible="true" Settings-AllowHeaderFilter="True" >
<Settings AllowHeaderFilter="True"></Settings>

                                                                        <SettingsHeaderFilter Mode="CheckedList" />
                                                                        <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                                        <CellStyle Font-Size="12px" HorizontalAlign="Center">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="Payment Method" FieldName="PaymentMethodDSP" Name="PaymentMethodDSP" 
                                                                        VisibleIndex="9" Width="175px" ReadOnly="True" Visible="true" Settings-AllowHeaderFilter="True">
<Settings AllowHeaderFilter="True"></Settings>

                                                                        <SettingsHeaderFilter Mode="CheckedList" />
                                                                        <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                                        <CellStyle Font-Size="12px" HorizontalAlign="Center">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataMemoColumn Caption="Remarks" FieldName="Remarks" 
                                                                        Name="Remarks" VisibleIndex="10" Width="250px" ReadOnly="True">
                                                                        <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                                        <CellStyle Font-Size="10px" Wrap="True" />
                                                                    </dx:GridViewDataMemoColumn>
                                                                    <dx:GridViewDataMemoColumn Caption="Charge Code" FieldName="ChargeCode" 
                                                                        Name="ChargeCode" VisibleIndex="11" Width="250px" ReadOnly="True">
                                                                        <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                                        <CellStyle Font-Size="10px" Wrap="True" />
                                                                    </dx:GridViewDataMemoColumn>
                                                                    <dx:GridViewDataTextColumn Caption="Receipt By" FieldName="username" Name="username" 
                                                                        VisibleIndex="12" Width="95px" ReadOnly="True" Visible="true" Settings-AllowHeaderFilter="True">
                                                                        <Settings AllowHeaderFilter="True"></Settings>
                                                                        <SettingsHeaderFilter Mode="CheckedList" />
                                                                        <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                                        <CellStyle Font-Size="12px">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="REFFNO" FieldName="REFFNO" Name="REFFNO" 
                                                                        VisibleIndex="13" Width="100px" ReadOnly="True" Visible="true">
                                                                        <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                                        <CellStyle Font-Size="12px">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="Flag Send" FieldName="FlagSend" Name="FlagSend" 
                                                                        VisibleIndex="14" Width="80px" ReadOnly="True" Visible="true">
                                                                        <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                                        <CellStyle Font-Size="12px">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataDateColumn Caption="Latest Send" FieldName="LatestSend" Name="LatestSend" 
                                                                        VisibleIndex="15" Width="120px" ReadOnly="True" Visible="true">
                                                                        <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                                        <CellStyle Font-Size="12px">
                                                                        </CellStyle>
                                                                        <PropertiesDateEdit DisplayFormatString="dd-MMM-yyyy hh:mm:ss">
                                                                        </PropertiesDateEdit>
                                                                        <SettingsHeaderFilter Mode="DateRangeCalendar" />
                                                                    </dx:GridViewDataDateColumn>
                                                                    <dx:GridViewDataTextColumn Caption="Latest By" FieldName="LatestBy" Name="LatestBy" 
                                                                        VisibleIndex="16" Width="120px" ReadOnly="True" Visible="true">
                                                                        <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                                        <CellStyle Font-Size="12px">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataMemoColumn Caption="Email From Lara" FieldName="EmailWS" Name="EmailWS" 
                                                                        VisibleIndex="17" Width="300px" Visible="true">
                                                                        <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                                        <CellStyle Font-Size="10px">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataMemoColumn>
                                                                    <dx:GridViewDataTextColumn Caption="Other Receipient" FieldName="OtherEmail" Name="OtherEmail" 
                                                                        VisibleIndex="18" Width="300px" Visible="true">
                                                                        <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                                        <CellStyle Font-Size="10px">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                </Columns>
                                                                <SettingsPager PageSize="15" ShowSeparators="True" AlwaysShowPager="True" >
                                                                    <PageSizeItemSettings Visible="true" Position="Right" Items="15, 20, 25, 30, 35, 40, 45, 50" />
                                                                </SettingsPager>
                                                                <SettingsBehavior ColumnResizeMode="Control" ConfirmDelete="True" FilterRowMode="Auto" />
                                                                <Settings ShowFooter="True" VerticalScrollableHeight="450" 
                                                                    VerticalScrollBarMode="Visible" HorizontalScrollBarMode="Auto"
                                                                    ShowFilterBar="Auto" ShowFilterRow="true" ShowFilterRowMenu="true" ShowGroupPanel="True" ShowHeaderFilterButton="false" />
                                                                <SettingsLoadingPanel Mode="ShowAsPopup" />
                                                                <SettingsEditing Mode="Batch" BatchEditSettings-AllowValidationOnEndEdit="true" BatchEditSettings-StartEditAction="DblClick">
                                                                    <BatchEditSettings EditMode="Row" ></BatchEditSettings>
                                                                </SettingsEditing>
                                                            </dx:ASPxGridView>
                                                        </dx:PanelContent>
                                                    </PanelCollection>
                                                </dx:ASPxCallbackPanel>
                                                <dx:ASPxPopupControl ID="PopRemindORDaily" ClientInstanceName="PopRemindORDaily" runat="server" AllowDragging="true" PopupHorizontalAlign="WindowCenter" 
                                                    PopupVerticalAlign="WindowCenter" OnWindowCallback="PopRemindORDaily_WindowCallback" ShowHeader="true" HeaderText="Reminder" CloseAction="CloseButton" Theme="iOS" Width="600">
                                                    <ContentCollection>
                                                        <dx:PopupControlContentControl>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="col-md-1"></div>
                                                                    <div class="col-md-10">
                                                                        <div class="panel">
                                                                            <div class="panel-header">
                                                                                <h2 class="w-500 alert bg-red">Additional Receipiens (Please becareful, Additional Receipiens will received all selected Receipt)</h2>
                                                                            </div>
                                                                            <div class="panel-content">
                                                                                <table style="width:100%">
                                                                                    <tr>
                                                                                        <td colspan="2">
                                                                                            <p class="alert bg-blue">Additional Receipiens:</p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <dx:ASPxCheckBox ID="cbToCC_ORDaily" ClientInstanceName="cbToCC_ORDaily" runat="server" Theme="iOS" Text="cc Email" > 
                                                                                            </dx:ASPxCheckBox>
                                                                                        </td>
                                                                                        <td>
                                                                                            <dx:ASPxTextBox ID="tbToCC_ORDaily" ClientInstanceName="tbToCC_ORDaily" Enabled="true" runat="server" Width="250px" Theme="iOS" />
                                                                                            <span style="font-size:x-small">use semicolon (;) for more than 1</span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <dx:ASPxCheckBox ID="cbToBCC_ORDaily" ClientInstanceName="cbToBCC_ORDaily" runat="server" Theme="iOS" Text="bcc Email" > 
                                                                                            </dx:ASPxCheckBox>
                                                                                        </td>
                                                                                        <td>
                                                                                            <dx:ASPxTextBox ID="tbToBCC_ORDaily" ClientInstanceName="tbToBCC_ORDaily" Enabled="true" runat="server" Width="250px" Theme="iOS" />
                                                                                            <span style="font-size:x-small">use semicolon (;) for more than 1</span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="2">
                                                                                            <dx:ASPxButton ID="btnReminderSetORDaily" runat="server" Text="Send" OnClick="btnReminderSetORDaily_Click" CssClass="btn btn-blue" >
                                                                                            </dx:ASPxButton>
                                                                                            <dx:ASPxButton ID="btnReminderCancelORDaily" ClientInstanceName="btnReminderCancelORDaily" runat="server" Text="Cancel" CssClass="btn btn-danger" OnClick="btnReminderCancelORDaily_Click">
                                                                                            </dx:ASPxButton>
                                                                                            <asp:Label ID="lblWarn_ORDaily" runat="server" ForeColor="Red" Visible="false" Font-Size="10pt" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-1"></div>
                                                                </div>
                                                            </div>
                                                        </dx:PopupControlContentControl>
                                                    </ContentCollection>
                                                </dx:ASPxPopupControl>
                                                <asp:SqlDataSource ID="dsORDaily" runat="server" ConnectionString="<%$ ConnectionStrings:ereceiptConnectionString %>" 
                                                    ProviderName="<%$ ConnectionStrings:ereceiptConnectionString.ProviderName %>"
                                                    UpdateCommand="UPDATE tblEmailTrx SET EmailWs = @EmailWS , OtherEmail = @OtherEmail
                                                                    WHERE receiptID = @IDReceipt;">
                                                    <UpdateParameters>
                                                        <asp:Parameter Name="EmailWS" />
                                                        <asp:Parameter Name="OtherEmail" />
                                                        <asp:Parameter Name="IDReceipt" />
                                                    </UpdateParameters>
                                                </asp:SqlDataSource>
                                                <asp:SqlDataSource ID="dsDetailORDaily" runat="server" ConnectionString="<%$ ConnectionStrings:cmsidConnectionString %>" 
                                                    ProviderName="<%$ ConnectionStrings:cmsidConnectionString.ProviderName %>"></asp:SqlDataSource>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <dx:TabPage Text="OR History">
                        <ContentCollection>
                            <dx:ContentControl>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel-group panel-accordion" id="Div1">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4>
                                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                                            Filter
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseTwo" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                BL# / Receipt#
                                                            </div>
                                                            <div class="col-md-4">
                                                                <dx:ASPxTextBox ID="txtBLFilterORHistory" runat="server" ClientInstanceName="txtBLFilterORHistory" Theme="iOS" Width="100%" />
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                Date Time 
                                                            </div>
                                                            <div class="col-md-4">
                                                                <dx:ASPxDateEdit ID="datetimeORHistory" runat="server" Theme="iOS" EditFormat="DateTime" EditFormatString="MM/dd/yyyy HH:MM:ss" Width="190" OnInit="datetimeORHistory_Init">
                                                                    <TimeSectionProperties Visible="true">
                                                                        <TimeEditProperties EditFormatString="HH:MM:ss" AllowMouseWheel="true" />
                                                                    </TimeSectionProperties>
                                                                    <CalendarProperties>
                                                                        <FastNavProperties Enabled="true" />
                                                                    </CalendarProperties>
                                                                </dx:ASPxDateEdit>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <dx:ASPxButton ID="btnSubmitFilterORHistory" runat="server" Text="Search" Theme="iOS" OnClick="btnSubmitFilterORHistory_Click" AutoPostBack="false" />
                                                                &nbsp;&nbsp;
                                                                <dx:ASPxButton ID="btnClearFilterORHistory" runat="server" Text="Clear" Theme="iOS" OnClick="btnClearFilterORHistory_Click" AutoPostBack="false" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>        
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row m-b-5">
                                            <div class="col-lg-2">
                                                <dx:ASPxButton ID="btnSendReceiptORHistory" runat="server" Text="Send Receipt" Theme="iOS" OnClick="btnSendReceiptORHistory_Click" AutoPostBack="false" />
                                            </div>
                                            <div class="col-lg-10">
                                                <dx:ASPxLabel ID="lblSubmitWarningORHistory" runat="server" CssClass="label label-danger" Visible="false" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <dx:ASPxCallbackPanel ID="cbpORHistory" ClientInstanceName="cbpORDaily" runat="server" Width="100%" Theme="iOS">
                                                    <PanelCollection>
                                                        <dx:PanelContent>
                                                            <dx:ASPxGridView ID="gvORHistory" ClientInstanceName="gvORDaily" runat="server" KeyFieldName="IDReceipt" Width="100%" 
                                                                Theme="Aqua" DataSourceID="dsORHistory">
                                                                <Columns>
                                                                    <dx:GridViewCommandColumn ShowSelectCheckbox="true" SelectAllCheckboxMode="AllPages" VisibleIndex="0" FixedStyle="Left" />
                                                                    <dx:GridViewDataTextColumn Caption="Receipt Number" FieldName="IDReceipt" Name="IDReceipt" 
                                                                        VisibleIndex="1" Width="175px" ReadOnly="True" Visible="true" FixedStyle="Left">
                                                                        <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                                        <CellStyle Font-Size="12px">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="BL#" FieldName="BLNUMBER" Name="BLNUMBER" 
                                                                        VisibleIndex="2" Width="175px" ReadOnly="True" Visible="true" FixedStyle="Left">
                                                                        <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                                        <CellStyle Font-Size="12px">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="Invoice #" FieldName="InvoiceNumber" Name="InvoiceNumber" 
                                                                        VisibleIndex="3" Width="175px" ReadOnly="True" Visible="true">
                                                                        <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                                        <CellStyle Font-Size="10px">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="Consignee Name" FieldName="CNEE" Name="CNEE" 
                                                                        VisibleIndex="4" Width="200px" ReadOnly="True" Visible="true">
                                                                        <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                                        <CellStyle Font-Size="12px">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataDateColumn Caption="Receipt Date" Name="ReceiptDate"
                                                                        FieldName="ReceiptDate" VisibleIndex="5" Width="150px" ReadOnly="True">
                                                                        <PropertiesDateEdit DisplayFormatString="dd-MMM-yyyy hh:mm:ss">
                                                                        </PropertiesDateEdit>
                                                                        <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                                        <CellStyle Font-Size="12px" Wrap="False" HorizontalAlign="Center">
                                                                        </CellStyle>
                                                                        <SettingsHeaderFilter Mode="DateRangeCalendar" />
                                                                    </dx:GridViewDataDateColumn>
                                                                    <dx:GridViewDataTextColumn Caption="Curr" FieldName="Currency" Name="Currency" 
                                                                        VisibleIndex="6" Width="80px" ReadOnly="True" Visible="true">
                                                                        <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                                        <CellStyle Font-Size="12px">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataSpinEditColumn Caption="Amount" FieldName="Amount" 
                                                                        Name="Amount"  VisibleIndex="7" Width="118px" ReadOnly="True">
                                                                        <PropertiesSpinEdit DisplayFormatString="N1"></PropertiesSpinEdit>
                                                                        <HeaderStyle Font-Size="12px" HorizontalAlign="Center" Wrap="True" />
                                                                        <CellStyle Font-Size="12px" Wrap="False"/>
                                                                    </dx:GridViewDataSpinEditColumn>
                                                                    <dx:GridViewDataTextColumn Caption="Type" FieldName="TYPE" Name="TYPE" ShowInCustomizationForm="True"  
                                                                        VisibleIndex="8" Width="80px" ReadOnly="true" Visible="true" Settings-AllowHeaderFilter="True" >
<Settings AllowHeaderFilter="True"></Settings>

                                                                        <SettingsHeaderFilter Mode="CheckedList" />
                                                                        <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                                        <CellStyle Font-Size="12px" HorizontalAlign="Center">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="Payment Method" FieldName="PaymentMethodDSP" Name="PaymentMethodDSP" 
                                                                        VisibleIndex="9" Width="175px" ReadOnly="True" Visible="true" Settings-AllowHeaderFilter="True">
<Settings AllowHeaderFilter="True"></Settings>

                                                                        <SettingsHeaderFilter Mode="CheckedList" />
                                                                        <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                                        <CellStyle Font-Size="12px" HorizontalAlign="Center">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataMemoColumn Caption="Remarks" FieldName="Remarks" 
                                                                        Name="Remarks" VisibleIndex="10" Width="250px" ReadOnly="True">
                                                                        <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                                        <CellStyle Font-Size="10px" Wrap="True" />
                                                                    </dx:GridViewDataMemoColumn>
                                                                    <dx:GridViewDataMemoColumn Caption="Charge Code" FieldName="ChargeCode" 
                                                                        Name="ChargeCode" VisibleIndex="11" Width="250px" ReadOnly="True">
                                                                        <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                                        <CellStyle Font-Size="10px" Wrap="True" />
                                                                    </dx:GridViewDataMemoColumn>
                                                                    <dx:GridViewDataTextColumn Caption="Receipt By" FieldName="username" Name="username" 
                                                                        VisibleIndex="12" Width="95px" ReadOnly="True" Visible="true" Settings-AllowHeaderFilter="True">
                                                                        <Settings AllowHeaderFilter="True"></Settings>
                                                                        <SettingsHeaderFilter Mode="CheckedList" />
                                                                        <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                                        <CellStyle Font-Size="12px">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="REFFNO" FieldName="REFFNO" Name="REFFNO" 
                                                                        VisibleIndex="13" Width="100px" ReadOnly="True" Visible="true">
                                                                        <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                                        <CellStyle Font-Size="12px">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="Flag Send" FieldName="FlagSend" Name="FlagSend" 
                                                                        VisibleIndex="14" Width="80px" ReadOnly="True" Visible="true">
                                                                        <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                                        <CellStyle Font-Size="12px">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="Latest Send" FieldName="LatestSend" Name="LatestSend" 
                                                                        VisibleIndex="15" Width="120px" ReadOnly="True" Visible="true">
                                                                        <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                                        <CellStyle Font-Size="12px">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="Latest By" FieldName="LatestBy" Name="LatestBy" 
                                                                        VisibleIndex="16" Width="120px" ReadOnly="True" Visible="true">
                                                                        <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                                        <CellStyle Font-Size="12px">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataMemoColumn Caption="Email From Lara" FieldName="EmailWS" Name="EmailWS" 
                                                                        VisibleIndex="17" Width="300px" Visible="true">
                                                                        <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                                        <CellStyle Font-Size="10px">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataMemoColumn>
                                                                    <dx:GridViewDataTextColumn Caption="Other Receipient" FieldName="OtherEmail" Name="OtherEmail" 
                                                                        VisibleIndex="18" Width="300px" Visible="true">
                                                                        <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                                        <CellStyle Font-Size="10px">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                </Columns>
                                                                <SettingsPager PageSize="15" ShowSeparators="True" AlwaysShowPager="True" >
                                                                    <PageSizeItemSettings Visible="true" Position="Right" Items="15, 20, 25, 30, 35, 40, 45, 50" />
                                                                </SettingsPager>
                                                                <SettingsBehavior ColumnResizeMode="Control" ConfirmDelete="True" FilterRowMode="Auto" />
                                                                <Settings ShowFooter="True" VerticalScrollableHeight="450" 
                                                                    VerticalScrollBarMode="Visible" HorizontalScrollBarMode="Auto"
                                                                    ShowFilterBar="Auto" ShowFilterRow="true" ShowFilterRowMenu="true" ShowGroupPanel="True" ShowHeaderFilterButton="false" />
                                                                <SettingsLoadingPanel Mode="ShowAsPopup" />
                                                                <SettingsEditing Mode="Batch" BatchEditSettings-AllowValidationOnEndEdit="true" BatchEditSettings-StartEditAction="DblClick">
                                                                    <BatchEditSettings EditMode="Row" ></BatchEditSettings>
                                                                </SettingsEditing>
                                                            </dx:ASPxGridView>
                                                        </dx:PanelContent>
                                                    </PanelCollection>
                                                </dx:ASPxCallbackPanel>
                                                <dx:ASPxPopupControl ID="PopRemindORHistory" ClientInstanceName="PopRemindORHistory" runat="server" AllowDragging="true" PopupHorizontalAlign="WindowCenter" 
                                                    PopupVerticalAlign="WindowCenter" OnWindowCallback="PopRemindORHistory_WindowCallback" ShowHeader="true" HeaderText="Reminder OR History" CloseAction="CloseButton" Theme="iOS" Width="600">
                                                    <ContentCollection>
                                                        <dx:PopupControlContentControl>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="col-md-1"></div>
                                                                    <div class="col-md-10">
                                                                        <div class="panel">
                                                                            <div class="panel-header">
                                                                                <h2 class="w-500 alert bg-red">Additional Receipiens (Please becareful, Additional Receipiens will received all selected Receipt)</h2>
                                                                            </div>
                                                                            <div class="panel-content">
                                                                                <table style="width:100%">
                                                                                    <tr>
                                                                                        <td colspan="2">
                                                                                            <p class="alert bg-blue">Additional Receipiens:</p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <dx:ASPxCheckBox ID="cbToCC_ORHistory" ClientInstanceName="cbToCC_ORHistory" runat="server" Theme="iOS" Text="cc Email" > 
                                                                                            </dx:ASPxCheckBox>
                                                                                        </td>
                                                                                        <td>
                                                                                            <dx:ASPxTextBox ID="tbToCC_ORHistory" ClientInstanceName="tbToCC_ORHistory" Enabled="true" runat="server" Width="250px" Theme="iOS" />
                                                                                            <span style="font-size:x-small">use semicolon (;) for more than 1</span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <dx:ASPxCheckBox ID="cbToBCC_ORHistory" ClientInstanceName="cbToBCC_ORHistory" runat="server" Theme="iOS" Text="bcc Email" > 
                                                                                            </dx:ASPxCheckBox>
                                                                                        </td>
                                                                                        <td>
                                                                                            <dx:ASPxTextBox ID="tbToBCC_ORHistory" ClientInstanceName="tbToBCC_ORHistory" Enabled="true" runat="server" Width="250px" Theme="iOS" />
                                                                                            <span style="font-size:x-small">use semicolon (;) for more than 1</span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="2">
                                                                                            <dx:ASPxButton ID="btnReminderSetORHistory" runat="server" Text="Send" OnClick="btnReminderSetORHistory_Click" CssClass="btn btn-blue" >
                                                                                            </dx:ASPxButton>
                                                                                            <dx:ASPxButton ID="btnReminderCancelORHistory" ClientInstanceName="btnReminderCancelORHistory" runat="server" Text="Cancel" CssClass="btn btn-danger" OnClick="btnReminderCancelORHistory_Click">
                                                                                            </dx:ASPxButton>
                                                                                            <asp:Label ID="lblWarn_ORHistory" runat="server" ForeColor="Red" Visible="false" Font-Size="10pt" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-1"></div>
                                                                </div>
                                                            </div>
                                                        </dx:PopupControlContentControl>
                                                    </ContentCollection>
                                                </dx:ASPxPopupControl>
                                                <asp:SqlDataSource ID="dsORHistory" runat="server" ConnectionString="<%$ ConnectionStrings:ereceiptConnectionString %>" 
                                                    ProviderName="<%$ ConnectionStrings:ereceiptConnectionString.ProviderName %>"
                                                    UpdateCommand="UPDATE tblEmailTrx SET EmailWs = @EmailWS , OtherEmail = @OtherEmail
                                                                    WHERE receiptID = @IDReceipt;">
                                                    <UpdateParameters>
                                                        <asp:Parameter Name="EmailWS" />
                                                        <asp:Parameter Name="OtherEmail" />
                                                        <asp:Parameter Name="IDReceipt" />
                                                    </UpdateParameters>
                                                </asp:SqlDataSource>
                                                <asp:SqlDataSource ID="dsDetailORHistory" runat="server" ConnectionString="<%$ ConnectionStrings:cmsidConnectionString %>" 
                                                    ProviderName="<%$ ConnectionStrings:cmsidConnectionString.ProviderName %>"></asp:SqlDataSource>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <dx:TabPage Text="Container Deposit" Visible="false">
                        <ContentCollection>
                            <dx:ContentControl>

                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                </TabPages>
            </dx:ASPxPageControl>
        </div>
    </div>
</asp:Content>

