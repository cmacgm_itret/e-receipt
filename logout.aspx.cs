﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace E_Receipt_v1_1
{
    public partial class logout : System.Web.UI.Page
    {
        string ConnectionString = ConfigurationManager.ConnectionStrings["ereceiptConnectionString"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            InsertData();
            Session.Clear();
            Session.RemoveAll();
            Response.Redirect("default.aspx");
        }

        public void InsertData()
        {
            MySqlConnection con = new MySqlConnection(ConnectionString);
            string ip1 = GetIPAddress();
            if (ip1 == "")
                ip1 = GetIPAdd();
            string ipName = Dns.GetHostName();
            string query = string.Format("INSERT INTO tblLogStat(logDate, logUsername, logVal1, logVal2) " +
                                "VALUES(NOW(), '{0}', 'Logout','{1}')", Session["username"].ToString(), ip1 + " : " + ipName);
            MySqlCommand cmd = new MySqlCommand(query, con);
            cmd.CommandType = CommandType.Text;
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            cmd.ExecuteNonQuery();
            con.Close();
        }

        protected string GetIPAddress()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }

            return context.Request.ServerVariables["REMOTE_HOST"];
        }

        protected string GetIPAdd()
        {
            string IPAddress = string.Empty;

            String strHostName = HttpContext.Current.Request.UserHostAddress.ToString();

            IPAddress = System.Net.Dns.GetHostAddresses(strHostName).GetValue(0).ToString();

            return IPAddress;
        }
    }
}