﻿using DevExpress.Web;
using MySql.Data.MySqlClient;
using SelectPdf;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace E_Receipt_v1_1
{
    public partial class MainGrid : System.Web.UI.Page
    {
        string ConnectionString = ConfigurationManager.ConnectionStrings["cmsidConnectionString"].ConnectionString;
        string ereceiptConnection = ConfigurationManager.ConnectionStrings["ereceiptConnectionString"].ConnectionString;
        int accessWebServiceORDaily = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            int ax = pcMainContent.ActiveTabIndex;
            if (!IsPostBack)
            {
                switch (ax)
                {
                    case 0:
                        GenerateORDaily();
                        break;

                    case 1:
                        GenerateORHistory();
                        break;
                }
            }

            switch (ax)
            {
                case 0:
                    GenerateORDaily();
                    break;

                case 1:
                    GenerateORHistory();
                    break;
            }
 
        }

        //================== webservice ==========================//
        public void getDataFromLara(string partnerCode, string shipcomp, string rId, int LaraType)
        {
            #region variable
            DataSet dsLara = new DataSet();
            DataTable dtRecord = new DataTable();
            string mType = string.Empty; List<string> mVal = new List<string>();
            string result = string.Empty;
            #endregion

            //string uri = "http://10.11.0.156:8080/CCWS/resources/partnercontact/partnercode/"+ partnerCode +"/countrycode/ID/shipcompcode/" + shipcomp;
            string uri = "http://10.11.1.245:8080/CCWS/resources/partnercontact/partnercode/" + partnerCode + "/countrycode/ID/shipcompcode/" + shipcomp;
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(uri);

            try
            {
                HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                dsLara.ReadXml(httpWebResponse.GetResponseStream());
                if (dsLara.Tables[0].Rows.Count > 0)
                {
                    dtRecord = dsLara.Tables["record"];
                    foreach (DataRow dr in dtRecord.Rows)
                    {
                        mType = dr["contact_num_type"].ToString();
                        if (mType == "EM")
                        {
                            mVal.Add(dr["contact_number"].ToString());
                        }
                    }
                }

                if (mVal.Count > 0)
                {
                    for (int i = 0; i < mVal.Count; i++)
                    {
                        if (result == string.Empty)
                        {
                            result = mVal[i].ToString().Trim() + "; ";
                        }
                        else if(!result.Contains(mVal[i].ToString()))
                        {
                            result += mVal[i].ToString().Trim() + "; ";
                        }
                    }

                    if (LaraType == 0)
                    {
                        UpdateEmailFromWS(rId, result);
                    }
                    else
                    {
                        UpdateEmailInfoFromWS(rId, result);
                    }
                    
                }
            }
            catch(Exception ex)
            {
                string msg = string.Empty;
                if (ex.ToString().Contains("404"))
                {
                    msg = "No Partner Code";
                }
                else
                {
                    msg = "Not Found on LARA";
                }

                if (LaraType == 0)
                {
                    UpdateEmailFromWS(rId, msg);
                }
                else
                {
                    UpdateEmailInfoFromWS(rId, msg);
                }
                //lblWarn_ORDaily.Visible = true;
                //lblWarn_ORDaily.Text = uri + "<br/>" + ex.ToString();
            }
        }

        protected void GenerateEmailFromWS(string val)
        {
            DataTable dt = dtGetData(val);
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string rID = string.Empty; string reffNo = string.Empty;
                    string partnerCode = string.Empty; string xReff = "0000";

                    #region get val
                    rID = dt.Rows[i]["IDReceipt"].ToString();
                    partnerCode = dt.Rows[i]["PartnerCode"].ToString();
                    try
                    {
                        reffNo = dt.Rows[i]["REFFNO"].ToString();
                    }
                    catch
                    {
                        reffNo = "";
                    }
                    #endregion

                    if (reffNo.Contains("CMA"))
                    {
                        xReff = "0001";
                    }
                    else if (reffNo.Contains("CNC"))
                    {
                        xReff = "0011";
                    }
                    else if (reffNo.Contains("ANL"))
                    {
                        xReff = "0002";
                    }
                    else
                    {
                        xReff = "0000";
                    }

                    #region find if receipt id already updated
                    DataTable dtPC = dtGetData("SELECT * FROM tblEmailTrx WHERE receiptID = '" + rID + "'");
                    if (dtPC.Rows.Count < 1)
                    {
                        getDataFromLara(partnerCode, xReff, rID, 0);
                    }
                    //else
                    //{
                    //    if (dtPC.Rows[0]["EmailWS"].ToString() == "Not Found on LARA" || dtPC.Rows[0]["EmailWS"].ToString() == "No Partner Code")
                    //        getDataFromLara(partnerCode, xReff, rID, 1);
                    //}
                    #endregion

                }
            }
        }

        //=================== OR Daily =============================//
        protected void btnClearFilterORDaily_Click(object sender, EventArgs e)
        {
            txtBLFilterORDaily.Text = "";
            GenerateORDaily();
        }

        protected void btnSubmitFilterORDaily_Click(object sender, EventArgs e)
        {
            if (txtBLFilterORDaily.Text != "")
            {
                lblSubmitWarningORDaily.Visible = false;
                GenerateORDaily();
            }
            else
            {
                lblSubmitWarningORDaily.Visible = true;
                lblSubmitWarningORDaily.Text = "Please type BL# or Receip ID on filter box";
                txtBLFilterORDaily.Focus();
            }
        }

        public void GenerateORDaily()
        {
            string query = string.Empty;
            query = string.Format("SELECT a.IDReceipt, a.Currency, a.Amount, a.PaymentMethodDSP, a.Remarks, b.ReceiptDate, c.username, \n " +
                                "   GROUP_CONCAT(DISTINCT(b.BLNUMBER)) BLNUMBER, GROUP_CONCAT(DISTINCT(b.REFFNO)) REFFNO, \n " +
                                "   GROUP_CONCAT(b.InvoiceNumber) InvoiceNumber, GROUP_CONCAT(DISTINCT(b.CNEE)) CNEE, \n " +
                                "   GROUP_CONCAT(b.ChargeCode) as ChargeCode, b.ActualCurr, SUM(b.ActualAmount) ActualAmount, b.TYPE, b.PartnerCode, \n " +
                                "   SUM(b.VatAmount) vatamount, GROUP_CONCAT(b.Address) Address, GROUP_CONCAT(DISTINCT(b.LocalInvoiceNumber)) LocalInvoice, \n " +
                                "   GROUP_CONCAT(DISTINCT(b.BankName)) BankName, \n " +
                                "   d.FlagSend, d.LatestSend, d.LatestBy, d.EmailWS, d.OtherEmail \n " +
                                "FROM cms_reengineering_id.receipt_payment_detail_daily a \n " +
                                "   LEFT JOIN cms_reengineering_id.otherreceiptdetail_daily b ON a.IDReceipt = b.OtherReceiptID \n " +
                                "   LEFT JOIN cms_reengineering_id.user_list c ON a.ReceiptBy = c.user_id \n " +
                                "   LEFT JOIN db_ereceipt.tblEmailTrx d ON a.IDReceipt = d.receiptID \n " +
                                "WHERE a.site = '{0}' AND a.deletestatus = 0 AND a.IDReceipt NOT LIKE 'IDID%' \n", Session["Site"].ToString());
                    //"GROUP BY OtherReceiptID;";
            if (txtBLFilterORDaily.Text == "")
            {
                //query = "CALL ORDaily_SP('" + Session["Site"].ToString() + "', 1, '')";
                query += "GROUP BY a.IDReceipt;";
            }
            else
            {
                //query = "CALL ORDaily_SP('" + Session["Site"].ToString() + "', 2, '"+ txtBLFilterORDaily.Text +"')";
                query += string.Format("AND (a.IDReceipt = '{0}' OR b.BLNUMBER = '{0}' OR b.InvoiceNumber = '{0}') \n GROUP BY a.IDReceipt;", txtBLFilterORDaily.Text);
            }

            //if (accessWebServiceORDaily == 0)
            //{
            //    //GenerateEmailFromWS(query);
            //    accessWebServiceORDaily += 1;
            //}

            GenerateEmailFromWS(query);

            dsORDaily.SelectCommand = query;
            gvORDaily.DataBind();
        }

        protected void btnSendReceiptORDaily_Click(object sender, EventArgs e)
        {
            List<object> selVal = gvORDaily.GetSelectedFieldValues(gvORDaily.KeyFieldName);
            int x = selVal.Count;
            if (x == 0)
            {
                lblSubmitWarningORDaily.Visible = true;
                lblSubmitWarningORDaily.Text = "Select Receipt to Generate!";
            }
            else
            {
                lblSubmitWarningORDaily.Visible = false;
                Session["ListReceiptIdORDaily"] = selVal;

                PopRemindORDaily.ShowOnPageLoad = true;
                #region Section Box
                cbToCC_ORDaily.Checked = false;
                cbToBCC_ORDaily.Checked = false;
                tbToCC_ORDaily.Text = "";
                tbToBCC_ORDaily.Text = "";
                #endregion
                //GenerateHTMLType1();
            }
        }

        protected void gvORDaily_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs e)
        {

        }

        protected void gvDtlORDaily_BeforePerformDataSelect(object sender, EventArgs e)
        {
            string keyVal = ASPxGridView.GetDetailRowKeyValue(sender as ASPxGridView).ToString();
            string query = string.Format("SELECT BLNUMBER, OtherReceiptID, ChargeCode, ActualCurr, ActualAmount, TYPE, InvoiceNumber, \n" +
                            "   CNEE, STATUS, IsPaid, ReceiptDate, Site, REFFNO, Remarks, Address, Updateby, \n" +
                            "   LocalInvoiceNumber, OFFSETType, BankName, vatamount, pph23, PartnerCode FROM otherreceiptdetail_daily \n" +
                            "WHERE OtherReceiptID = '{0}' \n" +
                            "UNION ALL \n" +
                            "SELECT BLNUMBER, OtherReceiptID, ChargeCode, ActualCurr, ActualAmount, TYPE, InvoiceNumber, \n" +
                            "   CNEE, STATUS, IsPaid, ReceiptDate, Site, REFFNO, Remarks, Address, Updateby, \n" +
                            "   LocalInvoiceNumber, OFFSETType, BankName, vatamount, pph23, PartnerCode FROM history_otherreceiptdetail\n" +
                            "WHERE OtherReceiptID = '{0}'", keyVal);
            dsDetailORDaily.SelectCommand = query;
            dsDetailORDaily.DataBind();
        }

        protected void gvDtlORDaily_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs e)
        {

        }

        protected void PopRemindORDaily_WindowCallback(object source, PopupWindowCallbackArgs e)
        {

        }

        protected void btnReminderSetORDaily_Click(object sender, EventArgs e)
        {
            if (cbToCC_ORDaily.Checked)
            {
                if (tbToCC_ORDaily.Text == string.Empty)
                {
                    lblWarn_ORDaily.Visible = true;
                    lblWarn_ORDaily.Text = "Input email address to CC.";
                }
            }
            else if (cbToBCC_ORDaily.Checked)
            {
                if (tbToBCC_ORDaily.Text == string.Empty)
                {
                    lblWarn_ORDaily.Visible = true;
                    lblWarn_ORDaily.Text = "Input email address to BCC.";
                }
            }
            else
            {
                try
                {
                    GenerateDataBeforeSendingEmailORDaily(tbToCC_ORDaily.Text, tbToBCC_ORDaily.Text);
                    PopRemindORDaily.ShowOnPageLoad = false;
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Successfuly Sent.');", true);
                    GenerateORDaily();
                }
                catch
                {
                    PopRemindORDaily.ShowOnPageLoad = false;
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Something wrong. Check Event Review.');", true);
                    GenerateORDaily();
                }
            }
        }

        protected void btnReminderCancelORDaily_Click(object sender, EventArgs e)
        {
            PopRemindORDaily.ShowOnPageLoad = false;
        }

        protected void GenerateDataBeforeSendingEmailORDaily(string mailCC, string mailBCC)
        {
            List<object> selVal = gvORDaily.GetSelectedFieldValues(gvORDaily.KeyFieldName);
            if (selVal.Count > 0)
            {
                #region variable
                int OR = 0; 
                //Header Variable
                string receiptID = string.Empty;
                string headAmount = string.Empty;
                string headCurrency = string.Empty;
                string headSaid = string.Empty;
                string headDate = string.Empty;
                string headReceived = string.Empty;
                string headUName = string.Empty;
                string headRefference = string.Empty;
                string headReffNo = string.Empty;
                //Details - OR
                List<string> orBL = new List<string>();
                List<string> orInv = new List<string>();
                List<string> orDesc = new List<string>();
                List<string> orAmount = new List<string>();
                string orCurr = string.Empty; string pCode = string.Empty;
                decimal orVAT = 0; decimal orPPH = 0; decimal orTotal = 0;
                #endregion
                for (int i = 0; i < selVal.Count; i++)
                {
                    OR = 0; orVAT = 0; orPPH = 0; orTotal = 0; headReceived = string.Empty;
                    orBL.Clear(); orInv.Clear(); orDesc.Clear(); orAmount.Clear(); pCode = string.Empty;
                    headReffNo = string.Empty;

                    DataTable dtReceiptHeader = dtGetHeaderForOR(selVal[i].ToString());
                    #region Get Header
                    if (dtReceiptHeader.Rows.Count > 0)
                    {
                        for (int j = 0; j < dtReceiptHeader.Rows.Count; j++)
                        {
                            receiptID = dtReceiptHeader.Rows[j]["IDReceipt"].ToString();
                            headAmount = Convert.ToDecimal(dtReceiptHeader.Rows[j]["Amount"]).ToString("N2");
                            headCurrency = dtReceiptHeader.Rows[j]["Currency"].ToString();
                            headSaid = dtReceiptHeader.Rows[j]["Said"].ToString();
                            headDate = dtReceiptHeader.Rows[j]["rd"].ToString();
                            headUName = dtReceiptHeader.Rows[j]["userN"].ToString();
                            headRefference = dtReceiptHeader.Rows[j]["Refference"].ToString();
                            headReffNo = dtReceiptHeader.Rows[j]["REFFNO"].ToString();
                        }
                    }
                    #endregion

                    DataTable dtReceiptDetails = dtGetDetailForOR(selVal[i].ToString());
                    #region Get Details
                    if (dtReceiptDetails.Rows.Count > 0)
                    {
                        for (int j = 0; j < dtReceiptDetails.Rows.Count; j++)
                        {
                            headReceived = dtReceiptDetails.Rows[j]["CNEE"].ToString();
                            orBL.Add(dtReceiptDetails.Rows[j]["BLNUMBER"].ToString());
                            orInv.Add(dtReceiptDetails.Rows[j]["InvoiceNumber"].ToString());
                            orDesc.Add(dtReceiptDetails.Rows[j]["ChargeCode"].ToString());
                            orCurr = dtReceiptDetails.Rows[j]["ActualCurr"].ToString();
                            orAmount.Add(Convert.ToDecimal(dtReceiptDetails.Rows[j]["ActualAmount"]).ToString("N2"));
                            orTotal = orTotal + Convert.ToDecimal(dtReceiptDetails.Rows[j]["ActualAmount"]);

                            int cVat = 0;

                            if (Convert.ToDecimal(dtReceiptDetails.Rows[j]["ActualAmount"]) != 0)
                            {
                                if (dtReceiptDetails.Rows[j]["vatamount"] is DBNull)
                                {
                                    orVAT = orVAT + 0;
                                    cVat = 0;
                                }
                                else
                                {
                                    orVAT = orVAT + Convert.ToDecimal(dtReceiptDetails.Rows[j]["vatamount"]);
                                    cVat = Convert.ToInt32(dtReceiptDetails.Rows[j]["vatamount"]);
                                }

                                orTotal = orTotal + orVAT; //exclude PPH23
                            }

                            if (cVat == 0)
                            {
                                orPPH = orPPH + 0;
                            }
                            else
                            {
                                if (dtReceiptDetails.Rows[j]["pph23"] is DBNull)
                                    orPPH = orPPH + 0;
                                else
                                    orPPH = orPPH + Convert.ToDecimal(dtReceiptDetails.Rows[j]["pph23"].ToString());
                            }

                            if (dtReceiptDetails.Rows[j]["PartnerCode"] is DBNull)
                            {
                                pCode = string.Empty;
                            }
                            else if (dtReceiptDetails.Rows[j]["PartnerCode"].ToString() == "")
                            {
                                pCode = string.Empty;
                            }
                            else
                            {
                                pCode = dtReceiptDetails.Rows[j]["PartnerCode"].ToString();
                            }

                        }
                    }
                    #endregion

                    GenerateHTMLType1(receiptID, string.Format("{0:N2}", headAmount), headCurrency, headSaid, headDate, headUName, headReceived, orBL, orInv, orDesc, orCurr, orAmount,
                            orTotal.ToString("N2"), orVAT.ToString("N2"), orPPH.ToString("N2"), pCode, headRefference, headReffNo, mailCC, mailBCC);
                }
            }
        }

        //=========================== OR History ===========================//

        protected void btnSubmitFilterORHistory_Click(object sender, EventArgs e)
        {
            if (txtBLFilterORHistory.Text == string.Empty && datetimeORHistory.Text == string.Empty)
            {
                lblSubmitWarningORHistory.Visible = true;
                lblSubmitWarningORHistory.Text = "Please type Receipt# / BL# / Inv# or Select Date Time to Filter";
                txtBLFilterORHistory.Focus();
            }
            else
            {
                lblSubmitWarningORHistory.Visible = false;
                GenerateORHistory();
            }
        }

        protected void btnClearFilterORHistory_Click(object sender, EventArgs e)
        {
            txtBLFilterORHistory.Text = "";
            datetimeORHistory.Text = "";
            GenerateORHistory();
        }

        protected void gvDtlORHistory_BeforePerformDataSelect(object sender, EventArgs e)
        {
            string keyVal = ASPxGridView.GetDetailRowKeyValue(sender as ASPxGridView).ToString();
            string query = string.Format("SELECT BLNUMBER, OtherReceiptID, ChargeCode, ActualCurr, ActualAmount, TYPE, InvoiceNumber, \n" +
                            "   CNEE, STATUS, IsPaid, ReceiptDate, Site, REFFNO, Remarks, Address, Updateby, \n" +
                            "   LocalInvoiceNumber, OFFSETType, BankName, vatamount, pph23, PartnerCode FROM otherreceiptdetail_daily \n" +
                            "WHERE OtherReceiptID = '{0}' \n" +
                            "UNION ALL \n" +
                            "SELECT BLNUMBER, OtherReceiptID, ChargeCode, ActualCurr, ActualAmount, TYPE, InvoiceNumber, \n" +
                            "   CNEE, STATUS, IsPaid, ReceiptDate, Site, REFFNO, Remarks, Address, Updateby, \n" +
                            "   LocalInvoiceNumber, OFFSETType, BankName, vatamount, pph23, PartnerCode FROM history_otherreceiptdetail\n" +
                            "WHERE OtherReceiptID = '{0}'", keyVal);
            dsDetailORHistory.SelectCommand = query;
            dsDetailORHistory.DataBind();
        }

        public void GenerateORHistory()
        {
            string query = string.Empty; string valDate = string.Empty; string valTime = string.Empty;
            int blVal = 0; 
            query = string.Format("SELECT a.IDReceipt, a.Currency, a.Amount, a.PaymentMethodDSP, b.ReceiptDate, a.Remarks, c.username, \n " +
                                "   GROUP_CONCAT(DISTINCT(b.BLNUMBER)) BLNUMBER, GROUP_CONCAT(DISTINCT(b.REFFNO)) REFFNO, \n " +
                                "   GROUP_CONCAT(b.InvoiceNumber) InvoiceNumber, GROUP_CONCAT(DISTINCT(b.CNEE)) CNEE, \n " +
                                "   GROUP_CONCAT(b.ChargeCode) as ChargeCode, b.ActualCurr, SUM(b.ActualAmount) ActualAmount, b.TYPE, b.PartnerCode, \n " +
                                "   SUM(b.VatAmount) vatamount, GROUP_CONCAT(b.Address) Address, GROUP_CONCAT(DISTINCT(b.LocalInvoiceNumber)) LocalInvoice, \n " +
                                "   GROUP_CONCAT(DISTINCT(b.BankName)) BankName, \n " +
                                "   d.FlagSend, d.LatestSend, d.LatestBy, d.EmailWS, d.OtherEmail \n " +
                                "FROM cms_reengineering_id.history_receipt_payment_detail a \n " +
                                "   LEFT JOIN cms_reengineering_id.history_otherreceiptdetail b ON a.IDReceipt = b.OtherReceiptID \n " +
                                "   LEFT JOIN cms_reengineering_id.user_list c ON a.ReceiptBy = c.user_id \n " +
                                "   LEFT JOIN db_ereceipt.tblEmailTrx d ON a.IDReceipt = d.receiptID \n " +
                                "WHERE a.site = '{0}' AND a.deletestatus = 0 AND a.IDReceipt NOT LIKE 'IDID%' \n", Session["Site"].ToString());
            if (txtBLFilterORHistory.Text != string.Empty)
            {
                query += string.Format("AND (a.IDReceipt = '{0}' OR BLNUMBER = '{0}' OR InvoiceNumber = '{0}') \n", txtBLFilterORHistory.Text);
                blVal = 1;
            }

            if (datetimeORHistory.Text != string.Empty)
            {
                string[] a = datetimeORHistory.Text.Split(' ');
                valDate = a[0].ToString();
                valTime = a[1].ToString();
                query += string.Format("AND DATE_FORMAT(DATE(a.receiptDate),'%m/%d/%Y') = '{0}' AND DATE_FORMAT(a.receiptDate,'%H:%i:%s') > '{1}' \n", valDate, valTime);
            }
            else
            {
                if (blVal == 0)
                {
                    query += "AND DATE(b.ReceiptDate) = DATE(DATE(NOW())-1) \n";
                }
            }

            query += "GROUP BY a.IDReceipt;";

            GenerateEmailFromWS(query);
            
            dsORHistory.SelectCommand = query;
            gvORHistory.DataBind();
        }

        protected void btnSendReceiptORHistory_Click(object sender, EventArgs e)
        {
            List<object> selVal = gvORHistory.GetSelectedFieldValues(gvORHistory.KeyFieldName);
            int x = selVal.Count;
            if (x == 0)
            {
                lblSubmitWarningORHistory.Visible = true;
                lblSubmitWarningORHistory.Text = "Select Receipt to Generate!";
            }
            else
            {
                lblSubmitWarningORHistory.Visible = false;
                Session["ListReceiptIdORHistory"] = selVal;

                PopRemindORHistory.ShowOnPageLoad = true;
                #region Section Box
                cbToCC_ORHistory.Checked = false;
                cbToBCC_ORHistory.Checked = false;
                tbToCC_ORHistory.Text = "";
                tbToBCC_ORHistory.Text = "";
                #endregion
                //GenerateHTMLType1();
            }
        }

        protected void datetimeORHistory_Init(object sender, EventArgs e)
        {
            //datetimeORHistory.Date = new DateTime(DateTime.Now.Year, DateTime.Now.Month - 1, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month - 1));
        }

        protected void PopRemindORHistory_WindowCallback(object source, PopupWindowCallbackArgs e)
        {

        }

        protected void btnReminderSetORHistory_Click(object sender, EventArgs e)
        {
            if (cbToCC_ORHistory.Checked)
            {
                if (tbToCC_ORHistory.Text == string.Empty)
                {
                    lblWarn_ORHistory.Visible = true;
                    lblWarn_ORHistory.Text = "Input email address to CC.";
                }
            }
            else if (cbToBCC_ORHistory.Checked)
            {
                if (tbToBCC_ORHistory.Text == string.Empty)
                {
                    lblWarn_ORHistory.Visible = true;
                    lblWarn_ORHistory.Text = "Input email address to BCC.";
                }
            }
            else
            {
                try
                {
                    GenerateDataBeforeSendingEmailORHistory(tbToCC_ORDaily.Text, tbToBCC_ORDaily.Text);
                    PopRemindORHistory.ShowOnPageLoad = false;
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Successfuly Sent.');", true);
                    GenerateORHistory();
                }
                catch
                {
                    PopRemindORHistory.ShowOnPageLoad = false;
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Something wrong. Check Event Review.');", true);
                    GenerateORHistory();
                }
            }
        }

        public void GenerateDataBeforeSendingEmailORHistory(string mailCC, string mailBCC)
        {
            List<object> selVal = gvORHistory.GetSelectedFieldValues(gvORHistory.KeyFieldName);
            if (selVal.Count > 0)
            {
                #region variable
                int OR = 0;
                //Header Variable
                string receiptID = string.Empty;
                string headAmount = string.Empty;
                string headCurrency = string.Empty;
                string headSaid = string.Empty;
                string headDate = string.Empty;
                string headReceived = string.Empty;
                string headUName = string.Empty;
                string headRefference = string.Empty;
                string headReffNo = string.Empty;
                //Details - OR
                List<string> orBL = new List<string>();
                List<string> orInv = new List<string>();
                List<string> orDesc = new List<string>();
                List<string> orAmount = new List<string>();
                string orCurr = string.Empty; string pCode = string.Empty;
                decimal orVAT = 0; decimal orPPH = 0; decimal orTotal = 0;
                #endregion
                for (int i = 0; i < selVal.Count; i++)
                {
                    OR = 0; orVAT = 0; orPPH = 0; orTotal = 0; headReceived = string.Empty;
                    orBL.Clear(); orInv.Clear(); orDesc.Clear(); orAmount.Clear(); pCode = string.Empty;
                    headReffNo = string.Empty;

                    DataTable dtReceiptHeader = dtGetHeaderForOR(selVal[i].ToString());
                    #region Get Header
                    if (dtReceiptHeader.Rows.Count > 0)
                    {
                        for (int j = 0; j < dtReceiptHeader.Rows.Count; j++)
                        {
                            receiptID = dtReceiptHeader.Rows[j]["IDReceipt"].ToString();
                            headAmount = Convert.ToDecimal(dtReceiptHeader.Rows[j]["Amount"]).ToString("N2");
                            headCurrency = dtReceiptHeader.Rows[j]["Currency"].ToString();
                            headSaid = dtReceiptHeader.Rows[j]["Said"].ToString();
                            headDate = dtReceiptHeader.Rows[j]["rd"].ToString();
                            headUName = dtReceiptHeader.Rows[j]["userN"].ToString();
                            headRefference = dtReceiptHeader.Rows[j]["Refference"].ToString();
                            headReffNo = dtReceiptHeader.Rows[j]["REFFNO"].ToString();
                        }
                    }
                    #endregion

                    DataTable dtReceiptDetails = dtGetDetailForOR(selVal[i].ToString());
                    #region Get Details
                    if (dtReceiptDetails.Rows.Count > 0)
                    {
                        for (int j = 0; j < dtReceiptDetails.Rows.Count; j++)
                        {
                            headReceived = dtReceiptDetails.Rows[j]["CNEE"].ToString();
                            orBL.Add(dtReceiptDetails.Rows[j]["BLNUMBER"].ToString());
                            orInv.Add(dtReceiptDetails.Rows[j]["InvoiceNumber"].ToString());
                            orDesc.Add(dtReceiptDetails.Rows[j]["ChargeCode"].ToString());
                            orCurr = dtReceiptDetails.Rows[j]["ActualCurr"].ToString();
                            orAmount.Add(Convert.ToDecimal(dtReceiptDetails.Rows[j]["ChargeAmount"]).ToString("N2"));
                            orTotal = orTotal + Convert.ToDecimal(dtReceiptDetails.Rows[j]["ChargeAmount"].ToString());

                            int cVat = 0;

                            if (dtReceiptDetails.Rows[j]["vatamount"] is DBNull)
                            {
                                orVAT = orVAT + 0;
                                cVat = 0;
                            }
                            else
                            {
                                orVAT = orVAT + Convert.ToDecimal(dtReceiptDetails.Rows[j]["vatamount"]);
                                cVat = Convert.ToInt32(dtReceiptDetails.Rows[j]["vatamount"]);
                            }

                            if (cVat == 0)
                            {
                                orPPH = orPPH + 0;
                            }
                            else
                            {
                                if (dtReceiptDetails.Rows[j]["pph23"] is DBNull)
                                    orPPH = orPPH + 0;
                                else
                                    orPPH = orPPH + Convert.ToDecimal(dtReceiptDetails.Rows[j]["pph23"].ToString());
                            }

                            if (dtReceiptDetails.Rows[j]["PartnerCode"] is DBNull)
                            {
                                pCode = string.Empty;
                            }
                            else if (dtReceiptDetails.Rows[j]["PartnerCode"].ToString() == "")
                            {
                                pCode = string.Empty;
                            }
                            else
                            {
                                pCode = dtReceiptDetails.Rows[j]["PartnerCode"].ToString();
                            }

                            orTotal = orTotal + orVAT; //exclude PPH23
                        }
                    }
                    #endregion

                    GenerateHTMLType1(receiptID, string.Format("{0:N2}", headAmount), headCurrency, headSaid, headDate, headUName, headReceived, orBL, orInv, orDesc, orCurr, orAmount,
                            orTotal.ToString("N2"), orVAT.ToString("N2"), orPPH.ToString("N2"), pCode, headRefference, headReffNo, mailCC, mailBCC);
                }
            }
        }

        protected void btnReminderCancelORHistory_Click(object sender, EventArgs e)
        {
            PopRemindORHistory.ShowOnPageLoad = false;
        }

        //========================== Master DB CRUD =============================//
        DataTable dtGetHeaderForOR(string val)
        {
            DataTable dt = new DataTable();
            MySqlConnection conn = new MySqlConnection(ConnectionString);
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            string sQuery = string.Format("SELECT ID, IDReceipt, Currency, Amount, Said, a.STATUS, PaymentMethodDSP, TYPE, \n" +
                        "ReceiptDate, DATE(ReceiptDate) as rd, Description, ReceiptBy, CONCAT(c.user_first_name, ' ', c.user_last_name) AS 'userN', Remarks, Invoice, ReceiptCategory, b.receiptCategoryName, \n" +
                        "a.Site, REFFNO, Deletestatus, Cancelby, Canceldate, CancelReason, \n" +
                        "Updateby, source, TransferAmount, BankDate, OFFSETType, Refference, GroupOrder \n" +
                    "FROM receipt_payment_detail_daily a \n" +
                    "LEFT JOIN receiptcategory b ON a.ReceiptCategory = b.receiptCode \n " +
                    "LEFT JOIN user_list c ON a.ReceiptBy = c.user_id \n " +
                    "WHERE IDReceipt = '{0}' \n" +
                    "UNION \n" +
                    "SELECT ID, IDReceipt, Currency, Amount, Said, a.STATUS, PaymentMethodDSP, TYPE, \n" +
                        "ReceiptDate, DATE(ReceiptDate) as rd, Description, ReceiptBy, CONCAT(c.user_first_name, ' ', c.user_last_name) AS 'userN', Remarks, Invoice, ReceiptCategory, b.receiptCategoryName, \n" +
                        "a.Site, REFFNO, Deletestatus, Cancelby, Canceldate, CancelReason, \n" +
                        "Updateby, source, TransferAmount, BankDate, OFFSETType, Refference, GroupOrder \n" +
                    "FROM history_receipt_payment_detail a \n" +
                    "LEFT JOIN receiptcategory b ON a.ReceiptCategory = b.receiptCode \n " +
                    "LEFT JOIN user_list c ON a.ReceiptBy = c.user_id \n " +
                    "WHERE IDReceipt = '{0}'" +
                    ";", val);
            
            MySqlCommand cmd = new MySqlCommand(sQuery, conn);
            MySqlDataAdapter da = new MySqlDataAdapter(cmd);
            da.Fill(dt);
            conn.Close();
            conn.Dispose();
            return dt;
        }

        DataTable dtGetDetailForOR(string val)
        {
            MySqlConnection conn = new MySqlConnection(ConnectionString);
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            string xx = string.Empty;
            string sQuery = string.Format("SELECT BLNUMBER, OtherReceiptID, ChargeCode, ChargeCurr, ChargeAmount, ActualCurr, ActualAmount, TYPE, InvoiceNumber, \n " +
                                "CNEE, STATUS, IsPaid, ReceiptDate, Site, REFFNO, Remarks, Deletestatus, Address, Updateby, source, \n" +
                                "LocalInvoiceNumber, OFFSETType, BankName, vatamount, pph23, PartnerCode FROM otherreceiptdetail_daily \n " +
                                "WHERE OtherReceiptID = '{0}' \n" +
                                "UNION \n " +
                                "SELECT BLNUMBER, OtherReceiptID, ChargeCode, ChargeCurr, ChargeAmount, ActualCurr, ActualAmount, TYPE, InvoiceNumber, \n " +
                                    "CNEE, STATUS, IsPaid, ReceiptDate, Site, REFFNO, Remarks, Deletestatus, Address, Updateby, source, \n " +
                                    "LocalInvoiceNumber, OFFSETType, BankName, vatamount, pph23, PartnerCode FROM history_otherreceiptdetail \n " +
                            "WHERE OtherReceiptID = '{0}';", val);
            MySqlCommand cmd = new MySqlCommand(sQuery, conn);
            MySqlDataAdapter da = new MySqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            conn.Close();
            conn.Dispose();
            return dt;
        }

        DataTable dtFindRefference(string val, string reffno)
        {
            DataTable dt = new DataTable();
            MySqlConnection conn = new MySqlConnection(ConnectionString);
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            string sQuery = string.Format("SELECT Refference, Bankdate, BLNUMBER, INVOICE, Amount, Remarks, Appliedfor, \n " +
                                            "AppliedDate, BankName, Description, ReleasedState, Idreceipt, \n " +
                                            "DeleteStatus, TYPE, GroupOrder, Currency, ChargeCode, b.BrandAccount \n " +
                                            "FROM bankstatementdetails a \n " +
                                            "LEFT JOIN tblBrandAccountSetup b ON TRIM(a.BankName) = b.BrandBank \n " +
                                            "WHERE Idreceipt = '{0}' \n" +
                                            "AND b.BrandName = SUBSTRING('{1}', 1, 3)", val, reffno);
            MySqlCommand cmd = new MySqlCommand(sQuery, conn);
            MySqlDataAdapter da = new MySqlDataAdapter(cmd);
            da.Fill(dt);
            conn.Close();
            conn.Dispose();
            return dt;
        }

        DataTable dtFindPartnerEmail(string val)
        {
            DataTable dt = new DataTable();
            MySqlConnection conn = new MySqlConnection(ereceiptConnection);
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            string sQuery = string.Format("SELECT receiptID, FlagSend, LatestSend, LatestBy, EmailWS, OtherEmail, GeneratedDate FROM tblEmailTrx WHERE receiptID = '{0}'", val);
            MySqlCommand cmd = new MySqlCommand(sQuery, conn);
            MySqlDataAdapter da = new MySqlDataAdapter(cmd);
            da.Fill(dt);
            conn.Close();
            conn.Dispose();
            return dt;
        }

        DataTable dtGetData(string val)
        {
            DataTable dt = new DataTable();
            MySqlConnection conn = new MySqlConnection(ereceiptConnection);
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            MySqlCommand cmd = new MySqlCommand(val, conn);
            MySqlDataAdapter da = new MySqlDataAdapter(cmd);
            da.Fill(dt);
            conn.Close();
            conn.Dispose();
            return dt;
        }

        protected void StoringEventReview(string logReceipt, string logBL, string logInv, string logTo, string logCc, string logBcc, string logStatus, string logDesc)
        {
            MySqlConnection conn2 = new MySqlConnection(ereceiptConnection);
            string logtrans;
            logtrans = "INSERT INTO tblLogSentEmail(logDate, logSentBy, logReceiptID, logBLNumber, logInvNumber,  \n " +
                        "logSentTo, logSentCc, logSentBcc, logStatus, logDesc) \n " +
                        "VALUES(NOW(), @usern, @receipt, @BLNum, @InvNum, @logTo, @logCc, @logBcc, @logStatus, @logDesc);";
            if (conn2.State == ConnectionState.Closed)
            {
                conn2.Open();
            }
            MySqlCommand cmdIns = new MySqlCommand(logtrans, conn2);
            cmdIns.Parameters.Add(new MySqlParameter("@receipt", logReceipt));
            cmdIns.Parameters.Add(new MySqlParameter("@usern", Session["username"].ToString()));
            cmdIns.Parameters.Add(new MySqlParameter("@BLNum", logBL));
            cmdIns.Parameters.Add(new MySqlParameter("@InvNum", logInv));
            cmdIns.Parameters.Add(new MySqlParameter("@logTo", logTo));
            cmdIns.Parameters.Add(new MySqlParameter("@logCc", logCc));
            cmdIns.Parameters.Add(new MySqlParameter("@logBcc", logBcc));
            cmdIns.Parameters.Add(new MySqlParameter("@logStatus", logStatus));
            cmdIns.Parameters.Add(new MySqlParameter("@logDesc", logDesc));
            cmdIns.ExecuteNonQuery();
            conn2.Close();
            conn2.Dispose();
        }

        protected void UpdateEmailFromWS(string rID, string mWS)
        {
            MySqlConnection conn2 = new MySqlConnection(ereceiptConnection);
            string logtrans;
            logtrans = "INSERT INTO tblEmailTrx(receiptID, FlagSend, EmailWS, OtherEmail, GeneratedDate)  \n " +
                        "VALUES('"+ rID +"', 0, '"+ mWS +"', '', NOW());";
            if (conn2.State == ConnectionState.Closed)
            {
                conn2.Open();
            }
            MySqlCommand cmdIns = new MySqlCommand(logtrans, conn2);
            cmdIns.ExecuteNonQuery();
            conn2.Close();
            conn2.Dispose();
        }

        protected void UpdateEmailInfoFromWS(string rID, string mWS)
        {
            MySqlConnection conn2 = new MySqlConnection(ereceiptConnection);
            string logtrans;
            logtrans = "UPDATE tblEmailTrx SET EmailWS = '"+ mWS +"', GeneratedDate = NOW() \n " +
                        "WHERE receiptID = '"+ rID +"';";
            if (conn2.State == ConnectionState.Closed)
            {
                conn2.Open();
            }
            MySqlCommand cmdIns = new MySqlCommand(logtrans, conn2);
            cmdIns.ExecuteNonQuery();
            conn2.Close();
            conn2.Dispose();
        }

        protected void UpdateSendingMailInfo(string receiptID, string uName)
        {
            MySqlConnection conn2 = new MySqlConnection(ereceiptConnection);
            string logtrans;
            logtrans = "UPDATE tblEmailTrx set FlagSend = 1, LatestSend = NOW(), LatestBy = @username \n " +
                        "WHERE receiptID = @receiptID";
            if (conn2.State == ConnectionState.Closed)
            {
                conn2.Open();
            }
            MySqlCommand cmdIns = new MySqlCommand(logtrans, conn2);
            cmdIns.Parameters.Add(new MySqlParameter("@receiptID", receiptID));
            cmdIns.Parameters.Add(new MySqlParameter("@username", uName));
            cmdIns.ExecuteNonQuery();
            conn2.Close();
            conn2.Dispose();
        }

        //========================= Generating HTML ===============================//
        const string uploadFolder = "~/GeneratedFiles/";
        public void GenerateHTMLType1(string headerReceipt, string headerAmt, string headerCurr, string headerSa, string headerDt, string headCashier, string detailReceiver,
        List<string> detailBL, List<string> detailInv, List<string> detailDesc, string detailCurr, List<string> detailAmount, string detailTotal, string detailVat, string detailPph,
        string partnerCode, string reff, string reffno, string mailCC, string mailBCC)
        {
            string headVal = string.Empty;
            string blNum = DesignForEventReview(detailBL);
            string invNum = DesignForEventReview(detailInv);
            string mailTo = string.Empty;
            
            
            string detailVal = string.Empty;

            DataTable dtReff = dtFindRefference(headerReceipt, reffno);

            headVal = "<html> \n";
            #region Head of HTML
            headVal += "    <head runat=\"server\"> \n";
            headVal += "        <style type=\"text/css\"> \n";
            headVal += "            @page{ \n" +
                                            "size:A4 portrait; \n" +
                                            "margin:1.25cm; \n" +
                                            "font-family: Calibri !important; \n" +
                                            "font-size:20pt; \n" +
                                        "} \n";
            headVal += "            .tbl{ \n" +
                                            "border: 1pt solid #000 !important; \n" +
                                            "border-collapse:collapse; \n" +
                                            "width:100%; \n" +
                                        "} \n";
            headVal += "            .tbl2{ \n" +
                                            "border-collapse:collapse; \n" +
                                            "width:90%; \n" +
                                            "margin-left:20px; \n " +
                                            "margin-bottom: 25px;\n " +
                                            "font-size: 10pt; \n" +
                                            "font-family: Calibri !important; \n" +
                                        "} \n";
            headVal += "            .tbl2 td, th{ \n" +
                                            "border: 1px solid black; \n" +
                                            "text-align:center;\n" +
                                        "}\n" +
                                        "\n" +
                                     ".tbl2 p{ \n" +
                                        "font-size:10pt; \n" +
                                     "} \n" +
                                              "\n" +
                                        ".tbl2 th{  \n" +
                                            "height:25px;\n" +
                                        "} \n";
            headVal += "            .tbl3{ \n" +
                                            "border: 1pt solid #000 !important; \n" +
                                            "border-collapse:collapse; \n" +
                                            "width:25%;\n" +
                                            "margin-top: -1px;\n" +
                                            "font-family: Calibri !important; \n" +
                                        "}\n" +
                                              "\n" +
                                        ".tbl3 td{\n" +
                                            "height:75px;\n" +
                                            "text-align:center;\n" +
                                            "font-weight:600;\n" +
                                        "}\n";
            headVal += "            p{\n" +
                                        "font-family: Calibri !important; \n" +
                                    "}\n" +
                                        "\n" +
                                    ".BrandAddress {\n" +
                                        "display:table-row;\n" +
                                    "}\n" +
                                          "\n" +
                                    ".Receipt1 p{\n" +
                                        "text-align:center;\n" +
                                        "font-weight:700;\n" +
                                        "font-size: 15pt; \n" +
                                    "}\n" +
                                          "\n" +
                                    ".trBottomBorder{\n" +
                                        "border-bottom:1pt solid #000;\n" +
                                        "height: 25px;\n " +
                                    "}\n" +
                                          "\n" +
                                    ".cellVal1{\n" +
                                        "font-weight:600;\n" +
                                        "text-wrap:normal;\n" +
                                    "}\n";
            headVal += "        </style> \n" +
                            "</head>";
            #endregion

            #region body of HTML
            headVal += "    <body> \n";
            headVal += "        <form id=\"form1\" runat=\"server\"> \n";
            headVal += "            <table style=\"width:100%\"> \n";
            
            #region Details of Receipt
            headVal += "                <tr> \n";
            headVal += "                    <td colspan=\"2\"> \n";
            headVal += "                        <table class=\"tbl\"> \n";
            headVal += "                            <tr class=\"trBottomBorder\"> \n";
            #region details: Receipt Title
            headVal += "                                <td class=\"Receipt1\" colspan=\"3\"> \n" +
                                                            "<p>BANK RECEIPT VOUCHER</p>\n" +
                                                        "</td>\n";
            #endregion
            headVal += "                            </tr> \n";
            headVal += "                            <tr class=\"trBottomBorder\"> \n";
            headVal += "                                <td style=\"width:50%\">&nbsp;</td> \n";
            headVal += "                                <td style=\"width:50%\" colspan=\"2\"> \n";
            headVal += "                                    <table> \n";
            #region details: Voucher No
            headVal += "                                        <tr> \n " +
                                                                    "<td>\n " +
                                                                        "<p>Voucher No</p>\n " +
                                                                    "</td>\n " +
                                                                    "<td>\n " +
                                                                        "<p>:</p>\n " +
                                                                    "</td>\n " +
                                                                    "<td>\n " +
                                                                        "<p>\n " +
                                                                            headerReceipt + "\n " +
                                                                        "</p>\n " +
                                                                    "</td>\n " +
                                                                "</tr>";
            #endregion
            #region details: Date
            headVal += "                                        <tr> \n";
            headVal += "                                            <td>\n " +
                                                                        "<p>Date</p>\n " +
                                                                    "</td>\n " +
                                                                    "<td>\n " +
                                                                        "<p>:</p>\n " +
                                                                    "</td>\n " +
                                                                    "<td>\n " +
                                                                        "<p>\n " +
                                                                            headerDt + "\n " +
                                                                        "</p>\n " +
                                                                    "</td>";
            headVal += "                                        </tr> \n";
            #endregion
            #region details: REFF NO
            headVal += "                                        <tr> \n";
            headVal += "                                            <td>\n " +
                                                                        "<p>REFF NO</p>\n " +
                                                                    "</td>\n " +
                                                                    "<td>\n " +
                                                                        "<p>:</p>\n " +
                                                                    "</td>\n " +
                                                                    "<td>\n " +
                                                                        "<p>\n " +
                                                                            reffno + "\n " +
                                                                        "</p>\n " +
                                                                    "</td>";
            headVal += "                                        </tr> \n";
            #endregion
            headVal += "                                    </table> \n";
            headVal += "                                </td> \n";
            headVal += "                            </tr> \n";
            headVal += "                            <tr class=\"trBottomBorder\"> \n";
            #region details: Received From
            headVal += "                                <td> \n " +
                                                            "<p>\n " +
                                                                "Received From / Diterima Dari\n " +
                                                            "</p>\n " +
                                                        "</td>\n " +
                                                        "<td>\n " +
                                                            "<p>\n " +
                                                                ":\n " +
                                                            "</p>\n " +
                                                        "</td>\n " +
                                                        "<td>\n " +
                                                            "<p class=\"cellVal1\">\n " +
                                                                detailReceiver + "\n " +
                                                            "</p>\n " +
                                                        "</td>";
            #endregion
            headVal += "                            </tr> \n";
            headVal += "                            <tr class=\"trBottomBorder\"> \n";
            #region details: Amount
            headVal += "                                <td> \n " +
                                                            "<p>\n " +
                                                                "Amount / Jumlah\n " +
                                                            "</p>\n " +
                                                        "</td>\n " +
                                                        "<td>\n " +
                                                            "<p>\n " +
                                                                ":\n " +
                                                            "</p>\n " +
                                                        "</td>\n " +
                                                        "<td>\n " +
                                                            "<p class=\"cellVal1\">\n " +
                                                                headerCurr + " " + headerAmt + "\n " +
                                                            "</p>\n " +
                                                        "</td> \n";
            #endregion
            headVal += "                            </tr> \n";
            headVal += "                            <tr class=\"trBottomBorder\"> \n";
            #region details: Said
            headVal += "                                <td> \n " +
                                                            "<p>\n " +
                                                                "Said / Terbilang\n " +
                                                            "</p>\n " +
                                                        "</td>\n " +
                                                        "<td>\n " +
                                                            "<p>\n " +
                                                                ":\n " +
                                                            "</p>\n " +
                                                        "</td>\n " +
                                                        "<td>\n " +
                                                            "<p class=\"cellVal1\">\n " +
                                                                headerSa + "\n " +
                                                            "</p>\n " +
                                                        "</td>";
            #endregion
            headVal += "                            </tr> \n";
            headVal += "                            <tr style=\"height: 25px;\"> \n";
            headVal += "                                <td> \n " +
                                                            "<p>\n " +
                                                                "Description\n " +
                                                            "</p>\n " +
                                                        "</td>\n " +
                                                        "<td colspan=\"2\">\n " +
                                                            "<p>:</p>\n " +
                                                        "</td>";
            headVal += "                            </tr> \n";
            headVal += "                            <tr> \n";
            headVal += "                                <td colspan=\"3\" style=\"text-align:center;\"> \n";
            detailVal += "                                    <table class=\"tbl2\" style=\"border-collapse:collapse;\"> \n";
            detailVal += "                                        <tr> \n";
            detailVal += "                                            <th style=\"border: 1px solid black;\">BL</th>\n";
            detailVal += "                                            <th style=\"border: 1px solid black;\">Invoice</th>\n";
            detailVal += "                                            <th style=\"border: 1px solid black;\">Description</th>\n";
            detailVal += "                                            <th style=\"border: 1px solid black;\">Currency</th>\n";
            detailVal += "                                            <th style=\"border: 1px solid black;\">Amount</th>\n";
            detailVal += "                                        </tr> \n";
            #region details: Description
            if (detailBL.Count > 0)
            {
                for (int l = 0; l < detailBL.Count; l++)
                {
                    detailVal += "                                    <tr> \n";
                    detailVal += "                                        <td style=\"border: 1px solid black;text-align:center;\"> \n";
                    detailVal += "                                            <p> " + detailBL[l].ToString() + "</p> \n";
                    detailVal += "                                        </td> \n";
                    detailVal += "                                        <td style=\"border: 1px solid black;text-align:center;\"> \n";
                    detailVal += "                                            <p> " + detailInv[l].ToString() + "</p> \n";
                    detailVal += "                                        </td> \n";
                    detailVal += "                                        <td style=\"border: 1px solid black;text-align:center;\"> \n";
                    detailVal += "                                            <p> " + detailDesc[l].ToString() + "</p> \n";
                    detailVal += "                                        </td> \n";
                    detailVal += "                                        <td style=\"border: 1px solid black;text-align:center;\"> \n";
                    detailVal += "                                            <p> " + detailCurr + "</p> \n";
                    detailVal += "                                        </td> \n";
                    detailVal += "                                        <td style=\"border: 1px solid black;text-align:right;\"> \n";
                    detailVal += "                                            <p> " + detailAmount[l].ToString() + "</p> \n";
                    detailVal += "                                        </td> \n";
                    detailVal += "                                    </tr> \n";
                }
            }
            detailVal += "                                        <tr> \n";
            detailVal += "                                            <td colspan=\"3\" style=\"text-align:center;border: 1px solid black;\"> <b>VAT</b> </td> \n";
            detailVal += "                                            <td style=\"text-align:center;border: 1px solid black;\"><p>VAT Total (TAX 10%)</p></td>\n";
            detailVal += "                                            <td style=\"text-align:right;border: 1px solid black;\"><p>" + string.Format("{0:N2}", detailVat) + "</p></td>\n";
            detailVal += "                                        </tr> \n";
            //detailVal += "                                        <tr> \n";
            //detailVal += "                                            <td colspan=\"2\" style=\"text-align:center;border: 1px solid black;\"> <b>PPH</b> </td> \n";
            //detailVal += "                                            <td style=\"text-align:center;border: 1px solid black;\"><p>PPH23 (2% Total Invoice)</p></td>\n";
            //detailVal += "                                            <td style=\"text-align:right;border: 1px solid black;\"><p>" + string.Format("{0:N2}", detailPph) + "</p></td>\n";
            //detailVal += "                                        </tr> \n";
            #endregion
            detailVal += "                                    </table> \n";
            headVal += detailVal;
            #region details: Total Amount
            headVal += "                                    <h3> \n " +
                        "                                       Total Amount : " + detailCurr + " " + string.Format("{0:N2}", detailTotal) + "\n " +
                        "                                   </h3> \n";
            #endregion
            headVal += "                                    <table class=\"tbl2\" style=\"font-size: 10pt;\"> \n";
            headVal += "                                        <tr>\n";
            headVal += "                                            <th>BANK Name</th> \n";
            headVal += "                                            <th>Account</th> \n";
            headVal += "                                            <th>Bank Date</th> \n";
            headVal += "                                            <th>Transfer Amount</th> \n";
            headVal += "                                        </tr>\n";
            if (dtReff.Rows.Count > 0)
            {
                for (int m = 0; m < dtReff.Rows.Count; m++)
                {
                    headVal += "                                <tr>\n";
                    headVal += "                                    <td><p>" + dtReff.Rows[m]["BankName"].ToString() + "</p></td> \n";
                    headVal += "                                    <td><p>" + dtReff.Rows[m]["BrandAccount"].ToString() + "</p></td> \n";
                    headVal += "                                    <td><p>" + dtReff.Rows[m]["Bankdate"].ToString() + "</p></td> \n";
                    headVal += "                                    <td style=\"text-align:right;\"><p>" + Convert.ToDecimal(dtReff.Rows[m]["Amount"]).ToString("N2") + "</p></td> \n";
                    headVal += "                                </tr>\n";
                }
            }
            headVal += "                                    </table> \n";
            headVal += "                                </td>";
            headVal += "                            </tr> \n";
            headVal += "                        </table>";
            #region details: Received By
            headVal += "                        <table class=\"tbl3\"> \n";
            headVal += "                            <tr> \n " +
                                                        "<td>\n " +
                                                            "<p>\n " +
                                                                "RECEIVED BY:\n " +
                                                            "</p>\n " +
                                                        "</td>\n " +
                                                    "</tr>\n " +
                                                    "<tr>\n " +
                                                        "<td>\n " +
                                                            headCashier + "\n " +
                                                        "</td>\n " +
                                                    "</tr>";
            headVal += "                        </table> \n";
            #endregion
            headVal += "                    </td> \n";
            headVal += "                </tr> \n";
            #endregion
            headVal += "            </table> \n";
            headVal += "        </form> \n";
            headVal += "    </body> \n";
            #endregion

            #region Get Mail To
            mailTo = generateMailTo(headerReceipt);
            #endregion

            try
            {
                SendEmail(headerReceipt, headVal, detailBL, detailInv, mailTo, mailCC, mailBCC, headCashier, detailReceiver, detailVal, reffno, "OFFICIAL");
                StoringEventReview(headerReceipt, blNum, invNum, mailTo, mailCC, mailBCC, "SENT", "NULL");
                UpdateSendingMailInfo(headerReceipt, Session["username"].ToString());
            }
            catch(Exception ex)
            {
                StoringEventReview(headerReceipt, blNum, invNum, mailTo, mailCC, mailBCC, "FAILED", ex.ToString());
            }
            

        }

        //====================== Sending EMail ====================================//
        public void SendEmail(string rID, string val, List<string> blNo, List<string> InvNo, string toMail, string ccMail, string bccMail, string cashierName, string receiveFrom, string detailBody, string reffNo, string typeReceipt)
        {
            HtmlToPdf converter = new HtmlToPdf();

            #region pdf Setting
            string headerUrl = Server.MapPath("~/Additional/Header.html");
            converter.Options.DisplayHeader = true;
            converter.Header.DisplayOnFirstPage = true;
            converter.Header.DisplayOnOddPages = true;
            converter.Header.DisplayOnEvenPages = true;
            converter.Header.Height = 140;

            PdfHtmlSection headerHtml = new PdfHtmlSection(headerUrl);
            headerHtml.AutoFitHeight = HtmlToPdfPageFitMode.AutoFit;
            headerHtml.AutoFitWidth = HtmlToPdfPageFitMode.AutoFit;
            converter.Header.Add(headerHtml);


            // set converter options
            converter.Options.PdfPageSize = PdfPageSize.A4;
            converter.Options.PdfPageOrientation = PdfPageOrientation.Portrait;
            converter.Options.AutoFitWidth = HtmlToPdfPageFitMode.AutoFit;
            converter.Options.AutoFitHeight = HtmlToPdfPageFitMode.ShrinkOnly;
            converter.Options.MarginLeft = 10;
            //converter.Options.MarginBottom = 1;
            converter.Options.MarginRight = 5;
            converter.Options.MarginTop = 20;

            // create a new pdf document converting an url
            //PdfDocument doc = converter.ConvertUrl(TxtUrl.Text);
            PdfDocument doc = converter.ConvertHtmlString(val);
            #endregion


            string fileName = rID + "_" + DateTime.Now.Date.ToString("dd-MM-yyyy");
            string blForBody = DesignBLEmail(blNo);
            string tm = DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString();
            bool fx = File.Exists(MapPath(uploadFolder + fileName + ".pdf"));
            if (fx)
                File.Move(MapPath(uploadFolder + fileName + ".pdf"), MapPath(uploadFolder + fileName + "_" + tm + ".pdf"));
            doc.Save(MapPath(uploadFolder + fileName + ".pdf"));
            string AttachmentFile = HttpContext.Current.Server.MapPath(uploadFolder + fileName + ".pdf");

            #region setup email receipiens
            MailMessage message = new MailMessage();
            message.From = new MailAddress("ereceipt-support@cma-cgm.com");
            //message.To.Add(new MailAddress(toMail));
            //message.To.Add(new MailAddress("dja.mrachmansyah@cma-cgm.com"));
            string sampleEmail = string.Empty;
            try
            {
                 sampleEmail = Session["uEmail"].ToString();
            }
            catch
            {
                sampleEmail = "asia.itretsupport@cma-cgm.com";
            }
            //message.To.Add(new MailAddress("asia.itretsupport@cma-cgm.com"));
            message.To.Add(new MailAddress(sampleEmail));
            if (ccMail != string.Empty)
            {
                message.CC.Add(new MailAddress(ccMail));
            }

            if (bccMail != string.Empty)
            {
                message.Bcc.Add(new MailAddress(bccMail));
            }
            #endregion

            string sub = typeReceipt;
            string eBody = string.Empty;

            #region Creating Email Body
            eBody += "Actually, this email will sent To : " + toMail + " <br/> \n";
            eBody += "               <table style=\"width:100%;\">";
            eBody += "                <tr> \n";
            eBody += "                  <td colspan=\"2\"> \n";
            eBody += "                      <span style=\"font-size: 10pt; font-family: Calibri;\"> \n" +
                                            "<b>*** This is an automatically generated email communication. Please do not reply to the sender of this message.***</b> \n" +
                                            "</span>\n";
            eBody += "                  </td> \n";
            eBody += "                </tr> \n";
            eBody += "                <tr> \n";
            eBody += "                  <td colspan=\"2\"> \n";
            eBody += "                      <span style=\"font-size: 11pt; font-family: Calibri;\"> \n";
            eBody += "                          Dear User of " + receiveFrom + ",<br/> <br/>\n";
            eBody += "                          Here your E-Receipt for BL# : <b>" + blForBody + "</b>\n";
            eBody += detailBody + "<br/> <br/>\n";
            eBody += "                          For details, please find your E-Receipt in the attachment. <br/>\n";
            eBody += "                          Thank you in advance for your attention.<br/> \n";
            eBody += "                          Best Regards, <br/><br/><br/>\n";
            eBody += "                      </span> \n";
            eBody += "                      <span style=\"margin-bottom:.0001pt;font-size:12pt;font-family:\"Calibri\";margin-left: 0cm;margin-right: 0cm;margin-top: 0cm;\"> \n\n";
            eBody += "                          <b>" + cashierName + "</b><br/>\n ";
            eBody += "                          [Division Name] <br/> \n";
            eBody += "                          <font color=\"#2F537C\"><b>Indonesia</b></font> <br/> \n";
            eBody += "                      </span> \n";
            eBody += "                      <span style=\"margin-bottom:.0001pt;font-size:8pt;font-family:\"Calibri\";margin-left: 0cm;margin-right: 0cm;margin-top: 0cm;\"> \n";
            eBody += "                          Email : ______________@cmacgm.com</span> <br/>\n";
            eBody += "                      </span> \n";
            eBody += "                  </td>";
            eBody += "                </tr> \n";
            eBody += "                <tr> \n";
            eBody += "                    <td style=\"text-align:center;\"> \n";
            eBody += "                        <img src=cid:ereceiptID> \n";
            eBody += "                    </td> \n";
            eBody += "                    <td> \n";
            eBody += "                        <table style=\"width:100%; margin:0;\" class=\"BrandAddress\"> \n";
            eBody += "                            <tr> \n";
            eBody += "                                <td>";
            eBody += "                                    <h1 style=\"font-weight:800; font-size:20px;\">CMA CGM SA</h1> \n";
            eBody += "                                    <p style=\"font-weight:600\" >C.O. PT CONTAINER MARITIME ACTIVITIES</p>";
            eBody += "                                    <p>PERMATA KUNINGAN LT.21 & 22 <br /> \n" +
                                                            "JL.KUNINGAN MULIA KAV.9C GUNTUR, SETIABUDI <br />\n" +
                                                            "JAKARTA SELATAN - DKI JAKARTA <br />\n" +
                                                            "Phone: +62 21 2854 6800 <br />\n" +
                                                            "Fax  : +62 21 2854 6801\n" +
                                                        "</p>";
            eBody += "                                </td>";
            eBody += "                            </tr> \n";
            eBody += "                        </table>";
            eBody += "                    </td> \n";
            eBody += "                </tr> \n";
            eBody += "                <tr> \n";
            eBody += "                  <td colspan=\"2\"> \n";
            eBody += "                      <span style=\"font-size: 10pt; font-family: Calibri;\"> \n" +
                                            "<b>*** This is an automatically generated email communication. Please do not reply to the sender of this message.***</b> \n" +
                                            "</span>\n";
            eBody += "                  </td> \n";
            eBody += "                </tr> \n";
            eBody += "              </table>";
            #endregion

            message.Subject = "MAIL TESTING -- " + sub + " Receipt of: " + rID;
            message.Body = eBody;

            Attachment attachment1 = new Attachment(AttachmentFile, MediaTypeNames.Application.Octet);
            message.Attachments.Add(attachment1);
            AlternateView view = AlternateView.CreateAlternateViewFromString(eBody, null, "text/html");
            message.AlternateViews.Add(view);

            LinkedResource theEmailImage = new LinkedResource(HttpContext.Current.Server.MapPath("~\\assets\\global\\images\\Voucher_Img\\Logo_CMA_3.png"));
            theEmailImage.ContentId = "ereceiptID";
            view.LinkedResources.Add(theEmailImage);

            #region smtp for sending email
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "129.35.173.46";
            smtp.Port = 25;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.Send(message);
            #endregion

            // close pdf document
            doc.Close();

            //string blDb = DesignBlInEventReview(blNo);
            //string invDb = DesignBlInEventReview(InvNo);
            //StoringEventReview(rID, blDb, invDb, toMail, "", "");
        }

        //===================== Another FUnction ===========================//
        public string generateMailTo(string id)
        {
            string val = string.Empty;
            string m1 = string.Empty;
            string m2 = string.Empty;
            DataTable dtByDB = dtFindPartnerEmail(id);
            if (dtByDB.Rows.Count > 0)
            {
                for (int i = 0; i < dtByDB.Rows.Count; i++)
                {
                    m1 = dtByDB.Rows[i]["EmailWS"].ToString();
                    m2 = dtByDB.Rows[i]["OtherEmail"].ToString();
                }
            }

            if (m2 != string.Empty)
            {
                val = m2 + "- This is from Other Email";
            }
            else
            {
                val = m1 + "- This is from Webservice";
            }

            return val;
        }

        public string DesignBLEmail(List<string> val)
        {
            string result = string.Empty;
            if (val.Count > 0)
            {
                for (int i = 0; i < val.Count; i++)
                {
                    if (!result.Contains(val[i].ToString().Trim()))
                    {
                        if (result != string.Empty)
                        {
                            result = result + ", " + val[i].ToString().Trim();
                        }
                        else
                        {
                            result = val[i].ToString().Trim();
                        }
                    }
                }
            }
            return result;
        }

        public string DesignForEventReview(List<string> val)
        {
            string result = string.Empty;
            if (val.Count > 0)
            {
                for (int i = 0; i < val.Count; i++)
                {
                    if (!result.Contains(val[i].ToString().Trim()))
                    {
                        if (result != string.Empty)
                        {
                            if (!result.Contains(val[i].ToString().Trim()))
                                result = result + "| " + val[i].ToString().Trim();
                        }
                        else
                        {
                            result = val[i].ToString().Trim();
                        }
                    }
                }
            }
            return result;
        }

        

        
    }
}