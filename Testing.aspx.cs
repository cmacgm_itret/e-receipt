﻿using DevExpress.Web;
using MySql.Data.MySqlClient;
using SelectPdf;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace E_Receipt_v1_1
{
    public partial class Testing : System.Web.UI.Page
    {
        string ConnectionString = ConfigurationManager.ConnectionStrings["cmsidConnectionString"].ConnectionString;
        string ereceiptConnection = ConfigurationManager.ConnectionStrings["ereceiptConnectionString"].ConnectionString;
        bool needToSelectAll;
        protected void Page_Load(object sender, EventArgs e)
        {
            GenerateMainGv();
        }

        protected void gvMainGrid_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs e)
        {
            
        }

        protected void gvDtlMain_BeforePerformDataSelect(object sender, EventArgs e)
        {
            string keyVal = ASPxGridView.GetDetailRowKeyValue(sender as ASPxGridView).ToString();
            string rId = string.Empty;
            int getType = 1;
            ASPxGridView gv = (ASPxGridView)sender;
            gv.Columns.Clear();
            DataTable dt = dtOfficialReceipt(keyVal, getType);
            if (dt.Rows.Count > 0)
            {
                foreach (DataColumn dc in dt.Columns)
                {
                    GridViewDataTextColumn dtc = new GridViewDataTextColumn();
                    dtc.FieldName = dc.ColumnName;
                    dtc.Caption = dc.ColumnName;
                    gv.Columns.Add(dtc);
                }

                
            }
            else
            {
                DataTable dt2 = dtContainerDeposit(keyVal, getType);
                if(dt2.Rows.Count > 0)
                {
                    foreach (DataColumn dc in dt2.Columns)
                    {
                        GridViewDataTextColumn dtc = new GridViewDataTextColumn();
                        dtc.FieldName = dc.ColumnName;
                        dtc.Caption = dc.ColumnName;
                        gv.Columns.Add(dtc);
                    }
                }
                else
                {
                    DataTable dt3 = dtFreeEntryReceipt(keyVal, getType);
                    if (dt3.Rows.Count > 0)
                    {
                        foreach (DataColumn dc in dt3.Columns)
                        {
                            GridViewDataTextColumn dtc = new GridViewDataTextColumn();
                            dtc.FieldName = dc.ColumnName;
                            dtc.Caption = dc.ColumnName;
                            gv.Columns.Add(dtc);
                        }
                    }
                    else
                    {
                        DataTable dt4 = dtFreeEntryPayment(keyVal, getType);
                        if (dt4.Rows.Count > 0)
                        {
                            foreach (DataColumn dc in dt4.Columns)
                            {
                                GridViewDataTextColumn dtc = new GridViewDataTextColumn();
                                dtc.FieldName = dc.ColumnName;
                                dtc.Caption = dc.ColumnName;
                                gv.Columns.Add(dtc);
                            }
                        }
                    }
                }
            }
            
            
            
            //dsMainDetail.SelectCommand = string.Format(query, keyVal);
        }

        protected void gvDtlMain_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs e)
        {
            switch (e.Column.FieldName)
            {
                case "Deletestatus":
                    switch (e.Value.ToString())
                    {
                        case "0":
                            e.DisplayText = "Saved";
                            break;

                        default:
                            e.DisplayText = "Cancelled";
                            e.Column.CellStyle.BackColor = System.Drawing.Color.LightGoldenrodYellow;
                            break;
                    }
                    break;

                case "ChargeAmount":
                case "ActualAmount":
                    e.DisplayText = String.Format("{0:#,###.##}", Math.Round(Convert.ToDecimal(e.Value.ToString()), 0));
                    break;
            }
        }

        protected void GenerateMainGv()
        {
            string query = string.Empty;
            if (txtBL.Text == "")
            {
                query = "SELECT ID, IDReceipt, Currency, Amount, Said, a.STATUS, PaymentMethodDSP, TYPE, \n" +
                        "ReceiptDate, Description, ReceiptBy, CONCAT(c.user_first_name, ' ', c.user_last_name) AS 'userN', Remarks, Invoice, ReceiptCategory, b.receiptCategoryName, \n" +
                        "a.Site, REFFNO, Deletestatus, Cancelby, Canceldate, CancelReason, \n" +
                        "Updateby, source, TransferAmount, BankDate, OFFSETType, Refference, GroupOrder \n" +
                    "FROM receipt_payment_detail_daily a \n" +
                    "LEFT JOIN receiptcategory b ON a.ReceiptCategory = b.receiptCode \n " +
                    "LEFT JOIN user_list c ON a.ReceiptBy = c.user_id;";

            }
            else
            {
                string receiptId = string.Empty;
                List<string> rID = GetReceiptID(txtBL.Text);

                if (rID.Count() == 0)
                {
                    lblSubmitWarning.Visible = true;
                    lblSubmitWarning.Text = "No data found!";
                }
                else
                {
                    lblSubmitWarning.Visible = false;
                }

                receiptId = "('"+ string.Join("', '", rID.ToArray()) +"')";
                query = string.Format("SELECT ID, IDReceipt, Currency, Amount, Said, a.STATUS, PaymentMethodDSP, TYPE, \n" +
                        "ReceiptDate, Description, ReceiptBy, CONCAT(c.user_first_name, ' ', c.user_last_name) AS 'userN', Remarks, Invoice, ReceiptCategory, b.receiptCategoryName, \n" +
                        "a.Site, REFFNO, Deletestatus, Cancelby, Canceldate, CancelReason, \n" +
                        "Updateby, source, TransferAmount, BankDate, OFFSETType, Refference, GroupOrder \n" +
                    "FROM receipt_payment_detail_daily a \n" +
                    "LEFT JOIN receiptcategory b ON a.ReceiptCategory = b.receiptCode \n " +
                    "LEFT JOIN user_list c ON a.ReceiptBy = c.user_id \n " +
                    "WHERE IDReceipt IN {0} \n" +
                    "UNION \n" +
                    "SELECT ID, IDReceipt, Currency, Amount, Said, a.STATUS, PaymentMethodDSP, TYPE, \n" +
                        "ReceiptDate, Description, ReceiptBy, CONCAT(c.user_first_name, ' ', c.user_last_name) AS 'userN', Remarks, Invoice, ReceiptCategory, b.receiptCategoryName, \n" +
                        "a.Site, REFFNO, Deletestatus, Cancelby, Canceldate, CancelReason, \n" +
                        "Updateby, source, TransferAmount, BankDate, OFFSETType, Refference, GroupOrder \n" +
                    "FROM history_receipt_payment_detail a \n" +
                    "LEFT JOIN receiptcategory b ON a.ReceiptCategory = b.receiptCode \n " +
                    "LEFT JOIN user_list c ON a.ReceiptBy = c.user_id \n " +
                    "WHERE IDReceipt IN {0} \n" +
                    ";", receiptId);
            }

            dsMainGrid.SelectCommand = query;
            gvMainGrid.DataBind();
        }

        DataTable dtOfficialReceipt(string blNum, int ty)
        {
            MySqlConnection conn = new MySqlConnection(ConnectionString);
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            string xx = string.Empty;
            if (ty == 0)
            {
                xx = "BLNUMBER";
            }
            else
            {
                xx = "OtherReceiptID";
            }
            string sQuery = string.Format("SELECT BLNUMBER, OtherReceiptID, ChargeCode, ChargeCurr, ChargeAmount, ActualCurr, ActualAmount, TYPE, InvoiceNumber, \n " +
                                "CNEE, STATUS, IsPaid, ReceiptDate, Site, REFFNO, Remarks, Deletestatus, Address, Updateby, source, \n" +
                                "LocalInvoiceNumber, OFFSETType, BankName, vatamount, pph23, Partner_Code FROM otherreceiptdetail_daily \n " +
                                "WHERE {1} = '{0}' \n" +
                                "UNION \n " +
                                "SELECT BLNUMBER, OtherReceiptID, ChargeCode, ChargeCurr, ChargeAmount, ActualCurr, ActualAmount, TYPE, InvoiceNumber, \n " +
                                    "CNEE, STATUS, IsPaid, ReceiptDate, Site, REFFNO, Remarks, Deletestatus, Address, Updateby, source, \n " +
                                    "LocalInvoiceNumber, OFFSETType, BankName, vatamount, pph23, Partner_Code FROM history_otherreceiptdetail \n " +
                            "WHERE {1} = '{0}';", blNum, xx);
            dsMainDetail.SelectCommand = sQuery;
            MySqlCommand cmd = new MySqlCommand(sQuery, conn);
            MySqlDataAdapter da = new MySqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            conn.Close();
            conn.Dispose();
            return dt;
        }

        DataTable dtContainerDeposit(string blNum, int ty)
        {
            MySqlConnection conn = new MySqlConnection(ConnectionString);
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            string xx = string.Empty;
            if (ty == 0)
            {
                xx = string.Format("(BLNUMBER = '{0}' OR Voyage = '{0}' )", blNum);
            }
            else
            {
                xx = string.Format("IDReceipt = '{0}' ", blNum);
            }
            string sQuery = string.Format("SELECT \n " +
                        "BLNUMBER, IDReceipt, ConsigneeFullname, Voyage, Vessel, STATUS, paidcounter, depCurr, depositAmount, doextCurr, DOExtAmount, \n " +
	                        "Transtype, DOExtensionCounter, refundno, StatusContainer, PICPostSurvey, MNRSurveyDate, TPCRemarks, IsPayRefund, REFFNO, \n " +
	                        "Deletestatus, receiptdate, discountPct, IsConfirm, TPCAmount, InvoiceTPC, TPCInvoiceDate, DEMAmount, InvoiceDEM, DemInvoiceDate \n " +
                        "FROM containerdepositreceiptdetail \n " +
                            "WHERE {1};", blNum, xx);
            dsMainDetail.SelectCommand = sQuery;
            MySqlCommand cmd = new MySqlCommand(sQuery, conn);
            MySqlDataAdapter da = new MySqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            conn.Close();
            conn.Dispose();
            return dt;
        }

        DataTable dtFreeEntryReceipt(string blNum, int ty)
        {
            MySqlConnection conn = new MySqlConnection(ConnectionString);
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            string xx = string.Empty;
            if (ty == 0)
            {
                xx = "BLNUMBER";
            }
            else
            {
                xx = "ReceiptID";
            }
            string sQuery = string.Format("SELECT ReceiptID, BLNUMBER, INVOICE, ReceivedFrom, Currency, Amount, said, STATUS, \n " +
	                                "TYPE, PaymentMethod, Remarks, Site, REFFNO, Remarks2, Deletestatus, Address, Updateby, \n " +
                                    "source, OFFSETType FROM freeentryotherreceipt_daily \n " +
                                "WHERE {1} = '{0}' \n" +
                                "UNION \n " +
                                "SELECT ReceiptID, BLNUMBER, INVOICE, ReceivedFrom, Currency, Amount, said, STATUS, \n " +
	                                "TYPE, PaymentMethod, Remarks, Site, REFFNO, Remarks2, Deletestatus, Address, Updateby, \n " +
                                    "source, OFFSETType FROM history_freeentryotherreceipt \n " +
                            "WHERE {1} = '{0}';", blNum, xx);
            dsMainDetail.SelectCommand = sQuery;
            MySqlCommand cmd = new MySqlCommand(sQuery, conn);
            MySqlDataAdapter da = new MySqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            conn.Close();
            conn.Dispose();
            return dt;
        }

        DataTable dtFreeEntryPayment(string blNum, int ty)
        {
            MySqlConnection conn = new MySqlConnection(ConnectionString);
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            string xx = string.Empty;
            if (ty == 0)
            {
                xx = "BLNUMBER";
            }
            else
            {
                xx = "PaymentID";
            }
            string sQuery = string.Format("SELECT PaymentID, BLNUMBER, PaidTo, Currency, Amount, Said, STATUS, TYPE, PaymentMethodDSP, \n " +
	                            "Remarks, Site, REFFNO, invoice, AccountCode, VesselName, Voyage, Principal, POD, \n " +
                                "DateOfCall, Description, Paymentdate, RefundCategory, EmployeeName, Deletestatus, \n " +
                                "Updateby, source, LinkPaymentID, StatusUsed, isRCA FROM freeentrypayment_daily \n " +
                                "WHERE {1} = '{0}' \n" +
                                "UNION \n " +
                                "SELECT PaymentID, BLNUMBER, PaidTo, Currency, Amount, Said, STATUS, TYPE, PaymentMethodDSP, \n " +
                                "Remarks, Site, REFFNO, invoice, AccountCode, VesselName, Voyage, Principal, POD, \n " +
                                "DateOfCall, Description, Paymentdate, RefundCategory, EmployeeName, Deletestatus, \n " +
                                "Updateby, source, LinkPaymentID, StatusUsed, isRCA FROM history_freeentrypayment \n " +
                            "WHERE {1} = '{0}';", blNum, xx);
            dsMainDetail.SelectCommand = sQuery;
            MySqlCommand cmd = new MySqlCommand(sQuery, conn);
            MySqlDataAdapter da = new MySqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            conn.Close();
            conn.Dispose();
            return dt;
        }

        DataTable dtFindByReferrence(string blNum, int ty)
        {
            MySqlConnection conn = new MySqlConnection(ConnectionString);
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            string xx = string.Empty;
            if (ty == 0)
            {
                xx = string.Format("Refference LIKE '%{0}%'", blNum);
            }
            else
            {
                xx = string.Format("IDReceipt = '{0}'", blNum);
            }
            string sQuery = string.Format("SELECT ID, IDReceipt, Currency, Amount, Said, a.STATUS, PaymentMethodDSP, TYPE, \n" +
                        "ReceiptDate, DATE(ReceiptDate) as rd, Description, ReceiptBy, CONCAT(c.user_first_name, ' ', c.user_last_name) AS 'userN', Remarks, Invoice, ReceiptCategory, b.receiptCategoryName, \n" +
                        "a.Site, REFFNO, Deletestatus, Cancelby, Canceldate, CancelReason, \n" +
                        "Updateby, source, TransferAmount, BankDate, OFFSETType, Refference, GroupOrder \n" +
                    "FROM receipt_payment_detail_daily a \n" +
                    "LEFT JOIN receiptcategory b ON a.ReceiptCategory = b.receiptCode \n " +
                    "LEFT JOIN user_list c ON a.ReceiptBy = c.user_id \n " +
                    "WHERE {0} \n" +
                    "UNION \n" +
                    "SELECT ID, IDReceipt, Currency, Amount, Said, a.STATUS, PaymentMethodDSP, TYPE, \n" +
                        "ReceiptDate, DATE(ReceiptDate) as rd, Description, ReceiptBy, CONCAT(c.user_first_name, ' ', c.user_last_name) AS 'userN', Remarks, Invoice, ReceiptCategory, b.receiptCategoryName, \n" +
                        "a.Site, REFFNO, Deletestatus, Cancelby, Canceldate, CancelReason, \n" +
                        "Updateby, source, TransferAmount, BankDate, OFFSETType, Refference, GroupOrder \n" +
                    "FROM history_receipt_payment_detail a \n" +
                    "LEFT JOIN receiptcategory b ON a.ReceiptCategory = b.receiptCode \n " +
                    "LEFT JOIN user_list c ON a.ReceiptBy = c.user_id \n " +
                    "WHERE {0}\n" +
                    ";", xx);
            dsMainDetail.SelectCommand = sQuery;
            MySqlCommand cmd = new MySqlCommand(sQuery, conn);
            MySqlDataAdapter da = new MySqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            conn.Close();
            conn.Dispose();
            return dt;
        }

        DataTable dtFindPartnerEmail(string PartnerCode)
        {
            DataTable dt = new DataTable();
            MySqlConnection conn = new MySqlConnection(ereceiptConnection);
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            string sQuery = string.Format("SELECT pdID, pdPartnerCode, pdFullName, pdContactNumber, pdContactType, pdStatus FROM db_ereceipt.tblPartnerDetails \n " +
                    "WHERE pdPartnerCode = '{0}' AND pdContactType = 'EM' AND pdStatus = 1;", PartnerCode);
            dsMainDetail.SelectCommand = sQuery;
            MySqlCommand cmd = new MySqlCommand(sQuery, conn);
            MySqlDataAdapter da = new MySqlDataAdapter(cmd);
            da.Fill(dt);
            conn.Close();
            conn.Dispose();
            return dt;
        }

        DataTable dtFindRefference(string val)
        {
            DataTable dt = new DataTable();
            MySqlConnection conn = new MySqlConnection(ConnectionString);
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            string sQuery = string.Format("SELECT BankDate, Description, Refference, Debit, Credit, Balance, BankName, Uploaddate, Uploadby, Defined, Account, Currency, \n" + 
	                    "AppliedAmount, ExcessAmount, RelatedRefference, IsManual, GroupOrder, TIME, void, remarks \n " +
                    "FROM bankstatement \n " +
                    "WHERE Refference IN ({0});", val);
            dsMainDetail.SelectCommand = sQuery;
            MySqlCommand cmd = new MySqlCommand(sQuery, conn);
            MySqlDataAdapter da = new MySqlDataAdapter(cmd);
            da.Fill(dt);
            conn.Close();
            conn.Dispose();
            return dt;
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (txtBL.Text != "")
            {
                lblSubmitWarning.Visible = false;
                GenerateMainGv();
            }
            else
            {
                lblSubmitWarning.Visible = true;
                lblSubmitWarning.Text = "Please type BL# on filter box";
                txtBL.Focus();
            }
        }

        protected void PopRemind_WindowCallback(object source, PopupWindowCallbackArgs e)
        {

        }

        private List<string> GetReceiptID(string val)
        {
            List<string> result = new List<string>();
            string bVal = string.Empty;
            DataTable dtOR = dtOfficialReceipt(val, 0);
            if (dtOR.Rows.Count > 0)
            {
                for (int i = 0; i < dtOR.Rows.Count; i++)
                {
                    if (bVal != dtOR.Rows[i]["OtherReceiptID"].ToString())
                    {
                        result.Add(dtOR.Rows[i]["OtherReceiptID"].ToString());
                        bVal = dtOR.Rows[i]["OtherReceiptID"].ToString();
                    }
                        
                }
                
            }

            DataTable dtFER = dtFreeEntryReceipt(val, 0);
            if (dtFER.Rows.Count > 0)
            {
                for (int i = 0; i < dtFER.Rows.Count; i++)
                {
                    if (bVal != dtFER.Rows[i]["ReceiptID"].ToString())
                    {
                        result.Add(dtFER.Rows[i]["ReceiptID"].ToString());
                        bVal = dtFER.Rows[i]["ReceiptID"].ToString();
                    }
                    
                }
            }

            DataTable dtFEP = dtFreeEntryPayment(val, 0);
            if (dtFEP.Rows.Count > 0)
            {
                for (int i = 0; i < dtFEP.Rows.Count; i++)
                {
                    if (bVal != dtFEP.Rows[i]["PaymentID"].ToString())
                    {
                        result.Add(dtFEP.Rows[i]["PaymentID"].ToString());
                        bVal = dtFEP.Rows[i]["PaymentID"].ToString();
                    }

                }
                
            }

            DataTable dtCD = dtContainerDeposit(val, 0);
            if (dtCD.Rows.Count > 0)
            {
                for (int i = 0; i < dtCD.Rows.Count; i++)
                {
                    if (bVal != dtCD.Rows[i]["IDReceipt"].ToString())
                    {
                        result.Add(dtCD.Rows[i]["IDReceipt"].ToString());
                        bVal = dtCD.Rows[i]["IDReceipt"].ToString();
                    }
                }
            }

            DataTable dtRefference = dtFindByReferrence(val, 0);
            if (dtRefference.Rows.Count > 0)
            {
                for(int i = 0; i<dtRefference.Rows.Count; i++)
                {
                    if (bVal != dtRefference.Rows[i]["IDReceipt"].ToString())
                    {
                        result.Add(dtRefference.Rows[i]["IDReceipt"].ToString());
                        bVal = dtRefference.Rows[i]["IDReceipt"].ToString();
                    }
                }
            }
            return result;
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            txtBL.Text = "";
            lblSubmitWarning.Visible = false;
            GenerateMainGv();
        }

        protected void btnSendReceipt_Click(object sender, EventArgs e)
        {
            List<object> selVal = gvMainGrid.GetSelectedFieldValues(gvMainGrid.KeyFieldName);
            int x = selVal.Count;
            if (x == 0)
            {
                lblSubmitWarning.Visible = true;
                lblSubmitWarning.Text = "Select Receipt to Generate!";
            }
            else
            {
                lblSubmitWarning.Visible = false;
                Session["ListReceiptId"] = selVal;
                PopRemind.ShowOnPageLoad = true;

                //GenerateHTMLType1();
            }
        }

        protected void btnReminderSet_Click(object sender, EventArgs e)
        {
            if (cbAllUser.Checked == false && cbOtherUser.Checked == false)
            {
                lblWarn.Visible = true;
                lblWarn.Text = "Please, select one or both of selection.";
            }
            else if (cbOtherUser.Checked == true && tbOthersRemind.Text == "")
            {
                lblWarn.Visible = true;
                lblWarn.Text = "Please, insert other Email.";
            }
            else
            {
                if (Session["ListReceiptId"] != null)
                {
                    #region variable
                    int OR = 0; int sType = 0;
                    //Header Variable
                    string receiptID = string.Empty;
                    string headAmount = string.Empty;
                    string headCurrency = string.Empty;
                    string headSaid = string.Empty;
                    string headDate = string.Empty;
                    string headReceived = string.Empty;
                    string headUName = string.Empty;
                    string headRefference = string.Empty;
                    //Details - OR
                    List<string> orBL = new List<string>();
                    List<string> orInv = new List<string>();
                    List<string> orDesc = new List<string>();
                    List<string> orAmount = new List<string>();
                    string orCurr = string.Empty; string pCode = string.Empty;
                    decimal orVAT = 0; decimal orPPH = 0; decimal orTotal = 0;
                    #endregion
                    
                    if (cbAllUser.Checked == true)
                        sType = sType + 1;
                    if (cbOtherUser.Checked == true)
                        sType = sType + 2;

                    List<object> selVal = Session["ListReceiptId"] as List<object>;
                    int valCount = selVal.Count();
                    for (int i = 0; i < valCount; i++)
                    {
                        OR = 0; orVAT = 0; orPPH = 0; orTotal = 0; headReceived = string.Empty;
                        orBL.Clear(); orInv.Clear(); orDesc.Clear(); orAmount.Clear(); pCode = string.Empty;
                        #region Get Data Header
                        DataTable dtReceiptPayment = dtFindByReferrence(selVal[i].ToString(), 1);
                        if (dtReceiptPayment.Rows.Count > 0)
                        {
                            for (int j = 0; j < dtReceiptPayment.Rows.Count; j++)
                            {
                                receiptID = dtReceiptPayment.Rows[j]["IDReceipt"].ToString();
                                headAmount = Convert.ToDecimal(dtReceiptPayment.Rows[j]["Amount"]).ToString("N2");
                                headCurrency = dtReceiptPayment.Rows[j]["Currency"].ToString();
                                headSaid = dtReceiptPayment.Rows[j]["Said"].ToString();
                                headDate = dtReceiptPayment.Rows[j]["rd"].ToString();
                                headUName = dtReceiptPayment.Rows[j]["userN"].ToString();
                                headRefference = dtReceiptPayment.Rows[j]["Refference"].ToString();
                            }
                        }
                        #endregion

                        #region Get Data OR
                        DataTable dtOR = dtOfficialReceipt(selVal[i].ToString(), 1);
                        if (dtOR.Rows.Count > 0)
                        {
                            OR = 1;
                            for (int k = 0; k < dtOR.Rows.Count; k++)
                            {
                                headReceived = dtOR.Rows[k]["CNEE"].ToString();
                                orBL.Add(dtOR.Rows[k]["BLNUMBER"].ToString());
                                orInv.Add(dtOR.Rows[k]["InvoiceNumber"].ToString());
                                orDesc.Add(dtOR.Rows[k]["ChargeCode"].ToString());
                                orCurr = dtOR.Rows[k]["ActualCurr"].ToString();
                                orAmount.Add(Convert.ToDecimal(dtOR.Rows[k]["ChargeAmount"]).ToString("N2"));
                                orTotal = orTotal + Convert.ToDecimal(dtOR.Rows[k]["ChargeAmount"].ToString());

                                int cVat = 0;

                                if (dtOR.Rows[k]["vatamount"] is DBNull)
                                {
                                    orVAT = orVAT + 0;
                                    cVat = 0;
                                }
                                else
                                {
                                    orVAT = orVAT + Convert.ToDecimal(dtOR.Rows[k]["vatamount"]);
                                    cVat = Convert.ToInt32(dtOR.Rows[k]["vatamount"]);
                                }

                                if (cVat == 0)
                                {
                                    orPPH = orPPH + 0;
                                }
                                else
                                {
                                    if (dtOR.Rows[k]["pph23"] is DBNull)
                                        orPPH = orPPH + 0;
                                    else
                                        orPPH = orPPH + Convert.ToDecimal(dtOR.Rows[k]["pph23"].ToString());
                                }

                                if (dtOR.Rows[k]["Partner_Code"] is DBNull)
                                {
                                    pCode = string.Empty;
                                }
                                else if(dtOR.Rows[k]["Partner_Code"].ToString() == "")
                                {
                                    pCode = string.Empty;
                                }
                                else
                                {
                                    pCode = dtOR.Rows[k]["Partner_Code"].ToString();
                                }
                                                                    
                            }

                            orTotal = orTotal + orVAT; //exclude PPH23
                        }
                        #endregion

                        GenerateHTMLType1(receiptID, string.Format("{0:N2}", headAmount), headCurrency, headSaid, headDate, headUName, headReceived, orBL, orInv, orDesc, orCurr, orAmount,
                            orTotal.ToString("N2"), orVAT.ToString("N2"), orPPH.ToString("N2"), pCode, sType, headRefference);
                    }
                }
            }
            
        }

        const string uploadFolder = "~/GeneratedFiles/";
        public void GenerateHTMLType1(string headerReceipt, string headerAmt, string headerCurr, string headerSa, string headerDt, string headCashier, string detailReceiver, 
            List<string>detailBL, List<string>detailInv, List<string>detailDesc, string detailCurr, List<string>detailAmount, string detailTotal, string detailVat, string detailPph, 
            string partnerCode, int eType, string reff)
        {
            string headVal = string.Empty;
            string blNum = string.Empty;
            string mailTo = string.Empty;
            string detailVal = string.Empty;

            string[] splitReff = reff.Split('|');
            List<string> listReff = new List<string>();
            foreach (string item in splitReff)
            {
                listReff.Add("'" + item.Trim() + "'");
            }
            string resReff = string.Join(",", listReff.ToArray());
            DataTable dtReff = dtFindRefference(resReff);

            headVal =  "<html> \n";
            #region Head of HTML
            headVal += "    <head runat=\"server\"> \n";
            headVal += "        <style type=\"text/css\"> \n";
            headVal += "            @page{ \n" +
                                            "size:A4 portrait; \n" +
                                            "margin:1.25cm; \n" +
                                            "font-family: Calibri !important; \n" +
                                            "font-size:12px; \n" +
                                        "} \n";
            headVal += "            .tbl{ \n" +
                                            "border: 1pt solid #000 !important; \n" +
                                            "border-collapse:collapse; \n" +
                                            "width:100%; \n" +
                                        "} \n";
            headVal += "            .tbl2{ \n" +
                                            "border-collapse:collapse; \n" +
                                            "width:75%; \n" +
                                            "margin-left:25px; \n " +
                                            "margin-bottom: 25px;\n " +
                                        "} \n";
            headVal += "            .tbl2 td, th{ \n" +
                                            "border: 1px solid black; \n" +
                                            "text-align:center;\n" +
                                        "}\n" +
                                              "\n" +
                                        ".tbl2 th{  \n" +
                                            "height:25px;\n" +
                                        "} \n";
            headVal += "            .tbl3{ \n" +
                                            "border: 1pt solid #000 !important; \n" +
                                            "border-collapse:collapse; \n" +
                                            "width:25%;\n" +
                                            "margin-top: -1px;\n" +
                                        "}\n" +
                                              "\n" +
                                        ".tbl3 td{\n" +
                                            "height:75px;\n" +
                                            "text-align:center;\n" +
                                            "font-weight:600;\n" +
                                        "}\n";
            headVal += "            p{\n" +
                                        "font-family: Calibri !important; \n" +
                                        "font-size:12px;\n" +
                                    "}\n" +
                                        "\n" +
                                    ".BrandAddress {\n" +
                                        "display:table-row;\n" +
                                    "}\n" +
                                          "\n" +
                                    ".Receipt1 p{\n" +
                                        "text-align:center;\n" +
                                        "font-weight:600;\n" +
                                    "}\n" +
                                          "\n" +
                                    ".trBottomBorder{\n" +
                                        "border-bottom:1pt solid #000;\n" +
                                        "height: 25px;\n " +
                                    "}\n" +
                                          "\n" +
                                    ".cellVal1{\n" +
                                        "font-weight:600;\n" +
                                        "text-wrap:normal;\n" +
                                    "}\n";
            headVal += "        </style> \n" + 
                            "</head>";
            #endregion

            #region body of HTML
            headVal += "    <body> \n";
            headVal += "        <form id=\"form1\" runat=\"server\"> \n";
            headVal += "            <table \n";
            #region Header of Receipt
            //headVal += "                <tr> \n";
            //headVal += "                    <td style=\"text-align:center;\"> \n";
            //headVal += "                        <img src=\"" + MapPath("/assets/global/images/Voucher_Img/Logo_CMA_2.png") + "\" alt=\"Logo\" width=\"65%\" /> \n";
            //headVal += "                    </td> \n";
            //headVal += "                    <td> \n";
            //headVal += "                        <table style=\"width:100%; margin:0;\" class=\"BrandAddress\"> \n";
            //headVal += "                            <tr> \n";
            //headVal += "                                <td>";
            //headVal += "                                    <h1 style=\"font-weight:800; font-size:20px;\">CMA CGM SA</h1> \n";
            //headVal += "                                    <p style=\"font-weight:600\" >C.O. PT CONTAINER MARITIME ACTIVITIES</p>";
            //headVal += "                                    <p>PERMATA KUNINGAN LT.21 & 22 <br /> \n" +
            //                                                "JL.KUNINGAN MULIA KAV.9C GUNTUR, SETIABUDI <br />\n" +
            //                                                "JAKARTA SELATAN - DKI JAKARTA <br />\n" +
            //                                                "Phone: +62 21 2854 6800 <br />\n" +
            //                                                "Fax  : +62 21 2854 6801\n" +
            //                                            "</p>";
            //headVal += "                                </td>";
            //headVal += "                            </tr> \n";
            //headVal += "                        </table>";
            //headVal += "                    </td> \n";
            //headVal += "                </tr> \n";
            #endregion
            #region Details of Receipt
            headVal += "                <tr> \n";
            headVal += "                    <td colspan=\"2\"> \n";
            headVal += "                        <table class=\"tbl\"> \n";
            headVal += "                            <tr> \n";
            #region details: Receipt Title
            headVal += "                                <td class=\"Receipt1\" colspan=\"3\"> \n" +
                                                            "<p>RECEIPT VOUCHER</p>\n" +
                                                            "<hr />\n" +
                                                        "</td>\n";
            #endregion
            headVal += "                            </tr> \n";
            headVal += "                            <tr class=\"trBottomBorder\"> \n";
            headVal += "                                <td style=\"width:50%\">&nbsp;</td> \n";
            headVal += "                                <td style=\"width:50%\" colspan=\"2\"> \n";
            headVal += "                                    <table> \n";
            #region details: Voucher No
            headVal += "                                        <tr> \n " +
                                                                    "<td>\n " +
                                                                        "<p>Voucher No</p>\n " +
                                                                    "</td>\n " +
                                                                    "<td>\n " +
                                                                        "<p>:</p>\n " +
                                                                    "</td>\n " +
                                                                    "<td>\n " +
                                                                        "<p>\n " +
                                                                            headerReceipt +"\n " +
                                                                        "</p>\n " +
                                                                    "</td>\n " +
                                                                "</tr>";
            #endregion
            #region details: Date
            headVal += "                                        <tr> \n";
            headVal += "                                            <td>\n " +
                                                                        "<p>Date</p>\n " +
                                                                    "</td>\n " +
                                                                    "<td>\n " +
                                                                        "<p>:</p>\n " +
                                                                    "</td>\n " +
                                                                    "<td>\n " +
                                                                        "<p>\n " +
                                                                            headerDt +"\n " +
                                                                        "</p>\n " +
                                                                    "</td>";
            headVal += "                                        </tr> \n";
            #endregion
            headVal += "                                    </table> \n";
            headVal += "                                </td> \n";
            headVal += "                            </tr> \n";
            headVal += "                            <tr class=\"trBottomBorder\"> \n";
            #region details: Received From
            headVal += "                                <td> \n " +
                                                            "<p>\n " +
                                                                "Received From / Diterima Dari\n " +
                                                            "</p>\n " +
                                                        "</td>\n " +
                                                        "<td>\n " +
                                                            "<p>\n " +
                                                                ":\n " +
                                                            "</p>\n " +
                                                        "</td>\n " +
                                                        "<td>\n " +
                                                            "<p class=\"cellVal1\">\n " +
                                                                detailReceiver + "\n " +
                                                            "</p>\n " +
                                                        "</td>";
            #endregion
            headVal += "                            </tr> \n";
            headVal += "                            <tr class=\"trBottomBorder\"> \n";
            #region details: Amount
            headVal += "                                <td> \n " +
                                                            "<p>\n " +
                                                                "Amount / Jumlah\n " +
                                                            "</p>\n " +
                                                        "</td>\n " +
                                                        "<td>\n " +
                                                            "<p>\n " +
                                                                ":\n " +
                                                            "</p>\n " +
                                                        "</td>\n " +
                                                        "<td>\n " +
                                                            "<p class=\"cellVal1\">\n " +
                                                                headerCurr + " " + headerAmt + "\n " +
                                                            "</p>\n " +
                                                        "</td> \n";
            #endregion
            headVal += "                            </tr> \n";
            headVal += "                            <tr class=\"trBottomBorder\"> \n";
            #region details: Said
            headVal += "                                <td> \n " +
                                                            "<p>\n " +
                                                                "Said / Terbilang\n " +
                                                            "</p>\n " +
                                                        "</td>\n " +
                                                        "<td>\n " +
                                                            "<p>\n " +
                                                                ":\n " +
                                                            "</p>\n " +
                                                        "</td>\n " +
                                                        "<td>\n " +
                                                            "<p class=\"cellVal1\">\n " +
                                                                headerSa +"\n " +
                                                            "</p>\n " +
                                                        "</td>";
            #endregion
            headVal += "                            </tr> \n";
            headVal += "                            <tr style=\"height: 25px;\"> \n";
            headVal += "                                <td> \n " +
                                                            "<p>\n " +
                                                                "Description\n " +
                                                            "</p>\n " +
                                                        "</td>\n " +
                                                        "<td colspan=\"2\">\n " +
                                                            "<p>:</p>\n " +
                                                        "</td>";
            headVal += "                            </tr> \n";
            headVal += "                            <tr> \n";
            headVal += "                                <td colspan=\"3\"> \n";
            detailVal += "                                    <table class=\"tbl2\" style=\"border-collapse:collapse;\"> \n";
            detailVal += "                                        <tr> \n";
            detailVal += "                                            <th style=\"border: 1px solid black;\">BL</th>\n";
            detailVal += "                                            <th style=\"border: 1px solid black;\">Invoice</th>\n";
            detailVal += "                                            <th style=\"border: 1px solid black;\">Description</th>\n";
            detailVal += "                                            <th style=\"border: 1px solid black;\">Amount</th>\n";
            detailVal += "                                        </tr> \n";
            #region details: Description
            if (detailBL.Count > 0)
            {
                for (int l = 0; l < detailBL.Count; l++)
                {
                    detailVal += "                                    <tr> \n";
                    detailVal += "                                        <td style=\"border: 1px solid black;text-align:center;\"> \n";
                    detailVal += "                                            <p> " + detailBL[l].ToString() + "</p> \n";
                    detailVal += "                                        </td> \n";
                    detailVal += "                                        <td style=\"border: 1px solid black;text-align:center;\"> \n";
                    detailVal += "                                            <p> " + detailInv[l].ToString() + "</p> \n";
                    detailVal += "                                        </td> \n";
                    detailVal += "                                        <td style=\"border: 1px solid black;text-align:center;\"> \n";
                    detailVal += "                                            <p> " + detailDesc[l].ToString() + "</p> \n";
                    detailVal += "                                        </td> \n";
                    detailVal += "                                        <td style=\"border: 1px solid black;text-align:right;\"> \n";
                    detailVal += "                                            <p> " + detailAmount[l].ToString() + "</p> \n";
                    detailVal += "                                        </td> \n";
                    detailVal += "                                    </tr> \n";
                    blNum = detailBL[l].ToString();
                }
            }
            detailVal += "                                        <tr> \n";
            detailVal += "                                            <td colspan=\"2\" style=\"text-align:center;border: 1px solid black;\"> <b>VAT</b> </td> \n";
            detailVal += "                                            <td style=\"text-align:center;border: 1px solid black;\"><p>VAT Total (TAX 10%)</p></td>\n";
            detailVal += "                                            <td style=\"text-align:right;border: 1px solid black;\"><p>" + string.Format("{0:N2}", detailVat) + "</p></td>\n";
            detailVal += "                                        </tr> \n";
            //detailVal += "                                        <tr> \n";
            //detailVal += "                                            <td colspan=\"2\" style=\"text-align:center;border: 1px solid black;\"> <b>PPH</b> </td> \n";
            //detailVal += "                                            <td style=\"text-align:center;border: 1px solid black;\"><p>PPH23 (2% Total Invoice)</p></td>\n";
            //detailVal += "                                            <td style=\"text-align:right;border: 1px solid black;\"><p>" + string.Format("{0:N2}", detailPph) + "</p></td>\n";
            //detailVal += "                                        </tr> \n";
            #endregion
            detailVal += "                                    </table> \n";
            headVal += detailVal;
            #region details: Total Amount
            headVal += "                                    <h3> \n " +
                        "                                       Total Amount : " + detailCurr + " " + string.Format("{0:N2}",detailTotal) + "\n " +
                        "                                   </h3> \n";
            #endregion
            headVal += "                                    <table class=\"tbl2\" style=\"font-size: 10pt;\"> \n";
            headVal += "                                        <tr>\n";
            headVal += "                                            <th>BANK</th> \n";
            headVal += "                                            <th>Refference Code</th> \n";
            headVal += "                                            <th>Amount</th> \n";
            headVal += "                                            <th>Date</th> \n";
            headVal += "                                        </tr>\n";
            if (dtReff.Rows.Count > 0)
            {
                for (int m = 0; m < dtReff.Rows.Count; m++)
                {
                    headVal += "                                <tr>\n";
                    headVal += "                                    <td><p>" + dtReff.Rows[m]["BankName"].ToString() + "</p></td> \n";
                    headVal += "                                    <td><p>" + dtReff.Rows[m]["Refference"].ToString() + "</p></td> \n";
                    headVal += "                                    <td style=\"text-align:right;\"><p>" + Convert.ToDecimal(dtReff.Rows[m]["AppliedAmount"]).ToString("N2") + "</p></td> \n";
                    headVal += "                                    <td><p>" + dtReff.Rows[m]["Uploaddate"].ToString() + "</p></td> \n";
                    headVal += "                                </tr>\n";
                }
            }
            headVal += "                                    </table> \n";
            headVal += "                                </td>";
            headVal += "                            </tr> \n";
            headVal += "                        </table>";
            #region details: Received By
            headVal += "                        <table class=\"tbl3\"> \n";
            headVal += "                            <tr> \n " +
                                                        "<td>\n " +
                                                            "<p>\n " +
                                                                "RECEIVED BY:\n " +
                                                            "</p>\n " +
                                                        "</td>\n " +
                                                    "</tr>\n " +
                                                    "<tr>\n " +
                                                        "<td>\n " +
                                                            headCashier +"\n " +
                                                        "</td>\n " +
                                                    "</tr>";
            headVal += "                        </table> \n";
            #endregion
            headVal += "                    </td> \n";
            headVal += "                </tr> \n";
            #endregion
            headVal += "            </table> \n";
            headVal += "        </form> \n";
            headVal += "    </body> \n";
            #endregion

            switch (eType)
            {
                case 1:
                    mailTo = getPartnerEmail(partnerCode);
                    break;

                case 2:
                    mailTo = tbOthersRemind.Text;
                    break;

                case 3:
                    mailTo = getPartnerEmail(partnerCode) + tbOthersRemind.Text;
                    break;
            }
            
            SendEmail(headerReceipt, headVal, blNum, mailTo, headCashier, detailReceiver, detailVal);
            
        }

        public void SendEmail(string rID, string val, string blNo, string toMail, string cashierName, string receiveFrom, string detailBody)
        {
            string a = tbOthersRemind.Text;
            HtmlToPdf converter = new HtmlToPdf();

            #region pdf Setting
            string headerUrl = Server.MapPath("~/Additional/Header.html");
            converter.Options.DisplayHeader = true;
            converter.Header.DisplayOnFirstPage = true;
            converter.Header.DisplayOnOddPages = true;
            converter.Header.DisplayOnEvenPages = true;
            converter.Header.Height = 125;

            PdfHtmlSection headerHtml = new PdfHtmlSection(headerUrl);
            headerHtml.AutoFitHeight = HtmlToPdfPageFitMode.AutoFit;
            headerHtml.AutoFitWidth = HtmlToPdfPageFitMode.AutoFit;
            converter.Header.Add(headerHtml);


            // set converter options
            converter.Options.PdfPageSize = PdfPageSize.A4;
            converter.Options.PdfPageOrientation = PdfPageOrientation.Portrait;
            converter.Options.AutoFitWidth = HtmlToPdfPageFitMode.AutoFit;
            converter.Options.AutoFitHeight = HtmlToPdfPageFitMode.ShrinkOnly;
            converter.Options.MarginLeft = 10;
            //converter.Options.MarginBottom = 1;
            converter.Options.MarginRight = 5;
            converter.Options.MarginTop = 20;

            // create a new pdf document converting an url
            //PdfDocument doc = converter.ConvertUrl(TxtUrl.Text);
            PdfDocument doc = converter.ConvertHtmlString(val);
            #endregion
            

            string fileName = rID + "_" + DateTime.Now.Date.ToString("dd-MM-yyyy");
            string tm = DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString();
            bool fx = File.Exists(MapPath(uploadFolder + fileName + ".pdf"));
            if (fx)
                File.Move(MapPath(uploadFolder + fileName + ".pdf"), MapPath(uploadFolder + fileName + "_" + tm + ".pdf"));
            doc.Save(MapPath(uploadFolder + fileName + ".pdf"));
            string AttachmentFile = HttpContext.Current.Server.MapPath(uploadFolder + fileName + ".pdf");
            
            //// create email message
            MailMessage message = new MailMessage();
            message.From = new MailAddress("ereceipt-support@cma-cgm.com");
            //message.To.Add(new MailAddress(toMail));
            message.To.Add(new MailAddress("dja.mrachmansyah@cma-cgm.com"));
            //message.To.Add(new MailAddress("asia.itretsupport@cma-cgm.com"));
            //message.CC.Add(new MailAddress("asia.itretsupport@cma-cgm.com"));
            string sub = receiptType(rID);
            string eBody = string.Empty;
            #region Creating Email Body
            eBody += "Actually, this email will sent To : " + toMail + " <br/> \n";
            eBody += "               <table style=\"width:100%;\">";
            eBody += "                <tr> \n";
            eBody += "                  <td colspan=\"2\"> \n";
            eBody += "                      <span style=\"font-size: 10pt; font-family: Calibri;\"> \n" +
                                            "<b>*** This is an automatically generated email communication. Please do not reply to the sender of this message.***</b> \n" +
                                            "</span>\n";
            eBody += "                  </td> \n";
            eBody += "                </tr> \n";
            eBody += "                <tr> \n";
            eBody += "                  <td colspan=\"2\"> \n";
            eBody += "                      <span style=\"font-size: 11pt; font-family: Calibri;\"> \n";
            eBody += "                          Dear User of " + receiveFrom + ",<br/> <br/>\n";
            eBody += "                          Here your E-Receipt for BL# : " + blNo + "\n";
            eBody +=                            detailBody + "<br/> <br/>\n";
            eBody += "                          For details, please find your E-Receipt in the attachment. <br/>\n";
            eBody += "                          Thank you in advance for your attention.<br/> \n";
            eBody += "                          Best Regards, <br/><br/><br/>\n";
            eBody += "                      </span> \n";
            eBody += "                      <span style=\"margin-bottom:.0001pt;font-size:12pt;font-family:\"Calibri\";margin-left: 0cm;margin-right: 0cm;margin-top: 0cm;\"> \n\n";
            eBody += "                          <b>" + cashierName + "</b><br/>\n ";
            eBody += "                          [Division Name] <br/> \n";
            eBody += "                          <font color=\"#2F537C\"><b>Indonesia</b></font> <br/> \n";
            eBody += "                      </span> \n";
            eBody += "                      <span style=\"margin-bottom:.0001pt;font-size:8pt;font-family:\"Calibri\";margin-left: 0cm;margin-right: 0cm;margin-top: 0cm;\"> \n";
            eBody += "                          Email : ______________@cmacgm.com</span> <br/>\n";
            eBody += "                      </span> \n";
            eBody += "                  </td>";
            eBody += "                </tr> \n";
            eBody += "                <tr> \n";
            eBody += "                    <td style=\"text-align:center;\"> \n";
            eBody += "                        <img src=cid:ereceiptID> \n";
            eBody += "                    </td> \n";
            eBody += "                    <td> \n";
            eBody += "                        <table style=\"width:100%; margin:0;\" class=\"BrandAddress\"> \n";
            eBody += "                            <tr> \n";
            eBody += "                                <td>";
            eBody += "                                    <h1 style=\"font-weight:800; font-size:20px;\">CMA CGM SA</h1> \n";
            eBody += "                                    <p style=\"font-weight:600\" >C.O. PT CONTAINER MARITIME ACTIVITIES</p>";
            eBody += "                                    <p>PERMATA KUNINGAN LT.21 & 22 <br /> \n" +
                                                            "JL.KUNINGAN MULIA KAV.9C GUNTUR, SETIABUDI <br />\n" +
                                                            "JAKARTA SELATAN - DKI JAKARTA <br />\n" +
                                                            "Phone: +62 21 2854 6800 <br />\n" +
                                                            "Fax  : +62 21 2854 6801\n" +
                                                        "</p>";
            eBody += "                                </td>";
            eBody += "                            </tr> \n";
            eBody += "                        </table>";
            eBody += "                    </td> \n";
            eBody += "                </tr> \n";
            eBody += "                <tr> \n";
            eBody += "                  <td colspan=\"2\"> \n";
            eBody += "                      <span style=\"font-size: 10pt; font-family: Calibri;\"> \n" +
                                            "<b>*** This is an automatically generated email communication. Please do not reply to the sender of this message.***</b> \n" +
                                            "</span>\n";
            eBody += "                  </td> \n";
            eBody += "                </tr> \n";
            eBody += "              </table>";
            #endregion

            message.Subject = "MAIL TESTING -- "+ sub + " Receipt of: " + rID;
            message.Body = eBody;
            Attachment attachment1 = new Attachment(AttachmentFile, MediaTypeNames.Application.Octet);
            message.Attachments.Add(attachment1);

            //message.Attachments.Add(new Attachment(pdfStream, rID+".pdf"));

            AlternateView view = AlternateView.CreateAlternateViewFromString(eBody, null, "text/html");
            message.AlternateViews.Add(view);

            LinkedResource theEmailImage = new LinkedResource(HttpContext.Current.Server.MapPath("~\\assets\\global\\images\\Voucher_Img\\Logo_CMA_3.png"));
            theEmailImage.ContentId = "ereceiptID";
            view.LinkedResources.Add(theEmailImage);

            // send email
            //new SmtpClient().Send(message);
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "129.35.173.46";
            smtp.Port = 25;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.Send(message);

            // close pdf document
            doc.Close();
        }

        public string receiptType(string r)
        {
            string val = string.Empty;
            switch (r.Substring(2,2))
            {
                case "OR":
                    val = "Official";
                    break;

                case "ID":
                    val = "Container Deposit";
                    break;

                default:
                    val = "";
                    break;
            }
            return val;
        }

        public string getPartnerEmail(string pCode)
        {
            string resultP = string.Empty;
            string nameP = string.Empty;
            if (pCode == string.Empty)
            {
                //Find through Web Service
                resultP = "No Partner Code for this Transaction;";
            }
            else
            {
                //Find into db_ereceipt
                DataTable dtByDB = dtFindPartnerEmail(pCode);
                if (dtByDB.Rows.Count > 0)
                {
                    for (int i = 0; i < dtByDB.Rows.Count; i++)
                    {
                        resultP += dtByDB.Rows[i]["pdContactNumber"].ToString() + "; ";
                    }

                }
            }

            return resultP;

        }

        protected void cbCheckAll_Init(object sender, EventArgs e)
        {
            ASPxCheckBox cb = (ASPxCheckBox)sender;
            cb.ClientSideEvents.CheckedChanged = string.Format("cbCheckAll_CheckedChanged");
            cb.Checked = needToSelectAll;
        }

        protected void cbCheck_Init(object sender, EventArgs e)
        {
            ASPxCheckBox cb = (ASPxCheckBox)sender;
            GridViewDataItemTemplateContainer container = (GridViewDataItemTemplateContainer)cb.NamingContainer;

            cb.ClientInstanceName = string.Format("cbCheck{0}", container.VisibleIndex);
            cb.Checked = gvMainGrid.Selection.IsRowSelected(container.VisibleIndex);
            if (!cb.Checked)
            {
                cb.ClientSideEvents.CheckedChanged = string.Format("function (s, e) {{ cbCheck_CheckedChanged(s,e, {0}); }}", container.VisibleIndex);
            }
            else
            {
                cb.ClientSideEvents.CheckedChanged = string.Format("function (s, e) {{ cbCheck_CheckedChanged(s,e, {0}); }}", container.VisibleIndex);
            }
            

            //DataRow row = gvMainGrid.GetDataRow(container.VisibleIndex);
            //cb.Visible = IsCheckBoxVisibleCriteria(row);

        }

        protected void gvMainGrid_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            needToSelectAll = false;
            bool.TryParse(e.Parameters, out needToSelectAll);

            ASPxGridView gridView = (ASPxGridView)sender;

            int startIndex = gridView.PageIndex * gridView.SettingsPager.PageSize;
            int endIndex = Math.Min(gridView.VisibleRowCount, startIndex + gridView.SettingsPager.PageSize);

            for (int i = startIndex; i < endIndex; i++)
            {
                if (needToSelectAll)
                {
                    ASPxCheckBox cb = (ASPxCheckBox)gridView.FindRowCellTemplateControl(i, (GridViewDataColumn)gridView.Columns["#"], "cbCheck");
                    DataRow row = gridView.GetDataRow(i);
                    gridView.Selection.SetSelection(i, true);
                }
                else
                    gridView.Selection.SetSelection(i, needToSelectAll);
            }
        }

        private bool IsCheckBoxVisibleCriteria(DataRow row)
        {
            return !row["IDReceipt"].ToString().Contains("a");
        }

        protected void btnRun_Click(object sender, EventArgs e)
        {
            DateTime dt1 = deDateCompare.Date;
            DateTime dtNow = DateTime.Now.Date;

            int compareValue = DateTime.Compare(dt1, dtNow);
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Value = " + compareValue.ToString() + "');", true);
        }
    }
}