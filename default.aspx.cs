﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.DirectoryServices;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace E_Receipt_v1_1
{
    public partial class _default : System.Web.UI.Page
    {
        string ConnectionString = ConfigurationManager.ConnectionStrings["ereceiptConnectionString"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            MySqlConnection myConn = new MySqlConnection(ConnectionString);
            int va = 0;
            switch (chkAD.Checked)
            {
                case true:
                    #region using AD
                    try
                    {
                        DirectoryEntry de = GetUser(name.Value, pass.Value);
                        if (de != null)
                        {
                            MySqlDataReader myrd = loginDB(name.Value, null, myConn);
                            myrd.Read();
                            switch (myrd.HasRows)
                            {
                                case true:
                                    lblWarning.Visible = false;
                                    PointingSessionLogin(myrd);
                                    InsertData("Data from AD - " + Session["AccessPage"].ToString());
                                    va = 1;
                                    break;

                                case false:
                                    InsertData("No Access Into E-Receipt");
                                    lblWarning.Visible = true;
                                    lblWarning.Text = "You have no access to E-Receipt";
                                    va = 0;
                                    break;
                            }
                            myConn.Close();
                        }
                    }
                    catch
                    {
                        va = 0;
                        lblWarning.Visible = true;
                        lblWarning.Text = "Bad Combination Between Username And Password!";
                    }
                    #endregion
                    break;

                case false:
                    #region Using Local DB
                    MySqlDataReader myrd2 = loginDB(name.Value, pass.Value, myConn);
                    myrd2.Read();
                    switch (myrd2.HasRows)
                    {
                        case true:
                            va = 1;
                            lblWarning.Visible = false;
                            PointingSessionLogin(myrd2);
                            InsertData("Data from DB - " + Session["AccessPage"].ToString());
                            break;

                        case false:
                            va = 0;
                            lblWarning.Visible = true;
                            lblWarning.Text = "Bad Combination Between Username And Password!";
                            break;
                    }
                    myConn.Close();
                    #endregion
                    break;
            }

            switch (va)
            {
                case 1:
                    //Response.Redirect("MainGrid.aspx", false);
                    Response.Redirect(Session["AccessPage"].ToString(), false);
                    break;
            }

        }

        protected void cbpAuthentication_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {

        }

        private MySqlDataReader loginDB(string val1, string val2, MySqlConnection connection)
        {
            string loginname = "select user_id, USER_FIRST_NAME, USER_MIDDLE_NAME, USER_LAST_NAME, DEPARTMENT_CODE, \n " +
	                            "COUNTRY_CODE, PICTURE_PATH, EXTENSION_NO, Company_code, username, PASSWORD, \n " +
	                            "levelKdAccess, STATUS, KdAccess, Site, POD, istoadmin, password2, UserOrder, UserEmail, AccessPage from user_list \n" +
                                "where username=@username ";
            if (val2 != null)
                loginname = loginname + "and password = @password ";

            //MySqlConnection connection = new MySqlConnection(ConnectionString);
            MySqlCommand cmdIns = new MySqlCommand(loginname, connection);
            cmdIns.Parameters.Add(new MySqlParameter("@username", val1));
            if (val2 != null)
                cmdIns.Parameters.Add(new MySqlParameter("@password", val2));
            connection.Open();
            MySqlDataReader mdr = cmdIns.ExecuteReader();
            return mdr;
        }

        private DirectoryEntry GetDirectoryObject(string UserName, string pwd)
        {
            DirectoryEntry oDE;
            oDE = new DirectoryEntry("LDAP://HKHKGCCW-AASI1.asia.cma-cgm.com:3268/DC=cma-cgm,DC=com", UserName, pwd);
            return oDE;
        }

        private DirectoryEntry GetUser(string UserName, string pwd)
        {
            DirectoryEntry de = GetDirectoryObject(UserName, pwd);
            DirectorySearcher deSearch = new DirectorySearcher();
            deSearch.SearchRoot = de;
            deSearch.Filter = "(&(objectClass=user)(SAMAccountName= " + UserName + "))";
            deSearch.SearchScope = SearchScope.Subtree;
            SearchResult results = deSearch.FindOne();
            if (!(results == null))
            {
                de = new DirectoryEntry(results.Path, UserName, pwd, AuthenticationTypes.Secure);
                return de;
            }
            else
            {
                return null;
            }
        }

        public void PointingSessionLogin(MySqlDataReader myrd)
        {
            Session["first_name"] = myrd["USER_FIRST_NAME"].ToString();
            Session["middle_name"] = myrd["USER_MIDDLE_NAME"].ToString();
            Session["last_name"] = myrd["USER_LAST_NAME"].ToString();
            Session["country_code"] = myrd["COUNTRY_CODE"].ToString();
            Session["company_code"] = myrd["Company_code"].ToString();
            Session["username"] = myrd["username"].ToString();
            Session["levelKdAccess"] = myrd["levelKdAccess"].ToString();
            Session["status"] = myrd["STATUS"].ToString();
            Session["KdAccess"] = myrd["KdAccess"].ToString();
            Session["Site"] = myrd["Site"].ToString();
            Session["POD"] = myrd["POD"].ToString();
            Session["istoadmin"] = myrd["istoadmin"].ToString();
            Session["uEmail"] = myrd["UserEmail"].ToString();
            Session["AccessPage"] = myrd["AccessPage"].ToString();
        }

        public void InsertData(string val)
        {
            MySqlConnection con = new MySqlConnection(ConnectionString);
            string ip1 = GetIPAddress();
            if (ip1 == "")
                ip1 = GetIPAdd();
            string ipName = Dns.GetHostName();
            string query = string.Format("INSERT INTO tblLogStat(logDate, logUsername, logVal1, logVal2) " +
                                "VALUES(NOW(), '{0}', 'Login','{1}')", Session["username"].ToString(), ip1 + " : " + ipName + " " + val);
            MySqlCommand cmd = new MySqlCommand(query, con);
            cmd.CommandType = CommandType.Text;
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            cmd.ExecuteNonQuery();
            con.Close();
        }

        protected string GetIPAddress()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }

            return context.Request.ServerVariables["REMOTE_HOST"];
        }

        protected string GetIPAdd()
        {
            string IPAddress = string.Empty;

            String strHostName = HttpContext.Current.Request.UserHostAddress.ToString();

            IPAddress = System.Net.Dns.GetHostAddresses(strHostName).GetValue(0).ToString();

            return IPAddress;
        }
    }
}