﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="E_Receipt_v1_1._default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/>
    <meta name="description" content="admin-themes-lab"/>
    <meta name="author" content="themes-lab"/>
    <link rel="shortcut icon" href="./assets/global/images/favicon.ico" type="image/png"/>
    <title>E-Receipt 1.1</title>
    <link href="./assets/global/css/style.css?v=1.05" rel="stylesheet"/>
    <link href="./assets/global/css/theme.css?v=1".05 rel="stylesheet"/>
    <link href="./assets/global/css/ui.css?v=1.05" rel="stylesheet"/>
    <link href="./assets/admin/layout2/css/layout.css?v=1.05" rel="stylesheet"/>
    <link href="./assets/global/plugins/bootstrap-loading/lada.min.css?v=1.05" rel="stylesheet"/>
    <script>
        function OnloadEve() {
            $.backstretch(["./assets/global/images/gallery/bg-8.jpg", "./assets/global/images/gallery/bg-4.jpg", "./assets/global/images/gallery/bg-3.jpg", "./assets/global/images/gallery/bg-2.jpg", "./assets/global/images/gallery/bg-1.jpg", "./assets/global/images/gallery/bg-5.jpg", "./assets/global/images/gallery/bg-6.jpg", "./assets/global/images/gallery/bg-7.jpg", "./assets/global/images/gallery/bg-15.jpg", "./assets/global/images/gallery/bg-9.jpg", "./assets/global/images/gallery/bg-10.jpg", "./assets/global/images/gallery/bg-11.jpg", "./assets/global/images/gallery/bg-12.jpg", "./assets/global/images/gallery/bg-13.jpg", "./assets/global/images/gallery/bg-14.jpg", "./assets/global/images/gallery/bg-16.jpg", "./assets/global/images/gallery/bg-17.jpg", "./assets/global/images/gallery/bg-18.jpg", "./assets/global/images/gallery/bg-19.jpg"], {
                fade: 600,
                duration: 4000
            });
        }
    </script>
</head>
<body class="sidebar-condensed account2 no-social" data-page="login" onload="OnloadEve()">
    <div class="container" id="login-block">
        <i class="user-img"><img src="assets/global/images/E-Receipt Header 2_375.png" /></i>
            
        <div class="account-info">
            <img src="assets/global/images/VisuelShippingthefuture.png" />
        </div>
        <div class="account-form">
            <form id="form1" class="form-signin" role="form" runat="server">
                <h3><strong>Sign in</strong> to your account</h3>
                <div class="append-icon">
                    <input type="text" name="name" id="name" runat="server" class="form-control form-white username" placeholder="e.g.: dja.wijaya" required="required" />
                    <i class="icon-user"></i>
                </div>
                <div class="append-icon">
                    <input type="password" name="password" id="pass" runat="server" class="form-control form-white password" placeholder="CITRIX/Local Password" required="required" />
                    <i class="icon-lock"></i>
                </div>
                <div class="append-icon m-b-10">
                    <dx:ASPxCheckBox ID="chkAD" runat="server" Text="Using AD" Checked="false" Theme="DevEx" CssClass="f-12" />
                </div>
                <div class="append-icon">
                    <asp:Button ID="btnLogin" runat="server" Text="Submit" CssClass="btn-lg btn-dark btn-rounded" OnClick="btnLogin_Click" />
                </div>
                <div class="form-footer">
                    <asp:Label ID="lblWarning" runat="server" ForeColor="PaleVioletRed" Visible="false" />
                </div>
            </form>
        </div>
        <div id="account-builder">
            <form class="form-horizontal" role="form">
                <div class="row">
                    <div class="col-md-12">
                        <i class="fa fa-spin fa-gear"></i> 
                        <h3><strong>Customize</strong> Login Page</h3>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-xs-8 control-label">Social Login</label>
                            <div class="col-xs-4">
                                <label class="switch m-r-20">
                                    <input id="social-cb" type="checkbox" class="switch-input" disabled="disabled"/>
                                    <span class="switch-label" data-on="On" data-off="Off"></span>
                                    <span class="switch-handle"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-xs-8 control-label">Background Image</label>
                            <div class="col-xs-4">
                                <label class="switch m-r-20">
                                    <input id="image-cb" type="checkbox" class="switch-input" />
                                    <span class="switch-label" data-on="On" data-off="Off"></span>
                                    <span class="switch-handle"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-xs-8 control-label">Background Slides</label>
                            <div class="col-xs-4">
                                <label class="switch m-r-20">
                                <input id="slide-cb" type="checkbox" class="switch-input" checked="checked"/>
                                <span class="switch-label" data-on="On" data-off="Off"></span>
                                <span class="switch-handle"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-xs-8 control-label">User Image</label>
                            <div class="col-xs-4">
                                <label class="switch m-r-20">
                                <input id="user-cb" type="checkbox" class="switch-input"/>
                                <span class="switch-label" data-on="On" data-off="Off"></span>
                                <span class="switch-handle"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <p class="account-copyright">
        <span>Copyright © 2018 </span><span>ASIA ITRET</span>.<span> All rights reserved.</span>
    </p>
    <script src="./assets/global/plugins/jquery/jquery-3.1.0.min.js"></script>
    <script src="./assets/global/plugins/jquery/jquery-migrate-3.0.0.min.js"></script>
    <script src="./assets/global/plugins/gsap/main-gsap.min.js"></script>
    <script src="./assets/global/plugins/tether/js/tether.min.js"></script>
    <script src="./assets/global/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="./assets/global/plugins/backstretch/backstretch.min.js"></script>
    <script src="./assets/global/plugins/bootstrap-loading/lada.min.js"></script>
    <script src="./assets/global/js/pages/login-v2.js"></script>
    <script src="./assets/admin/layout2/js/layout.js"></script>
</body>
</html>
