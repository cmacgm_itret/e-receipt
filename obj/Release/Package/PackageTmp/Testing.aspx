﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Testing.aspx.cs" Inherits="E_Receipt_v1_1.Testing" %>

<%@ Register Assembly="DevExpress.Web.v17.1, Version=17.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/>
    <meta name="description" content="admin-themes-lab"/>
    <meta name="author" content="themes-lab"/>
    <link rel="shortcut icon" href="../assets/global/images/favicon.png" type="image/png"/>
    <title>E-Receipt v1.1</title>
    <link href="./assets/global/css/style.css?v=1" rel="stylesheet"/>
    <link href="./assets/global/css/theme.css?v=1" rel="stylesheet"/>
    <link href="./assets/global/css/ui.css?v=1" rel="stylesheet"/>
    <link href="./assets/admin/layout2/css/layout.css?v=1" rel="stylesheet"/>
    <!-- BEGIN PAGE STYLE -->
    <link href="./assets/global/plugins/metrojs/metrojs.min.css" rel="stylesheet"/>
    <link href="./assets/global/plugins/maps-amcharts/ammap/ammap.css" rel="stylesheet"/>
    <!-- END PAGE STYLE -->
    <script src="./assets/global/plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <script type="text/javascript">
        function OnCustomButtonClick(s, e) {
            if(e.buttonID === 'btnGenerate1')
            {
                //PopRemind.PerformCallback(s.GetRowKey(e.visibleIndex));
                PopRemind.Show();
            }
        }
    </script>
    <script type="text/javascript">
        function cbCheckAll_CheckedChanged(s, e) {
            gvMainGrid.PerformCallback(s.GetChecked().toString());
        }
        function cbCheck_CheckedChanged(s, e, index) {
            gvMainGrid.SelectRowOnPage(index, s.GetChecked());
        }
	</script>
</head>
    <!-- LAYOUT: Apply "submenu-hover" class to body element to have sidebar submenu show on mouse hover -->
    <!-- LAYOUT: Apply "sidebar-collapsed" class to body element to have collapsed sidebar -->
    <!-- LAYOUT: Apply "sidebar-top" class to body element to have sidebar on top of the page -->
    <!-- LAYOUT: Apply "sidebar-hover" class to body element to show sidebar only when your mouse is on left / right corner -->
    <!-- LAYOUT: Apply "submenu-hover" class to body element to show sidebar submenu on mouse hover -->
    <!-- LAYOUT: Apply "fixed-sidebar" class to body to have fixed sidebar -->
    <!-- LAYOUT: Apply "fixed-topbar" class to body to have fixed topbar -->
    <!-- LAYOUT: Apply "rtl" class to body to put the sidebar on the right side -->
    <!-- LAYOUT: Apply "boxed" class to body to have your page with 1200px max width -->

    <!-- THEME STYLE: Apply "theme-sdtl" for Sidebar Dark / Topbar Light -->
    <!-- THEME STYLE: Apply  "theme sdtd" for Sidebar Dark / Topbar Dark -->
    <!-- THEME STYLE: Apply "theme sltd" for Sidebar Light / Topbar Dark -->
    <!-- THEME STYLE: Apply "theme sltl" for Sidebar Light / Topbar Light -->
  
    <!-- THEME COLOR: Apply "color-default" for dark color: #2B2E33 -->
    <!-- THEME COLOR: Apply "color-primary" for primary color: #319DB5 -->
    <!-- THEME COLOR: Apply "color-red" for red color: #C9625F -->
    <!-- THEME COLOR: Apply "color-green" for green color: #18A689 -->
    <!-- THEME COLOR: Apply "color-orange" for orange color: #B66D39 -->
    <!-- THEME COLOR: Apply "color-purple" for purple color: #6E62B5 -->
    <!-- THEME COLOR: Apply "color-blue" for blue color: #4A89DC -->
<body class="sidebar-condensed fixed-topbar fixed-sidebar theme-sdtl color-default">
    <form id="form1" runat="server">
        <!--[if lt IE 7]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <section>
          <!-- BEGIN SIDEBAR -->
          <div class="sidebar">
            <div class="logopanel">
              <h1>
                <a href="#"></a>
              </h1>
            </div>
            <div class="sidebar-inner">
              <div class="menu-title">
                Navigation 
                <div class="pull-right menu-settings">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" data-delay="300"> 
                  <i class="icon-settings"></i>
                  </a>
                  <ul class="dropdown-menu">
                    <li><a href="#" id="reorder-menu" class="reorder-menu">Reorder menu</a></li>
                    <li><a href="#" id="remove-menu" class="remove-menu">Remove elements</a></li>
                    <li><a href="#" id="hide-top-sidebar" class="hide-top-sidebar">Hide / show user image</a></li>
                  </ul>
                </div>
              </div>
              <ul class="nav nav-sidebar">
                <li class=" nav nav-active active"><a href="Testing.aspx"><i class="icon-home"></i><span>Home</span></a></li>
              </ul>
              <!-- SIDEBAR WIDGET -->
              <div class="sidebar-footer clearfix">
                <a class="pull-left footer-settings" href="#" data-rel="tooltip" data-placement="top" data-original-title="Settings">
                <i class="icon-settings"></i></a>
                <a class="pull-left toggle_fullscreen" href="#" data-rel="tooltip" data-placement="top" data-original-title="Fullscreen">
                <i class="icon-size-fullscreen"></i></a>
                <a class="pull-left" href="user-lockscreen.html" data-rel="tooltip" data-placement="top" data-original-title="Lockscreen">
                <i class="icon-lock"></i></a>
                <a class="pull-left btn-effect" href="user-login-v1.html" data-modal="modal-1" data-rel="tooltip" data-placement="top" data-original-title="Logout">
                <i class="icon-power"></i></a>
              </div>
            </div>
          </div>
          <!-- END SIDEBAR -->
          <div class="main-content">
            <!-- BEGIN TOPBAR -->
            <div class="topbar">
              <div class="header-left">
                <div class="topnav">
                  <a class="menutoggle" href="#" data-toggle="sidebar-collapsed"><span class="menu__handle"><span>Menu</span></span></a>
                  <ul class="nav nav-icons">
                    <li><a href="#" class="toggle-sidebar-top"><span class="icon-user-following"></span></a></li>
                    <li><a href="mailbox.html"><span class="octicon octicon-mail-read"></span></a></li>
                    <li><a href="#"><span class="octicon octicon-flame"></span></a></li>
                    <li><a href="builder-page.html"><span class="octicon octicon-rocket"></span></a></li>
                  </ul>
                </div>
              </div>
              <div class="header-right">
                <ul class="header-menu nav navbar-nav">
                  <!-- BEGIN USER DROPDOWN -->
                  <li class="dropdown" id="user-header">
                    <a href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                    <img src="../assets/global/images/avatars/user1.png" alt="user image">
                    <span class="username">Hi, John Doe</span>
                    </a>
                    <ul class="dropdown-menu">
                      <li>
                        <a href="#"><i class="icon-user"></i><span>My Profile</span></a>
                      </li>
                      <li>
                        <a href="#"><i class="icon-settings"></i><span>Account Settings</span></a>
                      </li>
                      <li>
                        <a href="#"><i class="icon-logout"></i><span>Logout</span></a>
                      </li>
                    </ul>
                  </li>
                  <!-- END USER DROPDOWN -->
                </ul>
              </div>
              <!-- header-right -->
            </div>
            <!-- END TOPBAR -->
            <!-- BEGIN PAGE CONTENT -->
            <div class="page-content">
              <div class="header">
                <h2><strong>Main</strong> Grid</h2>
                <div class="breadcrumb-wrapper">
                  <ol class="breadcrumb">
                    <li><a href="#">Pages</a>
                    </li>
                    <li class="active">Main Grid</li>
                  </ol>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-12">
                    <div class="panel-group panel-accordion" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4>
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                        Filter
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="row"    >
                                        <div class="col-md-2">
                                            BL# / Inv# / Voyage# / Refference
                                        </div>
                                        <div class="col-md-4">
                                            <dx:ASPxTextBox ID="txtBL" runat="server" ClientInstanceName="txtBL" Theme="iOS" Width="100%" />
                                        </div>
                                        <div class="col-md-2">
                                            <dx:ASPxButton ID="btnSubmit" runat="server" Text="Search" Theme="iOS" OnClick="btnSubmit_Click" AutoPostBack="false" />
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <dx:ASPxButton ID="btnClear" runat="server" Text="Clear" Theme="iOS" OnClick="btnClear_Click" AutoPostBack="false" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="row m-b-5">
                            <div class="col-lg-2">
                                <dx:ASPxButton ID="btnSendReceipt" runat="server" Text="Send Receipt" Theme="iOS" OnClick="btnSendReceipt_Click" AutoPostBack="false" />
                            </div>
                            <div class="col-lg-10">
                                <dx:ASPxLabel ID="lblSubmitWarning" runat="server" CssClass="label label-danger" Visible="false" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <dx:ASPxCallbackPanel ID="cbpMainGrid" ClientInstanceName="cbpMainGrid" runat="server" Width="100%">
                                    <PanelCollection>
                                        <dx:PanelContent>
                                            <dx:ASPxGridView ID="gvMainGrid" ClientInstanceName="gvMainGrid" runat="server" KeyFieldName="IDReceipt" Width="100%" 
                                                Theme="Aqua" DataSourceID="dsMainGrid" OnCustomColumnDisplayText="gvMainGrid_CustomColumnDisplayText" OnCustomCallback="gvMainGrid_CustomCallback">
                                                <Columns>
                                                    <dx:GridViewDataTextColumn Caption="#" VisibleIndex="0">
                                                        <HeaderTemplate>
                                                            <dx:ASPxCheckBox ID="cbCheckAll" runat="server" OnInit="cbCheckAll_Init" >
                                                            </dx:ASPxCheckBox>
                                                        </HeaderTemplate>
                                                        <DataItemTemplate>
                                                            <dx:ASPxCheckBox ID="cbCheck" runat="server" OnInit="cbCheck_Init">
                                                            </dx:ASPxCheckBox>
                                                        </DataItemTemplate>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="Receipt Number" FieldName="IDReceipt" Name="IDReceipt" 
                                                        VisibleIndex="1" Width="175px" ReadOnly="True" Visible="true" FixedStyle="Left">
                                                        <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                        <CellStyle Font-Size="12px">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataDateColumn Caption="Receipt Date" Name="ReceiptDate"
                                                        FieldName="ReceiptDate" VisibleIndex="2" Width="120px" FixedStyle="Left">
                                                        <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                        <CellStyle Font-Size="12px" Wrap="False" HorizontalAlign="Center">
                                                        </CellStyle>
                                                    </dx:GridViewDataDateColumn>
                                                    <dx:GridViewDataSpinEditColumn Caption="Amount" FieldName="Amount" 
                                                        Name="Amount"  VisibleIndex="3" Width="118px">
                                                        <PropertiesSpinEdit DisplayFormatString="g"></PropertiesSpinEdit>
                                                        <HeaderStyle Font-Size="12px" HorizontalAlign="Center" Wrap="True" />
                                                        <CellStyle Font-Size="12px" Wrap="False"/>
                                                    </dx:GridViewDataSpinEditColumn>
                                                    <dx:GridViewDataTextColumn Caption="Type" FieldName="TYPE" Name="TYPE" 
                                                        VisibleIndex="4" Width="80px" ReadOnly="True" Visible="true">
                                                        <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                        <CellStyle Font-Size="12px" HorizontalAlign="Center">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="Payment Method" FieldName="PaymentMethodDSP" Name="PaymentMethodDSP" 
                                                        VisibleIndex="5" Width="175px" ReadOnly="True" Visible="true">
                                                        <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                        <CellStyle Font-Size="12px" HorizontalAlign="Center">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataMemoColumn Caption="Remarks" FieldName="Remarks" 
                                                        Name="Remarks" VisibleIndex="6" Width="250px">
                                                        <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                        <CellStyle Font-Size="10px" Wrap="True" />
                                                    </dx:GridViewDataMemoColumn>
                                                    <dx:GridViewDataMemoColumn Caption="Description" FieldName="Description" 
                                                        Name="Description" VisibleIndex="7" Width="250px">
                                                        <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                        <CellStyle Font-Size="10px" Wrap="True" />
                                                    </dx:GridViewDataMemoColumn>
                                                    <dx:GridViewDataTextColumn Caption="Category" FieldName="receiptCategoryName" Name="receiptCategoryName" 
                                                        VisibleIndex="8" Width="95px" ReadOnly="True" Visible="true">
                                                        <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                        <CellStyle Font-Size="12px">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="Receipt By" FieldName="userN" Name="userN" 
                                                        VisibleIndex="9" Width="95px" ReadOnly="True" Visible="true">
                                                        <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                        <CellStyle Font-Size="12px">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="Refference" FieldName="Refference" Name="Refference" 
                                                        VisibleIndex="10" Width="200px" ReadOnly="True" Visible="true">
                                                        <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                        <CellStyle Font-Size="10px">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                </Columns>
                                                <ClientSideEvents CustomButtonClick="OnCustomButtonClick" />
                                                <SettingsPager PageSize="15" ShowSeparators="True" AlwaysShowPager="True" >
                                                    <PageSizeItemSettings Visible="true" Position="Right" Items="15, 20, 25, 30, 35, 40, 45, 50" />
                                                </SettingsPager>
                                                <SettingsBehavior ConfirmDelete="True" />
                                                <SettingsDetail ExportMode="Expanded" 
                                                    ShowDetailRow="True" ShowDetailButtons="true" />
                                                <Settings ShowFooter="True" VerticalScrollableHeight="500" 
                                                    VerticalScrollBarMode="Visible" HorizontalScrollBarMode="Auto" 
                                                    ShowFilterBar="Auto" ShowFilterRow="true" ShowFilterRowMenu="false" ShowGroupPanel="True" ShowHeaderFilterButton="false" />
                                                <SettingsLoadingPanel Mode="ShowAsPopup" />
                                                <Templates>
                                                    <DetailRow>
                                                        <dx:ASPxGridView ID="gvDtlMain" runat="server" ClientInstanceName="gvDtlMain" AutoGenerateColumns="false" DataSourceID="dsMainDetail" Theme="Aqua" Width="100%"
                                                            OnBeforePerformDataSelect="gvDtlMain_BeforePerformDataSelect" KeyFieldName="BLNUMBER" OnCustomColumnDisplayText="gvDtlMain_CustomColumnDisplayText" >
                                                            <Columns>
                                                                <dx:GridViewDataMemoColumn Caption="CNEE" FieldName="CNEE" 
                                                                    Name="CNEE" VisibleIndex="0" Width="200px">
                                                                    <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                                    <CellStyle Font-Size="10px" Wrap="True" />
                                                                </dx:GridViewDataMemoColumn>
                                                                <dx:GridViewDataTextColumn Caption="BL Number" FieldName="BLNUMBER" Name="BLNUMBER" 
                                                                    VisibleIndex="1" Width="175px" ReadOnly="True">
                                                                    <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                                    <CellStyle Font-Size="12px">
                                                                    </CellStyle>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Caption="Invoice" FieldName="InvoiceNumber" Name="InvoiceNumber" 
                                                                    VisibleIndex="3" Width="175px" ReadOnly="True" Visible="true">
                                                                    <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                                    <CellStyle Font-Size="12px">
                                                                    </CellStyle>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Caption="Charge Code" FieldName="ChargeCode" Name="ChargeCode" 
                                                                    VisibleIndex="4" Width="120px" ReadOnly="True" Visible="true">
                                                                    <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                                    <CellStyle Font-Size="12px">
                                                                    </CellStyle>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataSpinEditColumn Caption="VAT" FieldName="ChargeAmount" 
                                                                    Name="ChargeAmount"  VisibleIndex="5" Width="118px">
                                                                    <HeaderStyle Font-Size="12px" HorizontalAlign="Center" Wrap="True" />
                                                                    <CellStyle Font-Size="12px" Wrap="False"/>
                                                                </dx:GridViewDataSpinEditColumn>
                                                                <dx:GridViewDataTextColumn Caption="Currency" FieldName="ActualCurr" Name="ActualCurr" 
                                                                    VisibleIndex="6" Width="120px" ReadOnly="True" Visible="true">
                                                                    <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                                    <CellStyle Font-Size="12px">
                                                                    </CellStyle>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataSpinEditColumn Caption="Actual Amount" FieldName="ActualAmount" 
                                                                    Name="ActualAmount"  VisibleIndex="7" Width="118px">
                                                                    <HeaderStyle Font-Size="12px" HorizontalAlign="Center" Wrap="True" />
                                                                    <CellStyle Font-Size="12px" Wrap="False"/>
                                                                </dx:GridViewDataSpinEditColumn>
                                                                <dx:GridViewDataTextColumn Caption="Type" FieldName="TYPE" Name="TYPE" 
                                                                    VisibleIndex="8" Width="120px" ReadOnly="True" Visible="true">
                                                                    <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                                    <CellStyle Font-Size="12px">
                                                                    </CellStyle>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataMemoColumn Caption="Remarks" FieldName="Remarks" 
                                                                    Name="Remarks" VisibleIndex="9" Width="200px">
                                                                    <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                                    <CellStyle Font-Size="10px" Wrap="True" />
                                                                </dx:GridViewDataMemoColumn>
                                                                <dx:GridViewDataTextColumn Caption="Status" FieldName="Deletestatus" Name="Deletestatus" 
                                                                    VisibleIndex="10" Width="120px" ReadOnly="True" Visible="true">
                                                                    <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                                    <CellStyle Font-Size="12px">
                                                                    </CellStyle>
                                                                </dx:GridViewDataTextColumn>
                                                            </Columns>
                                                            <SettingsPager Mode="ShowAllRecords" />
                                                            <Settings VerticalScrollableHeight="500" VerticalScrollBarStyle="Standard" HorizontalScrollBarMode="Auto" />
                                                        </dx:ASPxGridView>
                                                    </DetailRow>
                                                </Templates>
                                            </dx:ASPxGridView>
                                        </dx:PanelContent>
                                    </PanelCollection>
                                </dx:ASPxCallbackPanel>
                                <asp:SqlDataSource ID="dsMainGrid" runat="server" ConnectionString="<%$ ConnectionStrings:cmsidConnectionString %>" 
                                    ProviderName="<%$ ConnectionStrings:cmsidConnectionString.ProviderName %>"></asp:SqlDataSource>
                                <asp:SqlDataSource ID="dsMainDetail" runat="server" ConnectionString="<%$ ConnectionStrings:cmsidConnectionString %>" 
                                        ProviderName="<%$ ConnectionStrings:cmsidConnectionString.ProviderName %>"></asp:SqlDataSource>
                                <dx:ASPxPopupControl ID="PopRemind" ClientInstanceName="PopRemind" runat="server" AllowDragging="true" PopupHorizontalAlign="WindowCenter" 
                                    PopupVerticalAlign="WindowCenter" OnWindowCallback="PopRemind_WindowCallback" ShowHeader="true" HeaderText="Reminder" CloseAction="CloseButton" Theme="iOS" Width="600">
                                    <ContentCollection>
                                        <dx:PopupControlContentControl>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="col-md-1"></div>
                                                    <div class="col-md-10">
                                                        <div class="panel">
                                                            <div class="panel-header">
                                                                <h3>This E-Receipt(s) Will Send To :</h3>
                                                            </div>
                                                            <div class="panel-content">
                                                                <table style="width:100%">
                                                                    <tr>
                                                                        <td colspan="2">
                                                                            <dx:ASPxCheckBox ID="cbAllUser" ClientInstanceName="cbAllUser" runat="server" Theme="iOS" Text="Current Customer Email" > 
                                                                            </dx:ASPxCheckBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <dx:ASPxCheckBox ID="cbOtherUser" ClientInstanceName="cbOtherUser" runat="server" Theme="iOS" Text="Other Email" > 
                                                                            </dx:ASPxCheckBox>
                                                                        </td>
                                                                        <td>
                                                                            <dx:ASPxTextBox ID="tbOthersRemind" ClientInstanceName="tbOthersRemind" Enabled="true" runat="server" Width="250px" Theme="iOS" />
                                                                            <span style="font-size:x-small">use semicolon (;) for more than 1</span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2">
                                                                            <dx:ASPxButton ID="btnReminderSet" runat="server" Text="Send" OnClick="btnReminderSet_Click" CssClass="btn btn-blue" >
                                                                            </dx:ASPxButton>
                                                                            <asp:Label ID="lblWarn" runat="server" ForeColor="Red" Visible="false" Font-Size="10pt" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-1"></div>
                                                </div>
                                            </div>
                                        </dx:PopupControlContentControl>
                                    </ContentCollection>
                                </dx:ASPxPopupControl>
                            </div>
                        </div>
                        <div class="row">
                            <div id="dv" runat="server" class="col-lg-12" />
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <dx:ASPxDateEdit ID="deDateCompare" runat="server"></dx:ASPxDateEdit>
                            </div>
                            <div class="col-md-4">
                                <dx:ASPxButton ID="btnRun" runat="server" Text="Compare Date" OnClick="btnRun_Click" />
                            </div>
                        </div>
                    </div>
                </div>
              <div class="footer">
                <div class="copyright">
                  <p class="pull-left sm-pull-reset">
                    <span>Copyright <span class="copyright">©</span> 2018 </span>
                    <span>ASIA ITRET</span>.
                    <span>All rights reserved. </span>
                  </p>
                  <p class="pull-right sm-pull-reset">
                    <span><a href="#" class="m-r-10">Support</a> | <a href="#" class="m-l-10 m-r-10">Terms of use</a> | <a href="#" class="m-l-10">Privacy Policy</a></span>
                  </p>
                </div>
              </div>
            </div>
            <!-- END PAGE CONTENT -->
          </div>
          <!-- END MAIN CONTENT -->
          <!-- BEGIN BUILDER -->
          <div class="builder hidden-sm hidden-xs" id="builder">
            <a class="builder-toggle"><i class="icon-wrench"></i></a>
            <div class="inner">
              <div class="builder-container">
                <a href="#" class="btn btn-sm btn-default" id="reset-style">reset default style</a>
                <h4>Layout options</h4>
                <div class="layout-option">
                  <span> Fixed Sidebar</span>
                  <label class="switch pull-right">
                  <input data-layout="sidebar" id="switch-sidebar" type="checkbox" class="switch-input" checked="checked" />
                  <span class="switch-label" data-on="On" data-off="Off"></span>
                  <span class="switch-handle"></span>
                  </label>
                </div>
                <div class="layout-option">
                  <span> Sidebar on Hover</span>
                  <label class="switch pull-right">
                  <input data-layout="sidebar-hover" id="switch-sidebar-hover" type="checkbox" class="switch-input" />
                  <span class="switch-label" data-on="On" data-off="Off"></span>
                  <span class="switch-handle"></span>
                  </label>
                </div>
                <div class="layout-option">
                  <span> Submenu on Hover</span>
                  <label class="switch pull-right">
                  <input data-layout="submenu-hover" id="switch-submenu-hover" type="checkbox" class="switch-input"/>
                  <span class="switch-label" data-on="On" data-off="Off"></span>
                  <span class="switch-handle"></span>
                  </label>
                </div>
                <div class="layout-option">
                  <span>Fixed Topbar</span>
                  <label class="switch pull-right">
                  <input data-layout="topbar" id="switch-topbar" type="checkbox" class="switch-input" checked="checked" />
                  <span class="switch-label" data-on="On" data-off="Off"></span>
                  <span class="switch-handle"></span>
                  </label>
                </div>
                <div class="layout-option">
                  <span>Boxed Layout</span>
                  <label class="switch pull-right">
                  <input data-layout="boxed" id="switch-boxed" type="checkbox" class="switch-input" />
                  <span class="switch-label" data-on="On" data-off="Off"></span>
                  <span class="switch-handle"></span>
                  </label>
                </div>
                <h4 class="border-top">Color</h4>
                <div class="row">
                  <div class="col-xs-12">
                    <div class="theme-color bg-dark" data-main="default" data-color="#2B2E33"></div>
                    <div class="theme-color background-primary" data-main="primary" data-color="#319DB5"></div>
                    <div class="theme-color bg-red" data-main="red" data-color="#C75757"></div>
                    <div class="theme-color bg-green" data-main="green" data-color="#1DA079"></div>
                    <div class="theme-color bg-orange" data-main="orange" data-color="#D28857"></div>
                    <div class="theme-color bg-purple" data-main="purple" data-color="#B179D7"></div>
                    <div class="theme-color bg-blue" data-main="blue" data-color="#4A89DC"></div>
                  </div>
                </div>
                <h4 class="border-top">Theme</h4>
                <div class="row row-sm">
                  <div class="col-xs-6">
                    <div class="theme clearfix sdtl" data-theme="sdtl">
                      <div class="header theme-left"></div>
                      <div class="header theme-right-light"></div>
                      <div class="theme-sidebar-dark"></div>
                      <div class="bg-light"></div>
                    </div>
                  </div>
                  <div class="col-xs-6">
                    <div class="theme clearfix sltd" data-theme="sltd">
                      <div class="header theme-left"></div>
                      <div class="header theme-right-dark"></div>
                      <div class="theme-sidebar-light"></div>
                      <div class="bg-light"></div>
                    </div>
                  </div>
                  <div class="col-xs-6">
                    <div class="theme clearfix sdtd" data-theme="sdtd">
                      <div class="header theme-left"></div>
                      <div class="header theme-right-dark"></div>
                      <div class="theme-sidebar-dark"></div>
                      <div class="bg-light"></div>
                    </div>
                  </div>
                  <div class="col-xs-6">
                    <div class="theme clearfix sltl" data-theme="sltl">
                      <div class="header theme-left"></div>
                      <div class="header theme-right-light"></div>
                      <div class="theme-sidebar-light"></div>
                      <div class="bg-light"></div>
                    </div>
                  </div>
                </div>
                <h4 class="border-top">Background</h4>
                <div class="row">
                  <div class="col-xs-12">
                    <div class="bg-color bg-clean" data-bg="clean" data-color="#F8F8F8"></div>
                    <div class="bg-color bg-lighter" data-bg="lighter" data-color="#EFEFEF"></div>
                    <div class="bg-color bg-light-default" data-bg="light-default" data-color="#E9E9E9"></div>
                    <div class="bg-color bg-light-blue" data-bg="light-blue" data-color="#E2EBEF"></div>
                    <div class="bg-color bg-light-purple" data-bg="light-purple" data-color="#E9ECF5"></div>
                    <div class="bg-color bg-light-dark" data-bg="light-dark" data-color="#DCE1E4"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- END BUILDER -->
        </section>
        <!-- BEGIN PRELOADER -->
        <div class="loader-overlay">
          <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
          </div>
        </div>
        <!-- END PRELOADER -->
    </form>

    <a href="#" class="scrollup"><i class="fa fa-angle-up"></i></a> 
    <script src="../assets/global/plugins/jquery/jquery-3.1.0.min.js"></script>
    <script src="../assets/global/plugins/jquery/jquery-migrate-3.0.0.min.js"></script>
    <script src="../assets/global/plugins/jquery-ui/jquery-ui.min.js"></script>
    <script src="../assets/global/plugins/gsap/main-gsap.min.js"></script>
    <script src="../assets/global/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="../assets/global/plugins/appear/jquery.appear.js"></script>
    <script src="../assets/global/plugins/jquery-cookies/jquery.cookies.min.js"></script> <!-- Jquery Cookies, for theme -->
    <script src="../assets/global/plugins/jquery-block-ui/jquery.blockUI.min.js"></script> <!-- simulate synchronous behavior when using AJAX -->
    <script src="../assets/global/plugins/bootbox/bootbox.min.js"></script> <!-- Modal with Validation -->
    <script src="../assets/global/plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js"></script> <!-- Custom Scrollbar sidebar -->
    <script src="../assets/global/plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js"></script> <!-- Show Dropdown on Mouseover -->
    <script src="../assets/global/plugins/charts-sparkline/sparkline.min.js"></script> <!-- Charts Sparkline -->
    <script src="../assets/global/plugins/retina/retina.min.js"></script> <!-- Retina Display -->
    <script src="../assets/global/plugins/select2/select2.full.min.js"></script> <!-- Select Inputs -->
    <script src="../assets/global/plugins/icheck/icheck.min.js"></script> <!-- Checkbox & Radio Inputs -->
    <script src="../assets/global/plugins/backstretch/backstretch.min.js"></script> <!-- Background Image -->
    <script src="../assets/global/plugins/bootstrap-progressbar/bootstrap-progressbar.min.js"></script> <!-- Animated Progress Bar -->
    <script src="../assets/global/js/builder.js"></script> <!-- Theme Builder -->
    <script src="../assets/global/js/sidebar_hover.js"></script> <!-- Sidebar on Hover -->
    <script src="../assets/global/js/application.js"></script> <!-- Main Application Script -->
    <script src="../assets/global/js/plugins.js"></script> <!-- Main Plugin Initialization Script -->
    <script src="../assets/global/js/widgets/notes.js"></script> <!-- Notes Widget -->
    <script src="../assets/global/js/quickview.js"></script> <!-- Chat Script -->
    <script src="../assets/global/js/pages/search.js"></script> <!-- Search Script -->
    <!-- BEGIN PAGE SCRIPT -->
    <script src="../assets/global/plugins/noty/jquery.noty.packaged.min.js"></script>  <!-- Notifications -->
    <script src="../assets/global/plugins/bootstrap-editable/js/bootstrap-editable.min.js"></script> <!-- Inline Edition X-editable -->
    <script src="../assets/global/plugins/bootstrap-context-menu/bootstrap-contextmenu.min.js"></script> <!-- Context Menu -->
    <script src="../assets/global/plugins/multidatepicker/multidatespicker.min.js"></script> <!-- Multi dates Picker -->
    <script src="../assets/global/js/widgets/todo_list.js"></script>
    <script src="../assets/global/plugins/metrojs/metrojs.min.js"></script> <!-- Flipping Panel -->
    <script src="../assets/global/plugins/charts-chartjs/Chart.min.js"></script>  <!-- ChartJS Chart -->
    <script src="../assets/global/plugins/charts-highstock/js/highstock.js"></script> <!-- financial Charts -->
    <script src="../assets/global/plugins/charts-highstock/js/modules/exporting.js"></script> <!-- Financial Charts Export Tool -->
    <script src="../assets/global/plugins/maps-amcharts/ammap/ammap.js"></script> <!-- Vector Map -->
    <script src="../assets/global/plugins/maps-amcharts/ammap/maps/js/worldLow.js"></script> <!-- Vector World Map  -->
    <script src="../assets/global/plugins/maps-amcharts/ammap/themes/black.js"></script> <!-- Vector Map Black Theme -->
    <script src="../assets/global/plugins/skycons/skycons.min.js"></script> <!-- Animated Weather Icons -->
    <script src="../assets/global/plugins/simple-weather/jquery.simpleWeather.js"></script> <!-- Weather Plugin -->
    <script src="../assets/global/js/widgets/widget_weather.js"></script>
    <script src="../assets/global/js/pages/dashboard.js"></script>
    <!-- END PAGE SCRIPT -->
    <script src="../assets/admin/layout2/js/layout.js"></script>

</body>
</html>
