﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ER.Master" AutoEventWireup="true" CodeBehind="Main.aspx.cs" Inherits="E_Receipt_v1_1.Main" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeaderTitle" runat="server">
    <h2><strong>Main</strong> Grid</h2>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentValues" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="panel-group panel-accordion" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                Filter
                            </a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-2">
                                    BL# / Inv# / Receipt#
                                </div>
                                <div class="col-md-4">
                                    <dx:ASPxTextBox ID="txtBL" runat="server" ClientInstanceName="txtBL" Theme="iOS" Width="100%" />
                                </div>
                                <div class="col-md-2">
                                    <dx:ASPxButton ID="btnSubmit" runat="server" Text="Search" Theme="iOS" OnClick="btnSubmit_Click" AutoPostBack="false" />
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <dx:ASPxButton ID="btnClear" runat="server" Text="Clear" Theme="iOS" OnClick="btnClear_Click" AutoPostBack="false" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="row m-b-5">
                <div class="col-lg-2">
                    <dx:ASPxButton ID="btnSendReceipt" runat="server" Text="Send Receipt" Theme="iOS" OnClick="btnSendReceipt_Click" AutoPostBack="false" />
                </div>
                <div class="col-lg-10">
                    <dx:ASPxLabel ID="lblSubmitWarning" runat="server" CssClass="label label-danger" Visible="false" />
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <dx:ASPxCallbackPanel ID="cbpMainGrid" ClientInstanceName="cbpMainGrid" runat="server" Width="100%">
                        <PanelCollection>
                            <dx:PanelContent>
                                <dx:ASPxGridView ID="gvMainGrid" ClientInstanceName="gvMainGrid" runat="server" KeyFieldName="IDReceipt" Width="100%" 
                                    Theme="Aqua" DataSourceID="dsMainGrid" OnCustomColumnDisplayText="gvMainGrid_CustomColumnDisplayText">
                                    <Columns>
                                        <dx:GridViewCommandColumn ShowSelectCheckbox="true" SelectAllCheckboxMode="AllPages" VisibleIndex="0" FixedStyle="Left" />
                                        <dx:GridViewDataTextColumn Caption="Receipt Number" FieldName="IDReceipt" Name="IDReceipt" 
                                            VisibleIndex="1" Width="175px" ReadOnly="True" Visible="true" FixedStyle="Left">
                                            <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                            <CellStyle Font-Size="12px">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Receipt Date" Name="ReceiptDate"
                                            FieldName="ReceiptDate" VisibleIndex="2" Width="120px" FixedStyle="Left">
                                            <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                            <CellStyle Font-Size="12px" Wrap="False" HorizontalAlign="Center">
                                            </CellStyle>
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataSpinEditColumn Caption="Amount" FieldName="Amount" 
                                            Name="Amount"  VisibleIndex="3" Width="118px">
                                            <PropertiesSpinEdit DisplayFormatString="g"></PropertiesSpinEdit>
                                            <HeaderStyle Font-Size="12px" HorizontalAlign="Center" Wrap="True" />
                                            <CellStyle Font-Size="12px" Wrap="False"/>
                                        </dx:GridViewDataSpinEditColumn>
                                        <dx:GridViewDataTextColumn Caption="Type" FieldName="TYPE" Name="TYPE" ShowInCustomizationForm="True"  
                                            VisibleIndex="4" Width="80px" ReadOnly="false" Visible="true" Settings-AllowHeaderFilter="True" >
                                            <SettingsHeaderFilter Mode="CheckedList" />
                                            <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                            <CellStyle Font-Size="12px" HorizontalAlign="Center">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Payment Method" FieldName="PaymentMethodDSP" Name="PaymentMethodDSP" 
                                            VisibleIndex="5" Width="175px" ReadOnly="True" Visible="true" Settings-AllowHeaderFilter="True">
                                            <SettingsHeaderFilter Mode="CheckedList" />
                                            <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                            <CellStyle Font-Size="12px" HorizontalAlign="Center">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataMemoColumn Caption="Remarks" FieldName="Remarks" 
                                            Name="Remarks" VisibleIndex="6" Width="250px">
                                            <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                            <CellStyle Font-Size="10px" Wrap="True" />
                                        </dx:GridViewDataMemoColumn>
                                        <dx:GridViewDataMemoColumn Caption="Description" FieldName="Description" 
                                            Name="Description" VisibleIndex="7" Width="250px">
                                            <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                            <CellStyle Font-Size="10px" Wrap="True" />
                                        </dx:GridViewDataMemoColumn>
                                        <dx:GridViewDataTextColumn Caption="Category" FieldName="receiptCategoryName" Name="receiptCategoryName" 
                                            VisibleIndex="8" Width="95px" ReadOnly="True" Visible="true" Settings-AllowHeaderFilter="True">
                                            <SettingsHeaderFilter Mode="CheckedList" />
                                            <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                            <CellStyle Font-Size="12px">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Receipt By" FieldName="userN" Name="userN" 
                                            VisibleIndex="9" Width="95px" ReadOnly="True" Visible="true" Settings-AllowHeaderFilter="True">
                                            <SettingsHeaderFilter Mode="CheckedList" />
                                            <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                            <CellStyle Font-Size="12px">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="REFFNO" FieldName="REFFNO" Name="REFFNO" 
                                            VisibleIndex="10" Width="100px" ReadOnly="True" Visible="true">
                                            <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                            <CellStyle Font-Size="12px">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <ClientSideEvents CustomButtonClick="OnCustomButtonClick" />
                                    <SettingsPager PageSize="15" ShowSeparators="True" AlwaysShowPager="True" >
                                        <PageSizeItemSettings Visible="true" Position="Right" Items="15, 20, 25, 30, 35, 40, 45, 50" />
                                    </SettingsPager>
                                    <SettingsBehavior ConfirmDelete="True" />
                                    <SettingsDetail ExportMode="Expanded" 
                                        ShowDetailRow="True" ShowDetailButtons="true" />
                                    <Settings ShowFooter="True" VerticalScrollableHeight="500" 
                                        VerticalScrollBarMode="Visible" HorizontalScrollBarMode="Auto"
                                        ShowFilterBar="Auto" ShowFilterRow="true" ShowFilterRowMenu="true" ShowGroupPanel="True" ShowHeaderFilterButton="false" />
                                    <SettingsLoadingPanel Mode="ShowAsPopup" />
                                    <Templates>
                                        <DetailRow>
                                            <dx:ASPxGridView ID="gvDtlMain" runat="server" ClientInstanceName="gvDtlMain" AutoGenerateColumns="false" DataSourceID="dsMainDetail" Theme="Aqua" Width="100%"
                                                OnBeforePerformDataSelect="gvDtlMain_BeforePerformDataSelect" KeyFieldName="BLNUMBER" OnCustomColumnDisplayText="gvDtlMain_CustomColumnDisplayText" >
                                                <Columns>
                                                    <dx:GridViewDataMemoColumn Caption="CNEE" FieldName="CNEE" 
                                                        Name="CNEE" VisibleIndex="0" Width="200px">
                                                        <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                        <CellStyle Font-Size="10px" Wrap="True" />
                                                    </dx:GridViewDataMemoColumn>
                                                    <dx:GridViewDataTextColumn Caption="BL Number" FieldName="BLNUMBER" Name="BLNUMBER" 
                                                        VisibleIndex="1" Width="175px" ReadOnly="True">
                                                        <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                        <CellStyle Font-Size="12px">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="Invoice" FieldName="InvoiceNumber" Name="InvoiceNumber" 
                                                        VisibleIndex="3" Width="175px" ReadOnly="True" Visible="true">
                                                        <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                        <CellStyle Font-Size="12px">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="Charge Code" FieldName="ChargeCode" Name="ChargeCode" 
                                                        VisibleIndex="4" Width="120px" ReadOnly="True" Visible="true">
                                                        <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                        <CellStyle Font-Size="12px">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataSpinEditColumn Caption="VAT" FieldName="ChargeAmount" 
                                                        Name="ChargeAmount"  VisibleIndex="5" Width="118px">
                                                        <HeaderStyle Font-Size="12px" HorizontalAlign="Center" Wrap="True" />
                                                        <CellStyle Font-Size="12px" Wrap="False"/>
                                                    </dx:GridViewDataSpinEditColumn>
                                                    <dx:GridViewDataTextColumn Caption="Currency" FieldName="ActualCurr" Name="ActualCurr" 
                                                        VisibleIndex="6" Width="120px" ReadOnly="True" Visible="true">
                                                        <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                        <CellStyle Font-Size="12px">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataSpinEditColumn Caption="Actual Amount" FieldName="ActualAmount" 
                                                        Name="ActualAmount"  VisibleIndex="7" Width="118px">
                                                        <HeaderStyle Font-Size="12px" HorizontalAlign="Center" Wrap="True" />
                                                        <CellStyle Font-Size="12px" Wrap="False"/>
                                                    </dx:GridViewDataSpinEditColumn>
                                                    <dx:GridViewDataTextColumn Caption="Type" FieldName="TYPE" Name="TYPE" 
                                                        VisibleIndex="8" Width="120px" ReadOnly="True" Visible="true">
                                                        <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                        <CellStyle Font-Size="12px">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataMemoColumn Caption="Remarks" FieldName="Remarks" 
                                                        Name="Remarks" VisibleIndex="9" Width="200px">
                                                        <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                        <CellStyle Font-Size="10px" Wrap="True" />
                                                    </dx:GridViewDataMemoColumn>
                                                    <dx:GridViewDataTextColumn Caption="Status" FieldName="Deletestatus" Name="Deletestatus" 
                                                        VisibleIndex="10" Width="120px" ReadOnly="True" Visible="true">
                                                        <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                                        <CellStyle Font-Size="12px">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                </Columns>
                                                <SettingsPager Mode="ShowAllRecords" />
                                                <Settings VerticalScrollableHeight="75" VerticalScrollBarStyle="Standard" HorizontalScrollBarMode="Auto" />
                                            </dx:ASPxGridView>
                                        </DetailRow>
                                    </Templates>
                                </dx:ASPxGridView>
                            </dx:PanelContent>
                        </PanelCollection>
                    </dx:ASPxCallbackPanel>
                    <asp:SqlDataSource ID="dsMainGrid" runat="server" ConnectionString="<%$ ConnectionStrings:cmsidConnectionString %>" 
                        ProviderName="<%$ ConnectionStrings:cmsidConnectionString.ProviderName %>"></asp:SqlDataSource>
                    <asp:SqlDataSource ID="dsMainDetail" runat="server" ConnectionString="<%$ ConnectionStrings:cmsidConnectionString %>" 
                            ProviderName="<%$ ConnectionStrings:cmsidConnectionString.ProviderName %>"></asp:SqlDataSource>
                    <dx:ASPxPopupControl ID="PopRemind" ClientInstanceName="PopRemind" runat="server" AllowDragging="true" PopupHorizontalAlign="WindowCenter" 
                        PopupVerticalAlign="WindowCenter" OnWindowCallback="PopRemind_WindowCallback" ShowHeader="true" HeaderText="Reminder" CloseAction="CloseButton" Theme="iOS" Width="600">
                        <ContentCollection>
                            <dx:PopupControlContentControl>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-1"></div>
                                        <div class="col-md-10">
                                            <div class="panel">
                                                <div class="panel-header">
                                                    <h3>This E-Receipt(s) Will Send To :</h3>
                                                </div>
                                                <div class="panel-content">
                                                    <table style="width:100%">
                                                        <tr>
                                                            <td colspan="2">
                                                                <dx:ASPxCheckBox ID="cbAllUser" ClientInstanceName="cbAllUser" runat="server" Theme="iOS" Text="Current Customer Email" > 
                                                                </dx:ASPxCheckBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <dx:ASPxCheckBox ID="cbOtherUser" ClientInstanceName="cbOtherUser" runat="server" Theme="iOS" Text="Other Email" > 
                                                                </dx:ASPxCheckBox>
                                                            </td>
                                                            <td>
                                                                <dx:ASPxTextBox ID="tbOthersRemind" ClientInstanceName="tbOthersRemind" Enabled="true" runat="server" Width="250px" Theme="iOS" />
                                                                <span style="font-size:x-small">use semicolon (;) for more than 1</span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <dx:ASPxCheckBox ID="cbToCC" ClientInstanceName="cbToCC" runat="server" Theme="iOS" Text="cc Email" > 
                                                                </dx:ASPxCheckBox>
                                                            </td>
                                                            <td>
                                                                <dx:ASPxTextBox ID="tbToCC" ClientInstanceName="tbToCC" Enabled="true" runat="server" Width="250px" Theme="iOS" />
                                                                <span style="font-size:x-small">use semicolon (;) for more than 1</span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <dx:ASPxCheckBox ID="cbToBCC" ClientInstanceName="cbToBCC" runat="server" Theme="iOS" Text="bcc Email" > 
                                                                </dx:ASPxCheckBox>
                                                            </td>
                                                            <td>
                                                                <dx:ASPxTextBox ID="tbToBCC" ClientInstanceName="tbToBCC" Enabled="true" runat="server" Width="250px" Theme="iOS" />
                                                                <span style="font-size:x-small">use semicolon (;) for more than 1</span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <dx:ASPxButton ID="btnReminderSet" runat="server" Text="Send" OnClick="btnReminderSet_Click" CssClass="btn btn-blue" >
                                                                </dx:ASPxButton>
                                                                <asp:Label ID="lblWarn" runat="server" ForeColor="Red" Visible="false" Font-Size="10pt" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-1"></div>
                                    </div>
                                </div>
                            </dx:PopupControlContentControl>
                        </ContentCollection>
                    </dx:ASPxPopupControl>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

