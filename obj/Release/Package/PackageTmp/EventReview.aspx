﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ER.Master" AutoEventWireup="true" CodeBehind="EventReview.aspx.cs" Inherits="E_Receipt_v1_1.EventReview" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeaderTitle" runat="server">
    <h2><strong>Event</strong> Review</h2>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentValues" runat="server">
    <div class ="row">
        <div class="col-md-12">
            <dx:ASPxCallbackPanel ID="cbpEventReview" runat="server" ClientInstanceName="cbpEventReview" Width="100%">
                <PanelCollection>
                    <dx:PanelContent>
                        <dx:ASPxGridView ID="gvEventReview" ClientInstanceName="gvEventReview" runat="server" Width="100%" Theme="Aqua"
                            DataSourceID="dsEventReview" KeyFieldName="LogDate" >
                            <Columns>
                                <dx:GridViewDataDateColumn Caption="Log Date" Name="LogDate"
                                    FieldName="LogDate" VisibleIndex="0" Width="175px" FixedStyle="Left">
                                    <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                    <CellStyle Font-Size="12px" Wrap="False" HorizontalAlign="Center">
                                    </CellStyle>
                                    <PropertiesDateEdit DisplayFormatString="dd-MMM-yyyy hh:mm:ss">
                                    </PropertiesDateEdit>
                                    <SettingsHeaderFilter Mode="DateRangeCalendar" />
                                </dx:GridViewDataDateColumn>
                                <dx:GridViewDataTextColumn Caption="Sent By" FieldName="LogSentBy" Name="LogSentBy" 
                                    VisibleIndex="1" Width="175px" ReadOnly="True" Visible="true" FixedStyle="Left" 
                                    Settings-AllowHeaderFilter="True">
                                    <SettingsHeaderFilter Mode="CheckedList" />
                                    <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                    <CellStyle Font-Size="12px">
                                    </CellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Log ReceiptID" FieldName="LogReceiptID" Name="LogReceiptID" 
                                    VisibleIndex="2" Width="175px" ReadOnly="True" Visible="true" FixedStyle="Left">
                                    <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                    <CellStyle Font-Size="12px">
                                    </CellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Log BL#" FieldName="LogBLNumber" Name="LogBLNumber" 
                                    VisibleIndex="3" Width="175px" ReadOnly="True" Visible="true" FixedStyle="None">
                                    <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                    <CellStyle Font-Size="12px">
                                    </CellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Log Inv#" FieldName="LogInvNumber" Name="LogInvNumber" 
                                    VisibleIndex="4" Width="175px" ReadOnly="True" Visible="true" FixedStyle="None">
                                    <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                    <CellStyle Font-Size="12px">
                                    </CellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Log Sent To" FieldName="LogSentTo" Name="LogSentTo" 
                                    VisibleIndex="5" Width="200px" ReadOnly="True" Visible="true" FixedStyle="None"
                                    Settings-AllowHeaderFilter="True">
                                    <SettingsHeaderFilter Mode="CheckedList" />
                                    <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                    <CellStyle Font-Size="10px">
                                    </CellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Log Sent Cc" FieldName="LogSentCc" Name="LogSentCc" 
                                    VisibleIndex="6" Width="175px" ReadOnly="True" Visible="true" FixedStyle="None">
                                    <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                    <CellStyle Font-Size="10px">
                                    </CellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Log Sent Bcc" FieldName="LogSentBcc" Name="LogSentBcc" 
                                    VisibleIndex="7" Width="175px" ReadOnly="True" Visible="true" FixedStyle="None">
                                    <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                    <CellStyle Font-Size="10px">
                                    </CellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Log Sent Status" FieldName="LogStatus" Name="LogStatus" 
                                    VisibleIndex="8" Width="175px" ReadOnly="True" Visible="true" FixedStyle="None">
                                    <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                    <CellStyle Font-Size="12px">
                                    </CellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataMemoColumn Caption="Log Desc" FieldName="LogDesc" Name="LogDesc" 
                                    VisibleIndex="9" Width="300px" ReadOnly="True" Visible="true" FixedStyle="None">
                                    <HeaderStyle Font-Size="12px" Wrap="True" HorizontalAlign="Center" />
                                    <CellStyle Font-Size="9px" Wrap="True">
                                    </CellStyle>
                                </dx:GridViewDataMemoColumn>
                            </Columns>
                            <SettingsPager PageSize="15" ShowSeparators="True" AlwaysShowPager="True" >
                                <PageSizeItemSettings Visible="true" Position="Right" Items="15, 20, 25, 30, 35, 40, 45, 50" />
                            </SettingsPager>
                            <SettingsBehavior ColumnResizeMode="Control" ConfirmDelete="True" FilterRowMode="Auto" />
                            <Settings ShowFooter="True" VerticalScrollableHeight="450" 
                                VerticalScrollBarMode="Visible" HorizontalScrollBarMode="Auto"
                                ShowFilterBar="Auto" ShowFilterRow="true" ShowFilterRowMenu="true" ShowGroupPanel="True" ShowHeaderFilterButton="false" />
                            <SettingsLoadingPanel Mode="ShowAsPopup" />
                        </dx:ASPxGridView>
                    </dx:PanelContent>
                </PanelCollection>
            </dx:ASPxCallbackPanel>
            <asp:SqlDataSource ID="dsEventReview" runat="server" ConnectionString="<%$ ConnectionStrings:ereceiptConnectionString %>"
                ProviderName="<%$ ConnectionStrings:ereceiptConnectionString.ProviderName %>"
                SelectCommand="SELECT LogDate, LogSentBy, LogReceiptID, LogBLNumber, LogInvNumber, LogSentTo, LogSentCc, LogSentBcc, LogStatus, LogDesc  FROM tblLogSentEmail ORDER BY LogDate DESC;"></asp:SqlDataSource>
        </div>
    </div>
</asp:Content>
