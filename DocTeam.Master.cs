﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace E_Receipt_v1_1
{
    public partial class DocTeam : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
             if (Session["username"] != null)
            {
                if (!IsPostBack)
                {
                    if (Session["username"] == null)
                    {
                        try
                        {
                            ASPxWebControl.RedirectOnCallback("~/default.aspx");
                            //Response.Redirect("login.aspx");
                        }
                        catch
                        {
                            Response.Redirect("default.aspx");
                            //ASPxWebControl.RedirectOnCallback("~/login.aspx");
                        }
                    }
                    else
                    {
                        divUser.InnerHtml = "<img src='./assets/global/images/avatars/user1.png' alt='user image' /> \n";
                        divUser.InnerHtml += "<span class='username'>Hi, " + Session["username"].ToString() + " </span>";
                    }
                }
            }
            else
            {
                //GenerateMainGv();
                Response.Redirect("default.aspx");
            }
        }
        
    }
}